package com.mananinnovation.tmssurvey.widgets

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LinearItemDecorator(private val spacing: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val layoutManager = parent.layoutManager
        if (layoutManager is LinearLayoutManager) {
            if (layoutManager.canScrollVertically()) {
                when (parent.getChildAdapterPosition(view)) {
                    0 -> {
                        outRect.top = spacing
                        outRect.bottom = spacing / 2
                    }
                    parent.adapter?.itemCount!! - 1 -> {
                        outRect.top = spacing / 2
                        outRect.bottom = spacing
                    }
                    else -> {
                        outRect.top = spacing / 2
                        outRect.bottom = spacing / 2
                    }
                }
                outRect.left = spacing
                outRect.right = spacing
            }
        }
    }
}
