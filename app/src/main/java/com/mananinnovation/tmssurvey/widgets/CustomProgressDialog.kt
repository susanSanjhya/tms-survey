package com.mananinnovation.tmssurvey.widgets

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_progress.view.*

class CustomProgressDialog : DialogFragment() {

    companion object {
        const val TAG = "CustomProgressDialog"
        const val KEY_MESSAGE = "KEY_MESSAGE"
    }

    private var message: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        message = arguments?.getString(KEY_MESSAGE)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        return inflater.inflate(R.layout.fragment_progress, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(false)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        this@CustomProgressDialog.javaClass.log("message: $message")
        if (message == null) {
            view.tvMessage.isGone = true
        } else {
            view.tvMessage.isVisible = true
            view.tvMessage.text = message
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}
