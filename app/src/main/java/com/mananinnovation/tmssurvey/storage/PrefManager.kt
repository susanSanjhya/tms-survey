package com.mananinnovation.tmssurvey.storage

import android.content.Context

class PrefManager(context: Context) {

    private val pref = context.getSharedPreferences("USER_DETAIL", Context.MODE_PRIVATE)
    private val keyAccessToken = "KEY_ACCESS_TOKEN"
    private val keyFullName = "KEY_FULL_NAME"
    private val keyUserId = "KEY_USER_ID"
    private val keyUserRole = "KEY_USER_ROLE"
    private val keyEnumerators = "KEY_ENUMERATORS"

    var enumerators: String?
        get() {
            return pref.getString(keyEnumerators, null)
        }
        set(value) {
            pref.edit().putString(keyEnumerators, value).apply()
        }
    var role: String?
        get() {
            return pref.getString(keyUserRole, null)
        }
        set(value) {
            pref.edit().putString(keyUserRole, value).apply()
        }

    var userId: Int
        get() {
            return pref.getInt(keyUserId, 0)
        }
        set(value) {
            pref.edit().putInt(keyUserId, value).apply()
        }
    var fullName: String?
        get() {
            return pref.getString(keyFullName, null)
        }
        set(value) {
            pref.edit().putString(keyFullName, value).apply()
        }

    var accessToken: String?
        get() {
            return pref.getString(keyAccessToken, null)
        }
        set(value) {
            pref.edit().putString(keyAccessToken, value).apply()
        }

}
