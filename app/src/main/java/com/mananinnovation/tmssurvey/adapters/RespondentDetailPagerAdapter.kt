package com.mananinnovation.tmssurvey.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mananinnovation.tmssurvey.fragments.HouseHoldDetailFragment
import com.mananinnovation.tmssurvey.fragments.RespondentDetailFragment

class RespondentDetailPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                HouseHoldDetailFragment()
            }
            else -> {
                RespondentDetailFragment()
            }
        }
    }

    override fun getCount() = 2

}
