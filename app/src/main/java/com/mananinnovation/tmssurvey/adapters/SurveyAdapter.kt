package com.mananinnovation.tmssurvey.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.RespondentDto
import com.mananinnovation.tmssurvey.protocols.OnRespondentDetailClickListener
import java.util.*

class SurveyAdapter(private val allRespondentInfo: ArrayList<RespondentDto>) : RecyclerView.Adapter<SurveyAdapter.ViewHolder>() {
    var onRespondentDetailClickListener: OnRespondentDetailClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.row_respondent, parent, false))
    }

    override fun getItemCount(): Int {
        return allRespondentInfo.size
    }

    fun getItem(position: Int) = allRespondentInfo[position]

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = String.format("Name: %s", allRespondentInfo[position].name)
        holder.tvAddress.text = String.format("Address: %s, %s, %s-%s",
                allRespondentInfo[position].provience, allRespondentInfo[position].district,
                allRespondentInfo[position].municipality, allRespondentInfo[position].wardNumber)
        holder.tvDate.text = allRespondentInfo[position].entryDate
        if(allRespondentInfo[position].status == "synced"){
            holder.btnSync.isEnabled = false
            holder.btnSync.text = "Synced"
        } else {
            holder.btnSync.isEnabled = true
            holder.btnSync.text = "Sync"
        }
        holder.btnSync.setOnClickListener {
            onRespondentDetailClickListener?.onSycnClicked(position)
        }
        holder.itemView.setOnClickListener {
            onRespondentDetailClickListener?.onItemClicked(position)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
        val btnSync: MaterialButton = itemView.findViewById(R.id.btnSync)
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
    }
}
