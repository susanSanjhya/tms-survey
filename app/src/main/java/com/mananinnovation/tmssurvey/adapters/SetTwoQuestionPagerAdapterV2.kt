package com.mananinnovation.tmssurvey.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mananinnovation.tmssurvey.fragments.KEY_RESPONDENT_ID
import com.mananinnovation.tmssurvey.fragments.TestFragment
import com.mananinnovation.tmssurvey.fragmentsTwo.*
import com.mananinnovation.tmssurvey.models.Question
import java.util.*

class SetTwoQuestionPagerAdapterV2(private val respondentId: Int, private val questionList: ArrayList<Question>, fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        val question = questionList[position]
        return when (question.id) {
            28 -> {
                QuestionOneFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionOneFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }

            }

            29 -> {
                QuestionTwoFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwoFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }

            }


            30 -> {
                QuestionThreeFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionThreeFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }

            }

            31 -> {
                QuestionFourFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            32 -> {
                QuestionFourPointOneIFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourPointOneIFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            33 -> {
                QuestionFourPointOneIiFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourPointOneIiFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            34 -> {
                QuestionFourPointTwoFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourPointTwoFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            35 -> {
                QuestionFiveFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFiveFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            36 -> {
                QuestionSixFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSixFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            37 -> {
                QuestionSevenFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSevenFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            38 -> {
                QuestionEightFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionEightFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            39 -> {
                QuestionNineFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionNineFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            40 -> {
                QuestionTenFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTenFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            41 -> {
                QuestionElevenFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionElevenFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            43 -> {
                QuestionThirteenFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionThirteenFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            42 -> {
                QuestionTwelveFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwelveFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            44 -> {
                QuestionFourteenFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourteenFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            else -> {
                TestFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(TestFragment.KEY_QUESTION, questionList[position])
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

        }
    }

    override fun getCount(): Int = questionList.size

}
