package com.mananinnovation.tmssurvey.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mananinnovation.tmssurvey.fragments.*
import com.mananinnovation.tmssurvey.models.Question
import java.util.*

class SetOneQuestionPagerAdapterV2(private val respondentId: Int, private val questionList: ArrayList<Question>, fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        val question = questionList[position]
        return when (question.id) {
            1 -> {
                QuestionOneFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionOneFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }

            }
            2 -> {
                QuestionTwoFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwoFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            3 -> {
                QuestionThreeFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionThreeFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            4 -> {
                QuestionFourFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            5 -> {
                QuestionFiveFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFiveFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            6 -> {
                QuestionFivePointOneIV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFivePointOneIV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            7 -> {
                QuestionFivePointOneIiV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFivePointOneIiV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            8 -> {
                QuestionFivePointTwoV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFivePointTwoV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            9 -> {
                QuestionSixFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSixFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            10 -> {
                QuestionSevenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSevenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            11 -> {
                QuestionEightFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionEightFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            12 -> {
                QuestionNineFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionNineFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }
            13 -> {
                QuestionTenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            14 -> {
                QuestionElevenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionElevenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            15 -> {
                QuestionTwelveFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwelveFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            16 -> {
                QuestionThirteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionThirteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            17 -> {
                QuestionFourteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFourteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            18 -> {
                QuestionFifteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionFifteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            19 -> {
                QuestionSixteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSixteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            20 -> {
                QuestionSeventeenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSeventeenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            21 -> {
                QuestionSeventeenPointOneFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSeventeenPointOneFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            22 -> {
                QuestionSeventeenPointTwoFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionSeventeenPointTwoFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            23 -> {
                QuestionEighteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionEighteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            24 -> {
                QuestionNineteenFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionNineteenFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            25 -> {
                QuestionTwentyFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwentyFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            26 -> {
                QuestionTwentyOneFragmentV2()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwentyOneFragmentV2.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

            27 -> {
                QuestionTwentyTwoFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(QuestionTwentyTwoFragment.KEY_QUESTION, question)
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }


            else -> {
                TestFragment()
                        .apply {
                            arguments = Bundle()
                                    .apply {
                                        putParcelable(TestFragment.KEY_QUESTION, questionList[position])
                                        putInt(KEY_RESPONDENT_ID, respondentId)
                                    }
                        }
            }

        }
    }

    override fun getCount(): Int = questionList.size

}
