package com.mananinnovation.tmssurvey.controllers

import android.os.AsyncTask
import com.mananinnovation.tmssurvey.protocols.OnResponseCallback
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class UserDetailController(private val onResponseCallback: OnResponseCallback) :
    AsyncTask<String, Void, String>() {
    override fun onPreExecute() {
        onResponseCallback.onResponseStarted()
    }

    override fun doInBackground(vararg params: String): String {
        return try {
            val url = URL("https://survey-ez.herokuapp.com/api/v1/users/user_info?access_token=${params[0]}")
//            val url = URL("http://192.168.50.43:3000/api/v1/users/user_info?access_token=${params[0]}")
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.setRequestProperty("Accept", "application/json")
            connection.connect()

            return if (connection.responseCode == 200) {
                val reader = BufferedReader(InputStreamReader(connection.inputStream))
                val builder = StringBuilder()
                var line: String?
                do {
                    line = reader.readLine()
                    if (line != null) {
                        builder.append(line)
                    }
                } while (line != null)
                builder.toString()
            } else {
                val reader = BufferedReader(InputStreamReader(connection.errorStream))
                val builder = StringBuilder()
                var line: String?
                do {
                    line = reader.readLine()
                    if (line != null) {
                        builder.append(line)
                    }
                } while (line != null)
                builder.toString()
            }
        } catch (e: Exception) {
            return "User detail error: ${e.localizedMessage}"
        }
    }

    override fun onPostExecute(result: String) {
        onResponseCallback.onResponseComplete(result)
    }
}