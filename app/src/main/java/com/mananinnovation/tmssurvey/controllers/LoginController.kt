package com.mananinnovation.tmssurvey.controllers

import android.os.AsyncTask
import com.mananinnovation.tmssurvey.protocols.OnResponseCallback
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class LoginController(private val onLoginCallback: OnResponseCallback) :
    AsyncTask<String, Void, String>() {

    override fun onPreExecute() {
        onLoginCallback.onResponseStarted()
    }

    override fun doInBackground(vararg params: String): String {
        return try {
            val url = URL("https://survey-ez.herokuapp.com/oauth/token")
//            val url = URL("http://192.168.50.43:3000/oauth/token")
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection.addRequestProperty("Content-Type", "application/json")
            connection.addRequestProperty("Accept", "application/json")

            val dos = DataOutputStream(connection.outputStream)
            dos.writeBytes(params[0])
            dos.flush()
            dos.close()

            connection.connect()

            return if (connection.responseCode == 200) {
                val reader = BufferedReader(InputStreamReader(connection.inputStream))
                val builder = StringBuilder()
                var line: String?
                do {
                    line = reader.readLine()
                    if (line != null) {
                        builder.append(line)
                    }
                } while (line != null)
                builder.toString()
            } else {
                val reader = BufferedReader(InputStreamReader(connection.errorStream))
                val builder = StringBuilder()
                var line: String?
                do {
                    line = reader.readLine()
                    if (line != null) {
                        builder.append(line)
                    }
                } while (line != null)
                builder.toString()
            }
        } catch (e: Exception) {
            "Login error: ${e.localizedMessage}"
        }
    }

    override fun onPostExecute(result: String) {
        onLoginCallback.onResponseComplete(result)
    }

}