package com.mananinnovation.tmssurvey.controllers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Base64
import com.mananinnovation.tmssurvey.protocols.OnAnswerPostListener
import com.mananinnovation.tmssurvey.utils.log
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class AnswerPostController(private val context: Context, private val accessToken: String, private val imageFileName: String?, private val onAnswerPostListener: OnAnswerPostListener) : AsyncTask<String, Void, Void>() {
    override fun onPreExecute() {
        onAnswerPostListener.onStarted()
    }

    override fun doInBackground(vararg params: String): Void? {
        this@AnswerPostController.javaClass.log("parameters: ${JSONObject(params[0])}")
        val url = URL("http://survey-ez.herokuapp.com/api/v1/answers?access_token=$accessToken")
//        val url = URL("http://192.168.50.43:3000/api/v1/answers?access_token=$accessToken")
        val connection = url.openConnection() as HttpURLConnection
        connection.setRequestProperty("Accept", "application/json")
        connection.setRequestProperty("Content-Type", "application/json")
        connection.requestMethod = "POST"

        val dos = DataOutputStream(connection.outputStream)
        dos.writeBytes(params[0])
        dos.flush()
        dos.close()
        connection.connect()

        if (connection.responseCode == 200) {
            val reader = BufferedReader(InputStreamReader(connection.inputStream))
            val builder = StringBuilder()
            var line: String?
            do {
                line = reader.readLine()
                if (line != null) {
                    builder.append(line)
                }
            } while (line != null)
            this@AnswerPostController.javaClass.log("success response: $builder")
            onAnswerPostListener.onComplete("Sync Complete")
        } else {
            val reader = BufferedReader(InputStreamReader(connection.errorStream))
            val builder = StringBuilder()
            var line: String?
            do {
                line = reader.readLine()
                if (line != null) {
                    builder.append(line)
                }
            } while (line != null)
            this@AnswerPostController.javaClass.log("error response: $builder")
            onAnswerPostListener.onError("Error occured")
        }
        return null
    }
}
