package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.*
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.*
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.btnNext
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.btnPrevious
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.llContainer
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.tvQuestionHeader
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.tvQuestionText
import kotlinx.android.synthetic.main.fragment_question_eleven_v2.view.tvSubHeader
import kotlinx.android.synthetic.main.fragment_question_six_v2.view.*


class QuestionElevenFragmentV2 : Fragment() {
    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()
    private var respondentId = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(QuestionOneFragmentV2.KEY_QUESTION)
        respondentId = arguments?.getInt(KEY_RESPONDENT_ID) ?: 0
        answerDtoList.addAll(AnswerDao(context!!).getAllAnswerInfoByRespondent(respondentId))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_eleven_v2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
                String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }

        generateOptionView(view)

        view.btnNext.setOnClickListener {
            onViewPagerActionListener?.onNextClicked(answerDtoList)
        }
        view.btnPrevious.setOnClickListener {
            onViewPagerActionListener?.onPreviousClick()
        }
    }

    private fun generateOptionView(root: View) {
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        var sNo = 0
        for (i in 0 until question?.subQuestionList?.size!!) {
            val subQuestion = question?.subQuestionList!![i]
            val llQuestion = LinearLayout(context)
            llQuestion.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            llQuestion.orientation = LinearLayout.HORIZONTAL
            llQuestion.weightSum = 100f
            llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

            if (subQuestion.level == 1) {
                sNo++
                val tvSno = TextView(context)
                val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lp.topMargin = dimen1dp
                lp.marginStart = dimen2dp
                lp.marginEnd = dimen1dp
                lp.bottomMargin = dimen1dp
                tvSno.layoutParams = lp
                tvSno.text = String.format("%d", sNo)
                tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvSno.setBackgroundColor(getBackgroundColor())
                tvSno.gravity = Gravity.CENTER
                tvSno.setTextColor(Color.parseColor("#212121"))
                tvSno.setTypeface(null, Typeface.BOLD)
                llQuestion.addView(tvSno)
            } else {
                val emptyView = View(context)
                val lpEmptyView =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpEmptyView.topMargin = dimen1dp
                lpEmptyView.marginStart = dimen2dp
                lpEmptyView.marginEnd = dimen1dp
                lpEmptyView.bottomMargin = dimen1dp
                emptyView.layoutParams = lpEmptyView
                emptyView.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                emptyView.setBackgroundColor(getBackgroundColor())
                llQuestion.addView(emptyView)
            }

            if (subQuestion.answerType == "TextBox") {
                val etQuestion = EditText(context)
                val lpQues =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                lpQues.bottomMargin = dimen1dp
                etQuestion.layoutParams = lpQues
                etQuestion.hint = if (subQuestion.level == 1) {
                    String.format("%s", subQuestion.title)
                } else {
                    Html.fromHtml("• ${subQuestion.title}")
                }
                etQuestion.setLines(5)
                etQuestion.setHintTextColor(Color.parseColor("#212121"))
                etQuestion.setBackgroundColor(getBackgroundColor())
                etQuestion.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                etQuestion.setTextColor(Color.parseColor("#212121"))
                etQuestion.gravity = Gravity.TOP

                etQuestion.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {

                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (p0.isNullOrEmpty()) {
                            var doesExist = false
                            var selectedIndex = 0
                            for (kl in 0 until answerDtoList.size) {
                                val answer = answerDtoList[kl]
                                selectedIndex = kl
                                if (answer.questionnaireId == question?.questionnaireId && answer.principalQuestionId == question?.id && answer.questionId == subQuestion.id) {
                                    doesExist = true
                                    break
                                }
                            }
                            if (doesExist) {
                                val answerDto = answerDtoList.removeAt(selectedIndex)
                                AnswerDao(context!!).delete(answerDto)
                            }
                        } else {
                            var doesExist = false
                            var selectedIndex = 0
                            for (kl in 0 until answerDtoList.size) {
                                val answer = answerDtoList[kl]
                                selectedIndex = kl
                                if (answer.questionnaireId == question?.questionnaireId && answer.principalQuestionId == question?.id && answer.questionId == subQuestion.id) {
                                    doesExist = true
                                    break
                                }
                            }
                            if (doesExist) {
                                val answerDto = answerDtoList.removeAt(selectedIndex)
                                AnswerDao(context!!).delete(answerDto)
                            }
                            answerDtoList.add(AnswerDto()
                                    .apply {
                                        this.questionnaireId = question?.questionnaireId ?: 0
                                        this.principalQuestionId = question?.id ?: 0
                                        this.questionId = subQuestion.id
                                        this.respondentAnswer = p0.toString()
                                        this.questionCategoryId = question?.questionCategoryId ?: 0
                                    }.also {
                                        this@QuestionElevenFragmentV2.javaClass.log("questioion int aklsdjfcategory id: $it")
                                    })
                        }
                    }
                })
                llQuestion.addView(etQuestion)
            } else {
                val tvQues = TextView(context)
                val lpQues =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                lpQues.bottomMargin = dimen1dp
                tvQues.layoutParams = lpQues
                tvQues.text = if (subQuestion.level == 1) {
                    String.format("%s", subQuestion.title)
                } else {
                    Html.fromHtml("• ${subQuestion.title}")
                }
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                tvQues.setTypeface(null, Typeface.BOLD)
                llQuestion.addView(tvQues)
            }

            if (subQuestion.answerOptionList.isEmpty()) {
                val emptyView1 = View(context)
                val lpEmptyView1 =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpEmptyView1.topMargin = dimen1dp
                lpEmptyView1.marginStart = dimen1dp
                lpEmptyView1.marginEnd = dimen1dp
                lpEmptyView1.bottomMargin = dimen1dp
                emptyView1.layoutParams = lpEmptyView1
                emptyView1.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                emptyView1.setBackgroundColor(getBackgroundColor())
                llQuestion.addView(emptyView1)

                val emptyView2 = View(context)
                val lpEmptyView2 =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpEmptyView2.topMargin = dimen1dp
                lpEmptyView2.marginStart = dimen1dp
                lpEmptyView2.marginEnd = dimen2dp
                lpEmptyView2.bottomMargin = dimen1dp
                emptyView2.layoutParams = lpEmptyView2
                emptyView2.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                emptyView2.setBackgroundColor(getBackgroundColor())
                llQuestion.addView(emptyView2)


            } else {
                val rbYes = RadioButton(context)
                val rbNo = RadioButton(context)

                val lpOptionYes =
                        LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                        )
                lpOptionYes.topMargin = dimen1dp
                lpOptionYes.marginStart = dimen1dp
                lpOptionYes.marginEnd = dimen1dp
                lpOptionYes.bottomMargin = dimen1dp
                rbYes.layoutParams = lpOptionYes
                rbYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                rbYes.gravity = Gravity.CENTER
                rbYes.setBackgroundColor(getBackgroundColor())
                rbYes.setTextColor(Color.parseColor("#212121"))
                rbYes.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) {
                        rbNo.isChecked = false
                        var doesExist = false
                        var selectedIndex = 0
                        for (llk in 0 until answerDtoList.size) {
                            val answer = answerDtoList[llk]
                            selectedIndex = llk
                            this@QuestionElevenFragmentV2.javaClass.log("respondent id: $respondentId")
                            this@QuestionElevenFragmentV2.javaClass.log("from list id: ${answer.respondentId}")
                            if (respondentId == answer.respondentId && answer.questionId == subQuestion.id) {
                                this@QuestionElevenFragmentV2.javaClass.log("does exist")
                                doesExist = true
                                break
                            }
                        }
                        if (doesExist) {
                            val answerDto = answerDtoList.removeAt(selectedIndex)
                            AnswerDao(context!!).delete(answerDto)
                        }
                        answerDtoList.add(AnswerDto()
                                .apply {
                                    this.respondentId = this@QuestionElevenFragmentV2.respondentId
                                    this.questionnaireId = question?.questionnaireId ?: 0
                                    this.principalQuestionId = question?.id ?: 0
                                    this.questionId = subQuestion.id
                                    this.answerOptionId = subQuestion.answerOptionList[0].id
                                    this.questionCategoryId = question?.questionCategoryId ?: 0
                                })
                        this@QuestionElevenFragmentV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
                    }
                }
                llQuestion.addView(rbYes)

                val lpOptionNo =
                        LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                        )
                lpOptionNo.topMargin = dimen1dp
                lpOptionNo.marginStart = dimen1dp
                lpOptionNo.marginEnd = dimen2dp
                lpOptionNo.bottomMargin = dimen1dp
                rbNo.layoutParams = lpOptionNo
                rbNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                rbNo.gravity = Gravity.CENTER
                rbNo.setBackgroundColor(getBackgroundColor())
                rbNo.setTextColor(Color.parseColor("#212121"))
                rbNo.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) {
                        rbYes.isChecked = false
                        var doesExist = false
                        var selectedIndex = 0
                        for (llk in 0 until answerDtoList.size) {
                            val answer = answerDtoList[llk]
                            selectedIndex = llk
                            this@QuestionElevenFragmentV2.javaClass.log("respondent id: $respondentId")
                            this@QuestionElevenFragmentV2.javaClass.log("from list id: ${answer.respondentId}")
                            if (respondentId == answer.respondentId && answer.questionId == subQuestion.id) {
                                this@QuestionElevenFragmentV2.javaClass.log("does exist")
                                doesExist = true
                                break
                            }
                        }
                        if (doesExist) {
                            val answerDto = answerDtoList.removeAt(selectedIndex)
                            AnswerDao(context!!).delete(answerDto)
                        }
                        answerDtoList.add(AnswerDto()
                                .apply {
                                    this.respondentId = this@QuestionElevenFragmentV2.respondentId
                                    this.questionnaireId = question?.questionnaireId ?: 0
                                    this.principalQuestionId = question?.id ?: 0
                                    this.questionId = subQuestion.id
                                    this.answerOptionId = subQuestion.answerOptionList[1].id
                                    this.questionCategoryId = question?.questionCategoryId ?: 0
                                })
                        this@QuestionElevenFragmentV2.javaClass.log("answer dto list size: ${answerDtoList.size}")

                    }
                }
                llQuestion.addView(rbNo)
            }
            root.llContainer.addView(llQuestion)
        }
    }

    private fun getBackgroundColor(): Int {
        return if (llContainer.childCount % 2 != 0) {
            Color.parseColor("#c4e3ed")
        } else {
            Color.WHITE
        }
    }

}
