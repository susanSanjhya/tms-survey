package com.mananinnovation.tmssurvey.fragments

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.Questions
import kotlinx.android.synthetic.main.fragment_question_one.*
import kotlinx.android.synthetic.main.fragment_question_one.view.*

class QuestionTwoFragment : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Questions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_one, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionHeader
        view.tvQuestionText.text = question?.questionText
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        val headerView = getHeaderView()
        view.llContainer.addView(headerView)
        for (i in 0 until question?.children?.size!!) {
            val subQues = question?.children!![i]
            val llQuestion = LinearLayout(context)
            llQuestion.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            llQuestion.orientation = LinearLayout.HORIZONTAL
            llQuestion.weightSum = 100f
            llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

            val tvSno = TextView(context)
            val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
            lp.topMargin = dimen1dp
            lp.marginStart = dimen2dp
            lp.marginEnd = dimen1dp
            if (i == question?.children?.size!! - 1) {
                lp.bottomMargin = dimen2dp
            } else {
                lp.bottomMargin = dimen1dp
            }
            tvSno.layoutParams = lp
            tvSno.text = String.format("%d", (i + 1))
            tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
            tvSno.setBackgroundColor(getBackgroundColor(llQuestion))
            tvSno.gravity = Gravity.CENTER
            tvSno.setTextColor(Color.parseColor("#212121"))
            llQuestion.addView(tvSno)

            if (subQues.type == "CustomQuestion") {
                val tvQues = EditText(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.hint = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor(llQuestion))
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                tvQues.setLines(10)
                tvQues.gravity = Gravity.TOP
                llQuestion.addView(tvQues)
            } else {
                val tvQues = TextView(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.text = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor(llQuestion))
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                llQuestion.addView(tvQues)
            }

            if (subQues.children.isEmpty()) {
                if (subQues.type == "SingleSelect") {
                    val cbYes = CheckBox(context)
                    val cbNo = CheckBox(context)
                    val lpYes =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpYes.topMargin = dimen1dp
                    lpYes.marginStart = dimen1dp
                    lpYes.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpYes.bottomMargin = dimen2dp
                    } else {
                        lpYes.bottomMargin = dimen1dp
                    }
                    cbYes.layoutParams = lpYes
//                tvYes.text = String.format("Yes")
                    cbYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbYes.gravity = Gravity.CENTER
                    cbYes.setBackgroundColor(getBackgroundColor(llQuestion))
                    cbYes.setTextColor(Color.parseColor("#212121"))
                    cbYes.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbNo.isChecked = false
                        }
                    }
                    llQuestion.addView(cbYes)

                    val lpNo =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpNo.topMargin = dimen1dp
                    lpNo.marginStart = dimen1dp
                    lpNo.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpNo.bottomMargin = dimen2dp
                    } else {
                        lpNo.bottomMargin = dimen1dp
                    }
                    cbNo.layoutParams = lpNo
//                tvNo.text = String.format("No")
                    cbNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbNo.gravity = Gravity.CENTER
                    cbNo.setBackgroundColor(getBackgroundColor(llQuestion))
                    cbNo.setTextColor(Color.parseColor("#212121"))
                    cbNo.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbYes.isChecked = false
                        }
                    }
                    llQuestion.addView(cbNo)
                } else if (subQues.type == "CustomQuestion") {
                    val tvYes = View(context)
                    val lpYes =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpYes.topMargin = dimen1dp
                    lpYes.marginStart = dimen1dp
                    lpYes.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpYes.bottomMargin = dimen2dp
                    } else {
                        lpYes.bottomMargin = dimen1dp
                    }
                    tvYes.layoutParams = lpYes
                    tvYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvYes.setBackgroundColor(
                        getBackgroundColor(llQuestion)
                    )
                    llQuestion.addView(tvYes)

                    val tvNo = View(context)
                    val lpNo =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpNo.topMargin = dimen1dp
                    lpNo.marginStart = dimen1dp
                    lpNo.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpNo.bottomMargin = dimen2dp
                    } else {
                        lpNo.bottomMargin = dimen1dp
                    }
                    tvNo.layoutParams = lpNo
                    tvNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvNo.setBackgroundColor(
                        getBackgroundColor(llQuestion)
                    )
                    tvSno.setTextColor(Color.parseColor("#212121"))
                    llQuestion.addView(tvNo)
                }
            } else {
                val tvYes = View(context)
                val lpYes =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpYes.topMargin = dimen1dp
                lpYes.marginStart = dimen1dp
                lpYes.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpYes.bottomMargin = dimen2dp
                } else {
                    lpYes.bottomMargin = dimen1dp
                }
                tvYes.layoutParams = lpYes
                tvYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvYes.setBackgroundColor(getBackgroundColor(llQuestion))
                llQuestion.addView(tvYes)

                val tvNo = View(context)
                val lpNo = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpNo.topMargin = dimen1dp
                lpNo.marginStart = dimen1dp
                lpNo.marginEnd = dimen2dp
                if (i == question?.children?.size!! - 1) {
                    lpNo.bottomMargin = dimen2dp
                } else {
                    lpNo.bottomMargin = dimen1dp
                }
                tvNo.layoutParams = lpNo
                tvNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvNo.setBackgroundColor(getBackgroundColor(llQuestion))
                tvSno.setTextColor(Color.parseColor("#212121"))
                llQuestion.addView(tvNo)
            }
            view.llContainer.addView(llQuestion)

            if (subQues.children.isNotEmpty()) {
                for (j in 0 until subQues.children.size) {
                    val subSubQues = subQues.children[j]
                    val llSubSub = LinearLayout(context)
                    llSubSub.layoutParams =
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    llSubSub.orientation = LinearLayout.HORIZONTAL
                    llSubSub.weightSum = 100f
                    llSubSub.setBackgroundColor(
                        ContextCompat.getColor(context!!, android.R.color.black)
                    )

                    val viewSnoEmpty = View(context)
                    val lpSnoEmpty =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpSnoEmpty.topMargin = dimen1dp
                    lpSnoEmpty.marginStart = dimen2dp
                    lpSnoEmpty.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpSnoEmpty.bottomMargin = dimen2dp
                    } else {
                        lpSnoEmpty.bottomMargin = dimen1dp
                    }
                    viewSnoEmpty.layoutParams = lpSnoEmpty
                    viewSnoEmpty.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewSnoEmpty.setBackgroundColor(
                        getBackgroundColor(llQuestion)
                    )
                    llSubSub.addView(viewSnoEmpty)

                    val tvOptions = TextView(context)
                    val lpOptions =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
                    lpOptions.topMargin = dimen1dp
                    lpOptions.marginStart = dimen1dp
                    lpOptions.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOptions.bottomMargin = dimen2dp
                    } else {
                        lpOptions.bottomMargin = dimen1dp
                    }
                    tvOptions.layoutParams = lpOptions
                    tvOptions.text = Html.fromHtml(
                        "\u2022 ${subSubQues.questionText}"
                    )
                    tvOptions.setBackgroundColor(
                        getBackgroundColor(llQuestion)
                    )
                    tvOptions.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvOptions.setTextColor(Color.parseColor("#212121"))
                    llSubSub.addView(tvOptions)

                    if (subSubQues.type == "SingleSelect") {
                        val cbOptionYes = CheckBox(context)
                        val cbOptionNo = CheckBox(context)
                        val lpOptionYes =
                            LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                            )
                        lpOptionYes.topMargin = dimen1dp
                        lpOptionYes.marginStart = dimen1dp
                        lpOptionYes.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOptionYes.bottomMargin = dimen2dp
                        } else {
                            lpOptionYes.bottomMargin = dimen1dp
                        }
                        cbOptionYes.layoutParams = lpOptionYes
                        cbOptionYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbOptionYes.gravity = Gravity.CENTER
                        cbOptionYes.setBackgroundColor(
                            getBackgroundColor(llQuestion)
                        )
                        cbOptionYes.setTextColor(Color.parseColor("#212121"))
                        cbOptionYes.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOptionNo.isChecked = false
                            }
                        }
                        llSubSub.addView(cbOptionYes)

                        val lpOptionNo =
                            LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                            )
                        lpOptionNo.topMargin = dimen1dp
                        lpOptionNo.marginStart = dimen1dp
                        lpOptionNo.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpOptionNo.bottomMargin = dimen2dp
                        } else {
                            lpOptionNo.bottomMargin = dimen1dp
                        }
                        cbOptionNo.layoutParams = lpOptionNo
                        cbOptionNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbOptionNo.gravity = Gravity.CENTER
                        cbOptionNo.setBackgroundColor(
                            getBackgroundColor(llQuestion)
                        )
                        cbOptionNo.setTextColor(Color.parseColor("#212121"))
                        cbOptionNo.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOptionYes.isChecked = false
                            }
                        }
                        llSubSub.addView(cbOptionNo)
                    } else {
                        val viewEmptyOptionYes = View(context)
                        val lpEmptyOptionYes =
                            LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                            )
                        lpEmptyOptionYes.topMargin = dimen1dp
                        lpEmptyOptionYes.marginStart = dimen1dp
                        lpEmptyOptionYes.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpEmptyOptionYes.bottomMargin = dimen2dp
                        } else {
                            lpEmptyOptionYes.bottomMargin = dimen1dp
                        }
                        viewEmptyOptionYes.layoutParams = lpEmptyOptionYes
                        viewEmptyOptionYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewEmptyOptionYes.setBackgroundColor(
                            getBackgroundColor(llQuestion)
                        )
                        llSubSub.addView(viewEmptyOptionYes)

                        val viewEmptyOptionNo = View(context)
                        val lpEmptyOptionNo = LinearLayout.LayoutParams(
                            0,
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            15f
                        )
                        lpEmptyOptionNo.topMargin = dimen1dp
                        lpEmptyOptionNo.marginStart = dimen1dp
                        lpEmptyOptionNo.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpEmptyOptionNo.bottomMargin = dimen2dp
                        } else {
                            lpEmptyOptionNo.bottomMargin = dimen1dp
                        }
                        viewEmptyOptionNo.layoutParams = lpEmptyOptionNo
                        viewEmptyOptionNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewEmptyOptionNo.setBackgroundColor(
                            getBackgroundColor(llQuestion)
                        )
                        tvSno.setTextColor(Color.parseColor("#212121"))
                        llSubSub.addView(viewEmptyOptionNo)
                    }

                    view.llContainer.addView(llSubSub)
                }
            }
        }
    }

    private fun getHeaderView(): View {
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        val llQuestion = LinearLayout(context)
        llQuestion.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llQuestion.orientation = LinearLayout.HORIZONTAL
        llQuestion.weightSum = 100f
        llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

        val tvSno = TextView(context)
        val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
        lp.topMargin = dimen2dp
        lp.marginStart = dimen2dp
        lp.marginEnd = dimen1dp
        lp.bottomMargin = dimen1dp
        tvSno.layoutParams = lp
        tvSno.text = String.format("S.No.")
        tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvSno.gravity = Gravity.CENTER
        tvSno.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvSno.setTextColor(Color.parseColor("#212121"))
        tvSno.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvSno)


        val tvQues = TextView(context)
        val lpQues = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 55f)
        lpQues.topMargin = dimen2dp
        lpQues.marginStart = dimen1dp
        lpQues.marginEnd = dimen1dp
        lpQues.bottomMargin = dimen1dp
        tvQues.layoutParams = lpQues
        tvQues.text = String.format("Types of Services")
        tvQues.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvQues.gravity = Gravity.CENTER
        tvQues.setTextColor(Color.parseColor("#212121"))
        tvQues.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvQues)

        val tvYes = TextView(context)
        val lpYes = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
        lpYes.topMargin = dimen2dp
        lpYes.marginStart = dimen1dp
        lpYes.marginEnd = dimen1dp
        lpYes.bottomMargin = dimen1dp
        tvYes.layoutParams = lpYes
        tvYes.text = String.format("Yes")
        tvYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvYes.gravity = Gravity.CENTER
        tvYes.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvYes.setTextColor(Color.parseColor("#212121"))
        tvYes.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvYes)

        val tvNo = TextView(context)
        val lpNo = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
        lpNo.topMargin = dimen2dp
        lpNo.marginStart = dimen1dp
        lpNo.marginEnd = dimen2dp
        lpNo.bottomMargin = dimen1dp
        tvNo.layoutParams = lpNo
        tvNo.text = String.format("No")
        tvNo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvNo.gravity = Gravity.CENTER
        tvNo.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvNo.setTextColor(Color.parseColor("#212121"))
        tvNo.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvNo)
        return llQuestion
    }

    private fun getBackgroundColor(container: LinearLayout): Int {
        return if (llContainer.childCount % 2 != 0) {
            Color.parseColor("#c4e3ed")
        } else {
            Color.WHITE
        }
    }

}
