/*
package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_quetion_three.*
import kotlinx.android.synthetic.main.fragment_quetion_three.view.*

class QuestionFiveFragment : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()
    private var respondentId = 0

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(QuestionOneFragmentV2.KEY_QUESTION)
        respondentId = arguments?.getInt(KEY_RESPONDENT_ID) ?: 0
        answerDtoList.addAll(AnswerDao(context!!).getAllAnswerInfoByRespondent(respondentId))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_quetion_three, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionHeader
        view.tvQuestionText.text = question?.questionText
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        for (i in 0 until question?.children?.size!!) {
            val subQuestion = question?.children!![i]
            val llQuestion = LinearLayout(context)
            llQuestion.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            llQuestion.orientation = LinearLayout.HORIZONTAL
            llQuestion.weightSum = 100f
            llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

            val tvSno = TextView(context)
            val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
            lp.topMargin = dimen1dp
            lp.marginStart = dimen2dp
            lp.marginEnd = dimen1dp
            if (i == question?.children?.size!! - 1) {
                lp.bottomMargin = dimen1dp
            } else {
                lp.bottomMargin = dimen1dp
            }
            tvSno.layoutParams = lp
            tvSno.text = String.format("%d", (i + 1))
            tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
            tvSno.setBackgroundColor(getBackgroundColor())
            tvSno.gravity = Gravity.CENTER
            tvSno.setTextColor(Color.parseColor("#212121"))
            llQuestion.addView(tvSno)

            if (subQuestion.type == "CustomQuestion") {
                val tvQues = EditText(context)
                val lpQues =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen1dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.hint = String.format("%s", subQuestion.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                tvQues.setLines(10)
                tvQues.gravity = Gravity.TOP
                llQuestion.addView(tvQues)
            } else {
                val tvQues = TextView(context)
                val lpQues =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen1dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.text = String.format("%s", subQuestion.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                llQuestion.addView(tvQues)
            }

            if (subQuestion.children.isEmpty()) {
                if (subQuestion.type == "SingleSelect") {
                    val cbOne = CheckBox(context)
                    val cbTwo = CheckBox(context)
                    val cbThree = CheckBox(context)
                    val cbFour = CheckBox(context)
                    val cbFive = CheckBox(context)

                    val lpOne =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpOne.topMargin = dimen1dp
                    lpOne.marginStart = dimen1dp
                    lpOne.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOne.bottomMargin = dimen1dp
                    } else {
                        lpOne.bottomMargin = dimen1dp
                    }
                    cbOne.layoutParams = lpOne
//                tvYes.text = String.format("Yes")
                    cbOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbOne.gravity = Gravity.CENTER
                    cbOne.setBackgroundColor(getBackgroundColor())
                    cbOne.setTextColor(Color.parseColor("#212121"))
                    cbOne.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                            var doesExist = false
                            var selectedIndex = 0
                            for (llk in 0 until answerDtoList.size) {
                                val answer = answerDtoList[llk]
                                selectedIndex = llk
                                this@QuestionFiveFragment.javaClass.log("respondent id: $respondentId")
                                this@QuestionFiveFragment.javaClass.log("from list id: ${answer.respondentId}")

                                if (respondentId == answer.respondentId && answer.questionId == subQuestion.id) {
                                    this@QuestionFiveFragment.javaClass.log("does exist")
                                    doesExist = true
                                    break
                                }
                            }
                            if (doesExist) {
                                val answerDto = answerDtoList.removeAt(selectedIndex)
                                AnswerDao(context!!).delete(answerDto)
                            }
                            answerDtoList.add(AnswerDto()
                                    .apply {
                                        this.respondentId = this@QuestionFiveFragment.respondentId
                                        this.questionnaireId = question?.questionnaireId ?: 0
                                        this.principalQuestionId = question?.id ?: 0
                                        this.questionId = subQuestion.id
                                        this.answerOptionId = subQuestion.answerOptionList[0].id
                                        this.questionCategoryId = question?.questionCategoryId ?: 0
                                    })
                            this@QuestionFiveFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
                        } else {
                            var doesExist = false
                            var selectedIndex = 0
                            for (llk in 0 until answerDtoList.size) {
                                val answer = answerDtoList[llk]
                                selectedIndex = llk
                                this@QuestionFiveFragment.javaClass.log("respondent id: $respondentId")
                                this@QuestionFiveFragment.javaClass.log("from list id: ${answer.respondentId}")
                                if (respondentId == answer.respondentId && answer.questionId == subQuestion.id) {
                                    this@QuestionFiveFragment.javaClass.log("does exist")
                                    doesExist = true
                                    break
                                }
                            }
                            if (doesExist) {
                                val answerDto = answerDtoList.removeAt(selectedIndex)
                                AnswerDao(context!!).delete(answerDto)
                            }
                        }
                    }

                    llQuestion.addView(cbOne)

                    val lpTwo =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpTwo.topMargin = dimen1dp
                    lpTwo.marginStart = dimen1dp
                    lpTwo.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpTwo.bottomMargin = dimen1dp
                    } else {
                        lpTwo.bottomMargin = dimen1dp
                    }
                    cbTwo.layoutParams = lpTwo
//                tvNo.text = String.format("No")
                    cbTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbTwo.gravity = Gravity.CENTER
                    cbTwo.setBackgroundColor(getBackgroundColor())
                    cbTwo.setTextColor(Color.parseColor("#212121"))
                    cbTwo.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbTwo)

                    val lpThree =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpThree.topMargin = dimen1dp
                    lpThree.marginStart = dimen1dp
                    lpThree.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpThree.bottomMargin = dimen1dp
                    } else {
                        lpThree.bottomMargin = dimen1dp
                    }
                    cbThree.layoutParams = lpThree
//                tvNo.text = String.format("No")
                    cbThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbThree.gravity = Gravity.CENTER
                    cbThree.setBackgroundColor(getBackgroundColor())
                    cbThree.setTextColor(Color.parseColor("#212121"))
                    cbThree.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbThree)

                    val lpFour =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFour.topMargin = dimen1dp
                    lpFour.marginStart = dimen1dp
                    lpFour.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpFour.bottomMargin = dimen1dp
                    } else {
                        lpFour.bottomMargin = dimen1dp
                    }
                    cbFour.layoutParams = lpFour
//                tvNo.text = String.format("No")
                    cbFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbFour.gravity = Gravity.CENTER
                    cbFour.setBackgroundColor(getBackgroundColor())
                    cbFour.setTextColor(Color.parseColor("#212121"))
                    cbFour.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbFour)

                    val lpFive =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFive.topMargin = dimen1dp
                    lpFive.marginStart = dimen1dp
                    lpFive.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpFive.bottomMargin = dimen1dp
                    } else {
                        lpFive.bottomMargin = dimen1dp
                    }
                    cbFive.layoutParams = lpFive
//                tvNo.text = String.format("No")
                    cbFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbFive.gravity = Gravity.CENTER
                    cbFive.setBackgroundColor(getBackgroundColor())
                    cbFive.setTextColor(Color.parseColor("#212121"))
                    cbFive.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                        }
                    }
                    llQuestion.addView(cbFive)
                } else if (subQuestion.type == "CustomQuestion") {
                    val viewOne = View(context)
                    val lpOne =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpOne.topMargin = dimen1dp
                    lpOne.marginStart = dimen1dp
                    lpOne.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOne.bottomMargin = dimen1dp
                    } else {
                        lpOne.bottomMargin = dimen1dp
                    }
                    viewOne.layoutParams = lpOne
                    viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewOne.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llQuestion.addView(viewOne)

                    val viewTwo = View(context)
                    val lpTwo =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpTwo.topMargin = dimen1dp
                    lpTwo.marginStart = dimen1dp
                    lpTwo.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpTwo.bottomMargin = dimen1dp
                    } else {
                        lpTwo.bottomMargin = dimen1dp
                    }
                    viewTwo.layoutParams = lpTwo
                    viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewTwo.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llQuestion.addView(viewTwo)

                    val viewThree = View(context)
                    val lpThree =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpThree.topMargin = dimen1dp
                    lpThree.marginStart = dimen1dp
                    lpThree.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpThree.bottomMargin = dimen1dp
                    } else {
                        lpThree.bottomMargin = dimen1dp
                    }
                    viewThree.layoutParams = lpThree
                    viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewThree.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llQuestion.addView(viewThree)

                    val viewFour = View(context)
                    val lpFour =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFour.topMargin = dimen1dp
                    lpFour.marginStart = dimen1dp
                    lpFour.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpFour.bottomMargin = dimen1dp
                    } else {
                        lpFour.bottomMargin = dimen1dp
                    }
                    viewFour.layoutParams = lpFour
                    viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewFour.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llQuestion.addView(viewFour)

                    val viewFive = View(context)
                    val lpFive =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFive.topMargin = dimen1dp
                    lpFive.marginStart = dimen1dp
                    lpFive.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpFive.bottomMargin = dimen1dp
                    } else {
                        lpFive.bottomMargin = dimen1dp
                    }
                    viewFive.layoutParams = lpFive
                    viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewFive.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llQuestion.addView(viewFive)
                }
            } else {
                val viewOne = View(context)
                val lpOne =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpOne.topMargin = dimen1dp
                lpOne.marginStart = dimen1dp
                lpOne.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpOne.bottomMargin = dimen1dp
                } else {
                    lpOne.bottomMargin = dimen1dp
                }
                viewOne.layoutParams = lpOne
                viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewOne.setBackgroundColor(
                        getBackgroundColor()
                )
                llQuestion.addView(viewOne)

                val viewTwo = View(context)
                val lpTwo =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpTwo.topMargin = dimen1dp
                lpTwo.marginStart = dimen1dp
                lpTwo.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpTwo.bottomMargin = dimen1dp
                } else {
                    lpTwo.bottomMargin = dimen1dp
                }
                viewTwo.layoutParams = lpTwo
                viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewTwo.setBackgroundColor(
                        getBackgroundColor()
                )
                llQuestion.addView(viewTwo)

                val viewThree = View(context)
                val lpThree =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpThree.topMargin = dimen1dp
                lpThree.marginStart = dimen1dp
                lpThree.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpThree.bottomMargin = dimen1dp
                } else {
                    lpThree.bottomMargin = dimen1dp
                }
                viewThree.layoutParams = lpThree
                viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewThree.setBackgroundColor(
                        getBackgroundColor()
                )
                llQuestion.addView(viewThree)

                val viewFour = View(context)
                val lpFour =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpFour.topMargin = dimen1dp
                lpFour.marginStart = dimen1dp
                lpFour.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpFour.bottomMargin = dimen1dp
                } else {
                    lpFour.bottomMargin = dimen1dp
                }
                viewFour.layoutParams = lpFour
                viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewFour.setBackgroundColor(
                        getBackgroundColor()
                )
                llQuestion.addView(viewFour)

                val viewFive = View(context)
                val lpFive =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpFive.topMargin = dimen1dp
                lpFive.marginStart = dimen1dp
                lpFive.marginEnd = dimen2dp
                if (i == question?.children?.size!! - 1) {
                    lpFive.bottomMargin = dimen1dp
                } else {
                    lpFive.bottomMargin = dimen1dp
                }
                viewFive.layoutParams = lpFive
                viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewFive.setBackgroundColor(
                        getBackgroundColor()
                )
                llQuestion.addView(viewFive)
            }
            view.llContainer.addView(llQuestion)

            if (subQuestion.children.isNotEmpty()) {
                for (j in 0 until subQuestion.children.size) {
                    val subSubQues = subQuestion.children[j]
                    val llSubSub = LinearLayout(context)
                    llSubSub.layoutParams =
                            LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                            )
                    llSubSub.orientation = LinearLayout.HORIZONTAL
                    llSubSub.weightSum = 100f
                    llSubSub.setBackgroundColor(
                            ContextCompat.getColor(context!!, android.R.color.black)
                    )

                    val viewSnoEmpty = View(context)
                    val lpSnoEmpty =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
                    lpSnoEmpty.topMargin = dimen1dp
                    lpSnoEmpty.marginStart = dimen2dp
                    lpSnoEmpty.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpSnoEmpty.bottomMargin = dimen1dp
                    } else {
                        lpSnoEmpty.bottomMargin = dimen1dp
                    }
                    viewSnoEmpty.layoutParams = lpSnoEmpty
                    viewSnoEmpty.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewSnoEmpty.setBackgroundColor(
                            getBackgroundColor()
                    )
                    llSubSub.addView(viewSnoEmpty)

                    if (subSubQues.type == "SingleSelect") {
                        val tvOptions = TextView(context)
                        val lpOptions =
                                LinearLayout.LayoutParams(
                                        0,
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        48f
                                )
                        lpOptions.topMargin = dimen1dp
                        lpOptions.marginStart = dimen1dp
                        lpOptions.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOptions.bottomMargin = dimen1dp
                        } else {
                            lpOptions.bottomMargin = dimen1dp
                        }
                        tvOptions.layoutParams = lpOptions
                        tvOptions.text = Html.fromHtml(
                                "\u2022 ${subSubQues.questionText}"
                        )
                        tvOptions.setBackgroundColor(
                                getBackgroundColor()
                        )
                        tvOptions.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        tvOptions.setTextColor(Color.parseColor("#212121"))
                        llSubSub.addView(tvOptions)
                    } else {
                        val tvOptions = EditText(context)
                        val lpOptions =
                                LinearLayout.LayoutParams(
                                        0,
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        48f
                                )
                        lpOptions.topMargin = dimen1dp
                        lpOptions.marginStart = dimen1dp
                        lpOptions.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOptions.bottomMargin = dimen1dp
                        } else {
                            lpOptions.bottomMargin = dimen1dp
                        }
                        tvOptions.layoutParams = lpOptions
                        tvOptions.hint = Html.fromHtml(
                                "\u2022 ${subSubQues.questionText}"
                        )
                        tvOptions.setBackgroundColor(
                                getBackgroundColor()
                        )
                        tvOptions.gravity = Gravity.TOP
                        tvOptions.setLines(5)
                        tvOptions.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        tvOptions.setTextColor(Color.parseColor("#212121"))
                        llSubSub.addView(tvOptions)
                    }

                    if (subSubQues.type == "SingleSelect") {
                        val cbOne = CheckBox(context)
                        val cbTwo = CheckBox(context)
                        val cbThree = CheckBox(context)
                        val cbFour = CheckBox(context)
                        val cbFive = CheckBox(context)

                        val lpOne =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpOne.topMargin = dimen1dp
                        lpOne.marginStart = dimen1dp
                        lpOne.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOne.bottomMargin = dimen1dp
                        } else {
                            lpOne.bottomMargin = dimen1dp
                        }
                        cbOne.layoutParams = lpOne
//                tvYes.text = String.format("Yes")
                        cbOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbOne.gravity = Gravity.CENTER
                        cbOne.setBackgroundColor(getBackgroundColor())
                        cbOne.setTextColor(Color.parseColor("#212121"))
                        cbOne.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbOne)

                        val lpTwo =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpTwo.topMargin = dimen1dp
                        lpTwo.marginStart = dimen1dp
                        lpTwo.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpTwo.bottomMargin = dimen1dp
                        } else {
                            lpTwo.bottomMargin = dimen1dp
                        }
                        cbTwo.layoutParams = lpTwo
//                tvNo.text = String.format("No")
                        cbTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbTwo.gravity = Gravity.CENTER
                        cbTwo.setBackgroundColor(getBackgroundColor())
                        cbTwo.setTextColor(Color.parseColor("#212121"))
                        cbTwo.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbTwo)

                        val lpThree =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpThree.topMargin = dimen1dp
                        lpThree.marginStart = dimen1dp
                        lpThree.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpThree.bottomMargin = dimen1dp
                        } else {
                            lpThree.bottomMargin = dimen1dp
                        }
                        cbThree.layoutParams = lpThree
//                tvNo.text = String.format("No")
                        cbThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbThree.gravity = Gravity.CENTER
                        cbThree.setBackgroundColor(getBackgroundColor())
                        cbThree.setTextColor(Color.parseColor("#212121"))
                        cbThree.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbThree)

                        val lpFour =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFour.topMargin = dimen1dp
                        lpFour.marginStart = dimen1dp
                        lpFour.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpFour.bottomMargin = dimen1dp
                        } else {
                            lpFour.bottomMargin = dimen1dp
                        }
                        cbFour.layoutParams = lpFour
//                tvNo.text = String.format("No")
                        cbFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbFour.gravity = Gravity.CENTER
                        cbFour.setBackgroundColor(getBackgroundColor())
                        cbFour.setTextColor(Color.parseColor("#212121"))
                        cbFour.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbFour)

                        val lpFive =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFive.topMargin = dimen1dp
                        lpFive.marginStart = dimen1dp
                        lpFive.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpFive.bottomMargin = dimen1dp
                        } else {
                            lpFive.bottomMargin = dimen1dp
                        }
                        cbFive.layoutParams = lpFive
//                tvNo.text = String.format("No")
                        cbFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbFive.gravity = Gravity.CENTER
                        cbFive.setBackgroundColor(getBackgroundColor())
                        cbFive.setTextColor(Color.parseColor("#212121"))
                        cbFive.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                            }
                        }
                        llSubSub.addView(cbFive)
                    } else {
                        val viewOne = View(context)
                        val lpOne =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpOne.topMargin = dimen1dp
                        lpOne.marginStart = dimen1dp
                        lpOne.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOne.bottomMargin = dimen1dp
                        } else {
                            lpOne.bottomMargin = dimen1dp
                        }
                        viewOne.layoutParams = lpOne
                        viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewOne.setBackgroundColor(
                                getBackgroundColor()
                        )
                        llSubSub.addView(viewOne)

                        val viewTwo = View(context)
                        val lpTwo =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpTwo.topMargin = dimen1dp
                        lpTwo.marginStart = dimen1dp
                        lpTwo.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpTwo.bottomMargin = dimen1dp
                        } else {
                            lpTwo.bottomMargin = dimen1dp
                        }
                        viewTwo.layoutParams = lpTwo
                        viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewTwo.setBackgroundColor(
                                getBackgroundColor()
                        )
                        llSubSub.addView(viewTwo)

                        val viewThree = View(context)
                        val lpThree =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpThree.topMargin = dimen1dp
                        lpThree.marginStart = dimen1dp
                        lpThree.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpThree.bottomMargin = dimen1dp
                        } else {
                            lpThree.bottomMargin = dimen1dp
                        }
                        viewThree.layoutParams = lpThree
                        viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewThree.setBackgroundColor(
                                getBackgroundColor()
                        )
                        llSubSub.addView(viewThree)

                        val viewFour = View(context)
                        val lpFour =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFour.topMargin = dimen1dp
                        lpFour.marginStart = dimen1dp
                        lpFour.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpFour.bottomMargin = dimen1dp
                        } else {
                            lpFour.bottomMargin = dimen1dp
                        }
                        viewFour.layoutParams = lpFour
                        viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewFour.setBackgroundColor(
                                getBackgroundColor()
                        )
                        llSubSub.addView(viewFour)

                        val viewFive = View(context)
                        val lpFive =
                                LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFive.topMargin = dimen1dp
                        lpFive.marginStart = dimen1dp
                        lpFive.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpFive.bottomMargin = dimen1dp
                        } else {
                            lpFive.bottomMargin = dimen1dp
                        }
                        viewFive.layoutParams = lpFive
                        viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewFive.setBackgroundColor(
                                getBackgroundColor()
                        )
                        llSubSub.addView(viewFive)
                    }

                    view.llContainer.addView(llSubSub)
                }
            }
        }
    }

    private fun getBackgroundColor(): Int {
        return if (llContainer.childCount % 2 != 0) {
            Color.parseColor("#c4e3ed")
        } else {
            Color.WHITE
        }
    }

}
*/
