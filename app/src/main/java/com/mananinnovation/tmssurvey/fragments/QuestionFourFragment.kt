package com.mananinnovation.tmssurvey.fragments

import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.Questions
import kotlinx.android.synthetic.main.fragment_question_four.*
import kotlinx.android.synthetic.main.fragment_question_four.view.*

class QuestionFourFragment : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Questions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_four, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionHeader
        view.tvQuestionText.text = question?.questionText
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)


        for (i in 0 until question?.children?.size!!) {
            val subQues = question?.children!![i]
            val llQuestion = LinearLayout(context)
            llQuestion.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            llQuestion.orientation = LinearLayout.HORIZONTAL
            llQuestion.weightSum = 100f
            llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

            val tvSno = TextView(context)
            val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
            lp.topMargin = dimen1dp
            lp.marginStart = dimen2dp
            lp.marginEnd = dimen1dp
            if (i == question?.children?.size!! - 1) {
                lp.bottomMargin = dimen2dp
            } else {
                lp.bottomMargin = dimen1dp
            }
            tvSno.layoutParams = lp
            tvSno.text = String.format("%d", (i + 1))
            tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
            tvSno.setBackgroundColor(getBackgroundColor())
            tvSno.gravity = Gravity.CENTER
            tvSno.setTextColor(Color.parseColor("#212121"))
            llQuestion.addView(tvSno)

            if (subQues.type == "CustomQuestion") {
                val tvQues = EditText(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 73f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.hint = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                tvQues.setLines(10)
                tvQues.gravity = Gravity.TOP
                llQuestion.addView(tvQues)
            } else {
                val tvQues = TextView(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 73f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.text = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                llQuestion.addView(tvQues)
            }

            if (subQues.children.isEmpty()) {
                if (subQues.type == "SingleSelect") {
                    val cbYes = CheckBox(context)
                    val lpYes =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpYes.topMargin = dimen1dp
                    lpYes.marginStart = dimen1dp
                    lpYes.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpYes.bottomMargin = dimen2dp
                    } else {
                        lpYes.bottomMargin = dimen1dp
                    }
                    cbYes.layoutParams = lpYes
//                tvYes.text = String.format("Yes")
                    cbYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbYes.gravity = Gravity.CENTER
                    cbYes.setBackgroundColor(getBackgroundColor())
                    cbYes.setTextColor(Color.parseColor("#212121"))
                    cbYes.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                        }
                    }
                    llQuestion.addView(cbYes)

                } else if (subQues.type == "CustomQuestion") {
                    val tvYes = View(context)
                    val lpYes =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                    lpYes.topMargin = dimen1dp
                    lpYes.marginStart = dimen1dp
                    lpYes.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpYes.bottomMargin = dimen2dp
                    } else {
                        lpYes.bottomMargin = dimen1dp
                    }
                    tvYes.layoutParams = lpYes
                    tvYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvYes.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(tvYes)
                }
            } else {
                val tvYes = View(context)
                val lpYes =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 15f)
                lpYes.topMargin = dimen1dp
                lpYes.marginStart = dimen1dp
                lpYes.marginEnd = dimen2dp
                if (i == question?.children?.size!! - 1) {
                    lpYes.bottomMargin = dimen2dp
                } else {
                    lpYes.bottomMargin = dimen1dp
                }
                tvYes.layoutParams = lpYes
                tvYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvYes.setBackgroundColor(getBackgroundColor())
                llQuestion.addView(tvYes)
            }
            view.llContainer.addView(llQuestion)

            if (subQues.children.isNotEmpty()) {
                for (j in 0 until subQues.children.size) {
                    val subSubQues = subQues.children[j]
                    val llSubSub = LinearLayout(context)
                    llSubSub.layoutParams =
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    llSubSub.orientation = LinearLayout.HORIZONTAL
                    llSubSub.weightSum = 100f
                    llSubSub.setBackgroundColor(
                        ContextCompat.getColor(context!!, android.R.color.black)
                    )

                    val viewSnoEmpty = View(context)
                    val lpSnoEmpty =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
                    lpSnoEmpty.topMargin = dimen1dp
                    lpSnoEmpty.marginStart = dimen2dp
                    lpSnoEmpty.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpSnoEmpty.bottomMargin = dimen2dp
                    } else {
                        lpSnoEmpty.bottomMargin = dimen1dp
                    }
                    viewSnoEmpty.layoutParams = lpSnoEmpty
                    viewSnoEmpty.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewSnoEmpty.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llSubSub.addView(viewSnoEmpty)

                    val tvOptions = TextView(context)
                    val lpOptions =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 73f)
                    lpOptions.topMargin = dimen1dp
                    lpOptions.marginStart = dimen1dp
                    lpOptions.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOptions.bottomMargin = dimen2dp
                    } else {
                        lpOptions.bottomMargin = dimen1dp
                    }
                    tvOptions.layoutParams = lpOptions
                    tvOptions.text = Html.fromHtml(
                        "\u2022 ${subSubQues.questionText}"
                    )
                    tvOptions.setBackgroundColor(
                        getBackgroundColor()
                    )
                    tvOptions.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvOptions.setTextColor(Color.parseColor("#212121"))
                    llSubSub.addView(tvOptions)

                    if (subSubQues.type == "SingleSelect") {
                        val cbOptionYes = CheckBox(context)
                        val lpOptionYes =
                            LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                            )
                        lpOptionYes.topMargin = dimen1dp
                        lpOptionYes.marginStart = dimen1dp
                        lpOptionYes.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpOptionYes.bottomMargin = dimen2dp
                        } else {
                            lpOptionYes.bottomMargin = dimen1dp
                        }
                        cbOptionYes.layoutParams = lpOptionYes
                        cbOptionYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbOptionYes.gravity = Gravity.CENTER
                        cbOptionYes.setBackgroundColor(
                            getBackgroundColor()
                        )
                        cbOptionYes.setTextColor(Color.parseColor("#212121"))
                        cbOptionYes.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                            }
                        }
                        llSubSub.addView(cbOptionYes)
                    } else {
                        val viewEmptyOptionYes = View(context)
                        val lpEmptyOptionYes =
                            LinearLayout.LayoutParams(
                                0,
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                15f
                            )
                        lpEmptyOptionYes.topMargin = dimen1dp
                        lpEmptyOptionYes.marginStart = dimen1dp
                        lpEmptyOptionYes.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpEmptyOptionYes.bottomMargin = dimen2dp
                        } else {
                            lpEmptyOptionYes.bottomMargin = dimen1dp
                        }
                        viewEmptyOptionYes.layoutParams = lpEmptyOptionYes
                        viewEmptyOptionYes.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewEmptyOptionYes.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewEmptyOptionYes)
                    }

                    view.llContainer.addView(llSubSub)
                }
            }
        }
    }

    private fun getBackgroundColor(): Int {
        return if (llContainer.childCount % 2 != 0) {
            Color.parseColor("#c4e3ed")
        } else {
            Color.WHITE
        }
    }

}
