package com.mananinnovation.tmssurvey.fragments

//import com.mananinnovation.tmssurvey.utils.log
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.adapters.StringAdapter
import com.mananinnovation.tmssurvey.protocols.OnItemClickListener
import com.mananinnovation.tmssurvey.utils.isCollapsed
import com.mananinnovation.tmssurvey.utils.isExpanded
import kotlinx.android.synthetic.main.fragment_respondent_detail.*
import kotlinx.android.synthetic.main.fragment_respondent_detail.view.*
import java.io.File


class RespondentDetailFragment : Fragment(), View.OnClickListener {
    private val provinceList = ArrayList<String>()
    private val provinceDistrictMap = HashMap<String, ArrayList<String>>()
    private var imageUri: Uri? = null
    private val rcCamera = 1234

    private lateinit var provinceBottomSheet: BottomSheetBehavior<View>
    private lateinit var districtBottomSheet: BottomSheetBehavior<View>
    private lateinit var municipalityBottomSheet: BottomSheetBehavior<View>
    private lateinit var educationBottomSheet: BottomSheetBehavior<View>
    private lateinit var religionBottomSheet: BottomSheetBehavior<View>
    private lateinit var occupationBottomSheet: BottomSheetBehavior<View>
    private var onRespondentDetailListener: OnRespondentDetailListener? = null
    private var imageFileName: String? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnRespondentDetailListener) {
            onRespondentDetailListener = context
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_respondent_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        provinceBottomSheet = BottomSheetBehavior.from(view.mcvProvinceSheet)
        provinceBottomSheet.peekHeight = 0
        districtBottomSheet = BottomSheetBehavior.from(view.mcvDistrict)
        districtBottomSheet.peekHeight = 0
        municipalityBottomSheet = BottomSheetBehavior.from(view.mcvMunicipality)
        municipalityBottomSheet.peekHeight = 0
        educationBottomSheet = BottomSheetBehavior.from(view.mcvEducation)
        educationBottomSheet.peekHeight = 0
        religionBottomSheet = BottomSheetBehavior.from(view.mcvReligion)
        religionBottomSheet.peekHeight = 0
        occupationBottomSheet = BottomSheetBehavior.from(view.mcvOccupation)
        occupationBottomSheet.peekHeight = 0
        setProvinceList(view)
        setEducationList(view)
        setReligionList(view)
        setOccupationList(view)
        view.etProvince.setOnClickListener(this@RespondentDetailFragment)
        view.etDistrict.setOnClickListener(this@RespondentDetailFragment)
        view.etMunicipality.setOnClickListener(this@RespondentDetailFragment)
        view.etEducation.setOnClickListener(this@RespondentDetailFragment)
        view.etReligion.setOnClickListener(this@RespondentDetailFragment)
        view.etOccupation.setOnClickListener(this@RespondentDetailFragment)
        view.btnGoToQuestionnaire.setOnClickListener(this@RespondentDetailFragment)
        view.btnPreviousPage.setOnClickListener(this@RespondentDetailFragment)
        view.ivProfile.setOnClickListener(this@RespondentDetailFragment)
    }

    private fun setOccupationList(view: View) {
        val occupationList = ArrayList<String>()
        occupationList.add("Agriculture/Fisher/Livestock")
        occupationList.add("Tourism Sector")
        occupationList.add("Hotel/Restaurant")
        occupationList.add("Aviation Sector")
        occupationList.add("Communication/Media/Press")
        occupationList.add("Students/Researchers")
        occupationList.add("Government/Public Job*")
        occupationList.add("Health Sector (Doctor/Nurses/Health Assistant/Pharmacy)")
        occupationList.add("Labour/Daily Wages")
        occupationList.add("Private (Industry and Commerce)")
        occupationList.add("Social Worker")
        occupationList.add("Unemployed")
        occupationList.add("Pensioner")
        occupationList.add("Others (Specify)....")

        view.rvOccupation.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        view.rvOccupation.adapter = StringAdapter(occupationList).apply {
            onItemClickListener = object : OnItemClickListener {
                override fun onItemClicked(position: Int) {
                    occupationBottomSheet.isCollapsed = true
                    val selectedItem = occupationList[position]
                    if (selectedItem != view.etOccupation.text.toString()) {
                        etOccupation.setText(selectedItem)
                    }
                }
            }
        }
    }

    private fun setReligionList(view: View) {
        val religionList = ArrayList<String>()
        religionList.add("Hindu")
        religionList.add("Buddhist")
        religionList.add("Christian")
        religionList.add("Muslim")
        religionList.add("Kirat")
        religionList.add("Others (Specify)....")

        view.rvReligion.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        view.rvReligion.adapter = StringAdapter(religionList).apply {
            onItemClickListener = object : OnItemClickListener {
                override fun onItemClicked(position: Int) {
                    religionBottomSheet.isCollapsed = true
                    val selectedItem = religionList[position]
                    if (selectedItem != view.etReligion.text.toString()) {
                        etReligion.setText(selectedItem)
                    }
                }
            }
        }
    }

    private fun setEducationList(view: View) {
        val educationList = ArrayList<String>()
        educationList.add("Illiterate")
        educationList.add("Can read and write")
        educationList.add("Primary")
        educationList.add("Below SLC/SEE")
        educationList.add("Up to +2")
        educationList.add("Graduate")
        educationList.add("Post Graduate")
        educationList.add("Others (Specify)....")

        view.rvEducation.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        view.rvEducation.adapter = StringAdapter(educationList).apply {
            onItemClickListener = object : OnItemClickListener {
                override fun onItemClicked(position: Int) {
                    educationBottomSheet.isCollapsed = true
                    val selectedItem = educationList[position]
                    if (selectedItem != view.etEducation.text.toString()) {
                        etEducation.setText(selectedItem)
                    }
                }
            }
        }
    }

    private fun setProvinceList(view: View) {
        provinceList.add("Province 1")
        provinceList.add("Province 2")
        provinceList.add("Province 3")
        provinceList.add("Province 4")
        provinceList.add("Province 5")
        provinceList.add("Province 6")
        provinceList.add("Province 7")

        view.rvProvince.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        view.rvProvince.adapter = StringAdapter(provinceList)
                .apply {
                    onItemClickListener = object :
                            OnItemClickListener {
                        override fun onItemClicked(position: Int) {
                            provinceBottomSheet.isCollapsed = true
                            val selectedPosition = provinceList[position]
                            if (selectedPosition != view.etProvince.text.toString()) {
                                view.etProvince.setText(selectedPosition)
                                view.etDistrict.setText("")
                                setDistrictList(view, provinceList[position])
                            }
                        }
                    }
                }
    }

    private fun setDistrictList(view: View, province: String) {
        val p1DistrictList = ArrayList<String>()
        p1DistrictList.add("Bhojpur")
        p1DistrictList.add("Dhankuta")
        p1DistrictList.add("Ilam")
        p1DistrictList.add("Jhapa")
        p1DistrictList.add("Khotang")
        p1DistrictList.add("Morang")
        p1DistrictList.add("Okhaldhunga")
        p1DistrictList.add("Panchthar")
        p1DistrictList.add("Sankhuwasabha")
        p1DistrictList.add("Solukhumbu")
        p1DistrictList.add("Sunsari")
        p1DistrictList.add("Taplejung")
        p1DistrictList.add("Terhathum")
        p1DistrictList.add("Udayapur")
        p1DistrictList.sort()
        provinceDistrictMap["Province 1"] = p1DistrictList

        val p2DistrictList = ArrayList<String>()
        p2DistrictList.add("Saptari")
        p2DistrictList.add("Siraha")
        p2DistrictList.add("Dhanusa")
        p2DistrictList.add("Mahottari")
        p2DistrictList.add("Sarlahi")
        p2DistrictList.add("Bara")
        p2DistrictList.add("Parsa")
        p2DistrictList.add("Rautahat")
        p2DistrictList.sort()
        provinceDistrictMap["Province 2"] = p2DistrictList

        val p3DistrictList = ArrayList<String>()
        p3DistrictList.add("Sindhuli")
        p3DistrictList.add("Ramechhap")
        p3DistrictList.add("Dolakha")
        p3DistrictList.add("Bhaktapur")
        p3DistrictList.add("Dhading")
        p3DistrictList.add("Kathmandu")
        p3DistrictList.add("Kavrepalanchok")
        p3DistrictList.add("Lalitpur")
        p3DistrictList.add("Nuwakot")
        p3DistrictList.add("Rasuwa")
        p3DistrictList.add("Sindhupalchok")
        p3DistrictList.add("Chitwan")
        p3DistrictList.add("Makwanpur")
        p3DistrictList.sort()
        provinceDistrictMap["Province 3"] = p3DistrictList

        val p4DistrictList = ArrayList<String>()
        p4DistrictList.add("Baglung")
        p4DistrictList.add("Gorkha")
        p4DistrictList.add("Kaski")
        p4DistrictList.add("Lamjung")
        p4DistrictList.add("Manang")
        p4DistrictList.add("Mustang")
        p4DistrictList.add("Myagdi")
        p4DistrictList.add("Nawalpur")
        p4DistrictList.add("Parbat")
        p4DistrictList.add("Syangja")
        p4DistrictList.add("Tanahun")
        p4DistrictList.sort()
        provinceDistrictMap["Province 4"] = p4DistrictList

        val p5DistrictList = ArrayList<String>()
        p5DistrictList.add("Kapilvastu")
        p5DistrictList.add("Parasi")
        p5DistrictList.add("Rupandehi")
        p5DistrictList.add("Arghakhanchi")
        p5DistrictList.add("Gulmi")
        p5DistrictList.add("Palpa")
        p5DistrictList.add("Dang")
        p5DistrictList.add("Pyuthan")
        p5DistrictList.add("Rolpa")
        p5DistrictList.add("Eastern Rukum")
        p5DistrictList.add("Banke")
        p5DistrictList.add("Bardiya")
        p5DistrictList.sort()
        provinceDistrictMap["Province 5"] = p5DistrictList

        val p6DistrictList = ArrayList<String>()
        p6DistrictList.add("Western Rukum")
        p6DistrictList.add("Salyan")
        p6DistrictList.add("Dolpa")
        p6DistrictList.add("Humla")
        p6DistrictList.add("Jumla")
        p6DistrictList.add("Kalikot")
        p6DistrictList.add("Mugu")
        p6DistrictList.add("Surkhet")
        p6DistrictList.add("Dailekh")
        p6DistrictList.add("Jajarkot")
        p6DistrictList.sort()
        provinceDistrictMap["Province 6"] = p6DistrictList

        val p7DistrictList = ArrayList<String>()
        p7DistrictList.add("Kailali")
        p7DistrictList.add("Achham")
        p7DistrictList.add("Doti")
        p7DistrictList.add("Bajhang")
        p7DistrictList.add("Bajura")
        p7DistrictList.add("Kanchanpur")
        p7DistrictList.add("Dadeldhura")
        p7DistrictList.add("Baitadi")
        p7DistrictList.add("Darchula")
        p7DistrictList.sort()
        provinceDistrictMap["Province 7"] = p7DistrictList

        view.rvDistrict.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        view.rvDistrict.adapter = StringAdapter(provinceDistrictMap[province]!!)
                .apply {
                    onItemClickListener = object :
                            OnItemClickListener {
                        override fun onItemClicked(position: Int) {
                            districtBottomSheet.isCollapsed = true
                            val selectedDistrict = provinceDistrictMap[province]!![position]
                            if (view.etDistrict.text.toString() != selectedDistrict) {
                                view.etDistrict.setText(selectedDistrict)
                                view.etMunicipality.setText("")
                                setMunicipalityList(view, provinceDistrictMap[province]!![position])
                            }
                        }
                    }
                }
    }

    private fun setMunicipalityList(view: View, district: String) {
//        this@RespondentDetailFragment.javaClass.log("district: $district")
        val districtToMunicipalityMap = HashMap<String, Array<String>>()
        districtToMunicipalityMap["Bhojpur"] = arrayOf(
                "Bhojpur Municipalities",
                "Shadananda Municipalities",
                "Tyamkemaiyum Rural Municipalities",
                "Ramprasadrai Rural Municipalities",
                "Arun Rural Municipalities",
                "Pauwadungma Rural Municipalities",
                "Salpasilichho Rural Municipalities",
                "Aamchok  Rural Municipalities",
                "Hatuwagadhi Rural Municipalities"
        )
        districtToMunicipalityMap["Dhankuta"] = arrayOf(
                "Dhankuta Municipality Ward no.3 Dhankuta",
                "Chaubise Gaun Palika",
                "Chhatar Jorpati Gaun palika",
                "Mahalakshmi Municipality Dhankuta",
                "Dhankuta Municipality",
                "Pakhirabas Municipality",
                "Sagurigadi Rural Municipality",
                "Sahidbhumi Rural Municipality"
        )

        districtToMunicipalityMap["Ilam"] = arrayOf(
                "Illam Municipalities",
                "Deumai Municipalities",
                "Mai Municipalities",
                "Suryoday Municipalities",
                "Fakafokathum Rural Municipalities",
                "Chulachuli Rural Municipalities",
                "Maijogmai Rural Municipalities",
                "Mansebung Rural Municipalities",
                "Rong Rural Municipalities",
                "Sandakpur Rural Municipalities"
        )

        districtToMunicipalityMap["Jhapa"] = arrayOf(
                "Mechinagar Municipalities",
                "Damak Municipalities",
                "Kankai Municipalities",
                "Bhadrapur Municipalities",
                "Arjundhara Municipalities",
                "Shivasatakshi Municipalities",
                "Gauradaha Municipalities",
                "Birtamod Municipalities",
                "Kamal Rural Municipalities",
                "Gauriganj Rural Municipalities",
                "Bahradashi Rural Municipalities",
                "Jhapa Rural Municipalities",
                "Buddhashanti Rural Municipalities",
                "Haldibari Rural Municipalities",
                "Kachankawal Rural Municipalities"
        )

        districtToMunicipalityMap["Khotang"] = arrayOf(
                "Halesi-Tuchung Municipalities",
                "Rupakotmajhugadhi Municipalities",
                "Ainselukharka Rural Municipalities",
                "Lamidanda Rural Municipalities",
                "Jantedhunga Rural Municipalities",
                "Khotehang Rural Municipalities",
                "Kepilasgadhi Rural Municipalities",
                "Diprung Rural Municipalities",
                "Sakela Rural Municipalities",
                "Bahapokhari Rural Municipalities"
        )

        districtToMunicipalityMap["Morang"] = arrayOf(
                "Biratnagar SMPC",
                "Belbari Municipalities",
                "Letang Municipalities",
                "Pathari-Sanishchare Municipalities",
                "Rangeli Municipalities",
                "Ratuwamai Municipalities",
                "Sunbarsi Municipalities",
                "Urlabari Municipalities",
                "Sundarharaicha Municipalities",
                "Budhiganga Rural Municipalities",
                "Dhanpalthan Rural Municipalities",
                "Gramthan Rural Municipalities",
                "Jahada Rural Municipalities",
                "Kanepokhari Rural Municipalities",
                "Katahari Rural Municipalities",
                "Kerabari Rural Municipalities",
                "Miklajung Rural Municipalities"
        )

        districtToMunicipalityMap["Okhaldunga"] = arrayOf(
                "Pakhribas Municipalities",
                "Dhankuta Municipalities",
                "Mahalaxmi Municipalities",
                "Sanghurigadhi Rural Municipalities",
                "Khalsa chhintang Sahidbhumi Rural Municipalities",
                "Chhathar Jorpati Rural Municipalities",
                "Chaubise Rural Municipalities",
                "Sidhhicharan Municipalities",
                "Khijidemba Rural Municipalities",
                "Champadevi Rural Municipalities",
                "Chisankhugadhi Rural Municipalities",
                "Manebhanjang Rural Municipalities",
                "Molung Rural Municipalities",
                "Likhu Rural Municipalities",
                "Sunkoshi Rural Municipalities"
        )

        districtToMunicipalityMap["Panchthar"] = arrayOf(
                "Phidim Municipalities",
                "Phalelung Rural Municipalities",
                "Phalgunanda Rural Municipalities",
                "Hilihang Rural Municipalities",
                "Kummayak Rural Municipalities",
                "Miklajung Rural Municipalities",
                "Tumwewa Rural Municipalities",
                "Yangwarak Rural Municipalities"
        )

        districtToMunicipalityMap["Sankhuwasabha"] = arrayOf(
                "Chainpur Municipalities",
                "Dharmadebi Municipalities",
                "Khadwari Municipalities",
                "Madi Municipalities",
                "Panchkhapan Municipalities",
                "Bhotkhola Rural Municipalities",
                "Chichila Rural Municipalities",
                "Makalu Rural Municipalities",
                "Sabhapokhari Rural Municipalities",
                "Silichong Rural Municipalities"
        )

        districtToMunicipalityMap["Solokhumbu"] = arrayOf(
                "Solududhkunda Municipalities",
                "Dudhkoshi Rural Municipalities",
                "Khumbu Pasanglhamu Rural Municipalities",
                "Dudhkaushika Rural Municipalities",
                "Nechasalyan Rural Municipalities",
                "Mahakulung Rural Municipalities",
                "Likhu Pike Rural Municipalities",
                "Sotang Rural Municipalities"
        )
        districtToMunicipalityMap["Sunsari"] = arrayOf(
                "Itahari SMPC",
                "Dharan SMPC",
                "Inaruwa Municipalities",
                "Duhabi Municipalities",
                "Ramdhuni Municipalities",
                "Baraha Municipalities",
                "Dewangunj Rural Municipalities",
                "Koshi Rural Municipalities",
                "Gadhi Rural Municipalities",
                "Barju Rural Municipalities",
                "Bhokraha Rural Municipalities",
                "Harinagara Rural Municipalities"
        )

        districtToMunicipalityMap["Taplejung"] = arrayOf(
                "Phungling Municipalities",
                "Aathrai Tribeni Rural Municipalities",
                "Sidingwa Rural Municipalities",
                "Phaktanglung Rural Municipalities",
                "Mikwakhola Rural Municipalities",
                "Meringden Rural Municipalities",
                "Maiwakhola Rural Municipalities",
                "Yangwarak Rural Municipalities",
                "Sirijangha Rural Municipalities"
        )
        districtToMunicipalityMap["Terathum"] = arrayOf(
                "Myanglung Municipalities",
                "Lalignuras Municipalities",
                "Athhrai Rural Municipalities",
                "Chhathar Rural Municipalities",
                "Fedap Rural Municipalities",
                "Menchyayem Rural Municipalities"
        )

        districtToMunicipalityMap["Udayapur"] = arrayOf(
                "Katari Municipalities",
                "Chaudandigadhi Municipalities",
                "Triyuga Municipalities",
                "Belaka Municipalities",
                "Udaypurgadhi Rural Municipalities",
                "Tapli Rural Municipalities",
                "Rautamai Rural Municipalities",
                "Sunkoshi Rural Municipalities"
        )

        districtToMunicipalityMap["Saptari"] = arrayOf(
                "Rajbiraj Municipalities",
                "Kanchanpur Municipalities",
                "Dakneshwori Municipalities",
                "Bodebarsayen Municipalities",
                "Khadak Municipalities",
                "Shambhunath Municipalities",
                "Suranga Municipalities",
                "Hanumannagar Kankalini Municipalities",
                "Krishnasabaran Rural Municipalities",
                "Chhinnamasta Rural Municipalities",
                "Mahadewa Rural Municipalities",
                "Saptakoshi Rural Municipalities",
                "Tirahut Rural Municipalities",
                "Tilathi Koiladi Rural Municipalities",
                "Rupani Rural Municipalities",
                "Belahi Chapena Rural Municipalities",
                "Bishnupur Rural Municipalities",
                "Balan Bihul Rural Municipalities"
        )

        districtToMunicipalityMap["Siraha"] = arrayOf(
                "Lahan Municipalities",
                "Dhangadhimai Municipalities",
                "Siraha Municipalities",
                "Golbazar Municipalities",
                "Mirchaiya Municipalities",
                "Kalyanpur Municipalities",
                "Bhagawanpur Rural Municipalities",
                "Aurahi Rural Municipalities",
                "Bishnupur Rural Municipalities",
                "Sukhipur Rural Municipalities",
                "Karjanha Rural Municipalities",
                "Bariyarpatti Rural Municipalities",
                "Laxmipur Patari Rural Municipalities",
                "Naraha Rural Municipalities",
                "Sakhuwanankarkatti",
                "Arnama Rural Municipalities",
                "Nawarajpur Rural Municipalities"
        )

        districtToMunicipalityMap["Bara"] = arrayOf(
                "Kalaiya SMPC",
                "Jitpur Simara SMPC",
                "Kolahawi Municipalities",
                "Nijgadh Municipalities",
                "Mahagadhimai Municipalities",
                "Simraungadh Municipalities",
                "Kotwal Rural Municipalities",
                "Karaiyamai Rural Municipalities",
                "Devtal Rural Municipalities",
                "Pacharauta  Municipalities",
                "Parawanipur Rural Municipalities",
                "Parsauni Rural Municipalities",
                "Feta Rural Municipalities",
                "Baragadhi Rural Municipalities",
                "Subarna Rural Municipalities",
                "Bishrampur Gaupalika"
        )

        districtToMunicipalityMap["Dhanusha"] = arrayOf(
                "Janakpur SMPC",
                "Chhireshwornath Municipalities",
                "Ganeshman Charnath Municipalities",
                "Dhanushadham Municipalities",
                "Nagarayen Municipalities",
                "Bideha Municipalities",
                "Mithila Municipalities",
                "Sahidnagar Municipalities",
                "Sabaila Municipalities",
                "Kamalasiddhidatri Rural Municipalities",
                "Janaknandani Rural Municipalities",
                "Bateshwor Rural Municipalities",
                "Mithila Bihari Rural Municipalities",
                "Mukhiyapatti Municipalitiesusaharmiya Rural Munici",
                "Laxminiya Rural Municipalities",
                "Hansapur Municipalities",
                "Aurahi Rural Municipalities",
                "Dhanauji"
        )

        districtToMunicipalityMap["Mahottari"] = arrayOf(
                "Hetauda SMPC",
                "Thaha Municipalities",
                "Indrasarobar Rural Municipalities",
                "Kailash Rural Municipalities",
                "Bakaiya Rural Municipalities",
                "Bagmati Rural Municipalities",
                "Bhimfedi Rural Municipalities",
                "Makawanpurgadhi Rural Municipalities",
                "Manahari Rural Municipalities",
                "Raksirang Rural Municipalities"
        )

        districtToMunicipalityMap["Parsa"] = arrayOf(
                "Birgunj MPC",
                "Pokhariya Municipalities",
                "Thori Rural Municipalities",
                "Jagarnathpur Rural Municipalities",
                "Dhobini Rural Municipalities",
                "Chhipaharmai Rural Municipalities",
                "Pakaha Mainpur Rural Municipalities",
                "Bindabasini Rural Municipalities",
                "Bahudarmai Rural Municipalities",
                "Jirabhawani Rural Municipalities",
                "Parsagadhi Rural Municipalities ",
                "Sakhuwa Prasauni Rural Municipalities",
                "Paterwa Sugauli Rural Municipalities",
                "Kalikamai Rural Municipality"
        )

        districtToMunicipalityMap["Rautahat"] = arrayOf(
                "Chandrapur Municipalities",
                "Garuda Municipalities",
                "Gaur Municipalities",
                "Baudhimai Rural Municipalities",
                "Brindaban Rural Municipalities",
                "Debahi Gonahi Rural Municipalities",
                "Durga Bhagawati Rural Municipalities",
                "Gadhimai Rural Municipalities",
                "Gujara Rural Municipalities",
                "Katahariya Rural Municipalities",
                "Madhavnarayan Rural Municipalities",
                "Maulapur Rural Municipalities",
                "Fatuwabi Jaypur Rural Municipalities",
                "Ishanath Rural Municipalities",
                "Paroha Rural Municipalities",
                "Rajpur Rural Municipalities",
                "Yamuna Mai Rural Municipality",
                "Rajdevi Municipality"
        )

        districtToMunicipalityMap["Sarlahi"] = arrayOf(
                "Ishworpur Municipalities",
                "Malangawa Municipalities",
                "Lalbandi Municipalities",
                "Haripur Municipalities",
                "Haripurwa Municipalities",
                "Hariwan Municipalities",
                "Barahathawa Municipalities",
                "Balara Municipalities",
                "Godaita Municipalities",
                "Bagmati Municipalities",
                "Kabilasi Rural Municipalities",
                "Chakarghatta Rural Municipalities",
                "Chandranagar Rural Municipalities",
                "Dhanakaul Rural Municipalities",
                "Bramhapuri Rural Municipalities",
                "Ramnagar Rural Municipalities",
                "Bishnu Rural Municipalities",
                "Kaudena ga.pa",
                "Basbariya ga.pa",
                "Parsa Ga.pa"
        )

        districtToMunicipalityMap["Bhaktapur"] = arrayOf(
                "Changunarayan Municipalities",
                "Bhaktapur Municipalities",
                "Madhyapur Thimi Municipalities",
                "Suryabinayak Municipalities"
        )

        districtToMunicipalityMap["Chitwan"] = arrayOf(
                "Bharatpur MunicipalitiesPC",
                "Kalika Municipalities",
                "Khairahani Municipalities",
                "Madi Municipalities",
                "Ratnanagar Municipalities",
                "Rapti Municipalities",
                "Ekshakama Rural Municipalities"
        )

        districtToMunicipalityMap["Dhading"] = arrayOf(
                "Dhunibeshi Municipalities",
                "Nilkantha Municipalities",
                "Khaniyabas Rural Municipalities",
                "Gajuri Rural Municipalities",
                "Galchhi Rural Municipalities",
                "GangaJamuna Rural Municipalities",
                "Jwalamukhi Rural Municipalities",
                "Thake Rural Municipalities",
                "Netrabati Rural Municipalities",
                "Benighat Rorang Rural Municipalities",
                "Rubi Vyali Rural Municipalities",
                "Siddhalek Rural Municipalities",
                "Tripurasundari Rural Municipalities"
        )
        districtToMunicipalityMap["Dolakha"] = arrayOf(
                "Jiri Municipalities",
                "Veemedhar Municipalities",
                "Kalinchwok Rural Municipalities",
                "Gaurishankar Rural Municipalities",
                "Tamakoshi Rural Municipalities",
                "Melung Rural Municipalities",
                "Wigu Rural Municipalities",
                "Waiteshor Rural Municipalities",
                "Shailung Rural Municipalities"
        )

        districtToMunicipalityMap["Kathmandu"] = arrayOf(
                "Kathmandu MunicipalitiesPC",
                "Kageshwori Municipalitiesanohara Municipalities",
                "Kirtipur Municipalities",
                "Gokarneshwor Municipalities",
                "Chandragiri Municipalities",
                "Tokha Municipalities",
                "Tarakeshwor Municipalities",
                "Dakshinkali Municipalities",
                "Nagarjun Municipalities",
                "Budhanilkanth Municipalities",
                "Shankharapur Municipalities"
        )

        districtToMunicipalityMap["kavrepalanchok"] = arrayOf(
                "Dhulikhel Municipalities",
                "Banepa Municipalities",
                "Panauti Municipalities",
                "Panchkhal Municipalities",
                "Namobuddha Municipalities",
                "Khanikhola Rural Municipalities",
                "Chaunrideurali Rural Municipalities",
                "Temal Rural Municipalities",
                "Bethanchowk Rural Municipalities",
                "Bhumlu Rural Municipalities",
                "Mandandeupur Municipalities",
                "Mahabharat Rural Municipalities",
                "Roshi Rural Municipalities"
        )

        districtToMunicipalityMap["Lalitpur"] = arrayOf(
                "Lalitpur MunicipalitiesPC",
                "Godabari Municipalities",
                "Mahalaxmi Municipalities",
                "Konjyosom Rural Municipalities",
                "Bagmati Rural Municipalities",
                "Mahankal Rural Municipalities"
        )

        districtToMunicipalityMap["Makwanpur"] = arrayOf(
                "Jaleshwor Municipalities",
                "Bardibas Municipalities",
                "Gaushala Municipalities",
                "Ekdara Rural Municipalities",
                "Sonama Rural Municipalities",
                "Samsi Rural Municipalities",
                "Loharpatti Rural Municipalities",
                "Ramgopalpur Rural Municipalities",
                "Mahottari Rural Municipalities",
                "Manara Rural Municipalities",
                "Matihani Rural Municipalities",
                "Bhangaha Rural Municipalities",
                "Balawa Rural Municipalities",
                "Pipara Rural Municipalities",
                "Aurahi Rural Municipalities"
        )

        districtToMunicipalityMap["Nuwakot"] = arrayOf(
                "Bidur Municipalities",
                "Belkotgadhi Municipalities",
                "Kakani Rural Municipalities",
                "Kispang Rural Municipalities",
                "Taadi Rural Municipalities",
                "Taarakedhar Rural Municipalities",
                "Dupchedhar Rural Municipalities",
                "Panchakanya Rural Municipalities",
                "Likhu Rural Municipalities",
                "Meghang Rural Municipalities",
                "Shibapuri Rural Municipalities",
                "Suryagadhi Rural Municipalities"
        )

        districtToMunicipalityMap["Ramechhap"] = arrayOf(
                "Manthali Municipalities",
                "Ramechhap Municipalities",
                "Umakunda Rural Municipalities",
                "Khandadevi Rural Municipalities",
                "Gokulganga Rural Municipalities",
                "Doramba Rural Municipalities",
                "Likhu Rural Municipalities",
                "Sunapati Rural Municipalities"
        )
        districtToMunicipalityMap["Rasuwa"] = arrayOf(
                "Uttargaya Rural Municipalities",
                "Kalika Rural Municipalities",
                "Gosaikunda Rural Municipalities",
                "Naukunda Rural Municipalities",
                "Parbatikunda Rural Municipalities"
        )

        districtToMunicipalityMap["Sindhuli"] = arrayOf(
                "Kamalamai Municipalities",
                "Dudhauli Municipalities",
                "Golanjor Rural Municipalities",
                "Ghyanglek Rural Municipalities",
                "Tinpatan Rural Municipalities",
                "Fikkal Rural Municipalities",
                "Marin Rural Municipalities",
                "Sunkoshi Rural Municipalities",
                "Hariharpurgadhi Rural Municipalities"
        )

        districtToMunicipalityMap["Sindhupalchok"] = arrayOf(
                "Chautara Sangachokgadhi Municipalities",
                "Barhabise Municipalities",
                "Melamchi Municipalities",
                "Indrabati Rural Municipalities",
                "Jugal Rural Municipalities ",
                "Panchpokhari Thangpal Rural Municipalities",
                "Balephi Rural Municipalities",
                "Bhotekoshi Rural Municipalities",
                "Lisankhu Paakhar Rural Municipalities",
                "Sunkoshi Rural Municipalities",
                "Helambu Rural Municipalities",
                "Tripurasundari Rural Municipalities"
        )

        districtToMunicipalityMap["Arghakhanchi"] = arrayOf(
                "Sandhikharka Municipalities",
                "Sitaganga Municipalities",
                "Vumikasthan Municipalities",
                "Chhatradeb Rural Municipalities",
                "Paadeni Rural Municipalities",
                "Malarani Rural Municipalities",
                "Chhatradev Rural Municipality"
        )

        districtToMunicipalityMap["Gulmi"] = arrayOf(
                "Musikot Municipalities",
                "Resunga Municipalities",
                "Isma Rural Municipalities",
                "Kaligandaki Rural Municipalities",
                "Gulmidarbar Rural Municipalities",
                "Satyabati Rural Municipalities",
                "Chndrakot Rural Municipalities",
                "Ruru Rural Municipalities",
                "Chhatrakot Rural Municipalities",
                "Dhurkot Rural Municipalities",
                "Madane Rural Municipalities",
                "Malika  Rural Municipalities"
        )

        districtToMunicipalityMap["Kapilbastu"] = arrayOf(
                "Kapilbastu Municipalities",
                "Buddhavumi Municipalities",
                "Shivraj Municipalities",
                "Maharajganj Municipalities",
                "Krishnanagar Municipalities",
                "Baadganga Municipalities",
                "Maayadebi Rural Municipalities",
                "Yashodhara Rural Municipalities",
                "Suddhodhan Rural Municipalities",
                "Bijayanagar Rural Municipalities"
        )

        districtToMunicipalityMap["Palpa"] = arrayOf(
                "Rampur Municipalities",
                "Tansen Municipalities",
                "Nisdi Rural Municipalities",
                "Purbakhola Rural Municipalities",
                "Ramva Rural Municipalities",
                "Mathagadhi Rural Municipalities",
                "Tinau Rural Municipalities",
                "Bagnaskali Rural Municipalities",
                "Ribdikot Rural Municipalities",
                "Rainadebi Chhahara Rural Municipalities"
        )

        districtToMunicipalityMap["Rupandehi"] = arrayOf(
                "Butwal SMMP",
                "Devdaha Municipalities",
                "Lumbini Sanskritik Municipalities",
                "Sainamaina Municipalities",
                "Siddharthanagar Municipalities",
                "Tilottama Municipalities",
                "Gaidahawa Rural Municipalities",
                "Kanchan Rural Municipalities",
                "Kotahimaai Rural Municipalities",
                "Marchawari Rural Municipalities",
                "Mayadebi Rural Municipalities",
                "Omsatiya Rural Municipalities",
                "Rohini Rural Municipalities",
                "Sammarimai Rural Municipalities",
                "Siyari Rural Municipalities",
                "Suddhodhan Rural Municipalities"
        )

        districtToMunicipalityMap["Banke"] = arrayOf(
                "Aanbukhaireni Rural Municipalities, Aanbukhaireni",
                "Bhimad Rural Municipalities, Bhimad",
                "Bandipur Rural Municipalities, Bandipur",
                "Suklagandaki Nagar palika, Sulkagandaki",
                "Myagde rural municipalities, Myagde",
                "Rishing Rural Municipalities, Rishing",
                "Ghiring Rural Municipalities, Ghiring",
                "Devghat Rural Municipalities, Devghat",
                "Bhanu Nagar palika, Bhanu",
                "Vyas Nagar palika, Vyas"
        )

        districtToMunicipalityMap["Bardiya"] = arrayOf(
                "Nepalgunj SMM",
                "Kohalpur Municipalities",
                "Narainapur Rural Municipalities",
                "Raptisonari Rural Municipalities",
                "Baijanath Rural Municipalities",
                "Khajura Rural Municipalities",
                "Duduwa Rural Municipalities",
                "Janaki Rural Municipalities"
        )

        districtToMunicipalityMap["Dang"] = arrayOf(
                "Gulariya Municipalities",
                "Madhuwan Municipalities",
                "Rajapur Municipalities ",
                "Thakurbaba Municipalities",
                "Bansgadhi Municipalities",
                "Barbardiya Municipalities",
                "Badhaiyatal Rural Municipalities",
                "Geruwa Rural Municipalities"
        )

        districtToMunicipalityMap["Pyuthan"] = arrayOf(
                "Pyuthan Municipalities",
                "Sworgadwari Municipalities",
                "Gaumukhi Rural Municipalities",
                "Mandwi Rural Municipalities",
                "Sarumarani Rural Municipalities",
                "Mallarani Rural Municipalities",
                "Naubahini Rural Municipalities",
                "Jhimruk Rural Municipalities",
                "Airabati Rural Municipalities"
        )

        districtToMunicipalityMap["Rolpa"] = arrayOf(
                "Rolpa Municipalities",
                "Tribeni Rural Municipalities",
                "Paribartan Rural Municipalities",
                "Maadi Rural Municipalities",
                "Runtigadhi Rural Municipalities",
                "Lungri Rural Municipalities",
                "Sukidaha Rural Municipalities",
                "Sunchhahari Rural Municipalities",
                "Sunil smriti Rural Municipalities",
                "Thawang Rural Municipalities"
        )

        districtToMunicipalityMap["Rukum East"] = arrayOf(
                "Musikot Municipalities",
                "Chaurjahari Municipalities",
                "Aathbiskot Municipalities",
                "Putha Uttarganga Rural Municipalities",
                "Vume Rural Municipalities",
                "Sisne Rural Municipalities",
                "Banfikot Rural Municipalities",
                "Tribeni Rural Municipalities",
                "Saniveri Rural Municipalities",
                "Jhingha"
        )

        districtToMunicipalityMap["Baglung"] = arrayOf(
                "Baglung Municipality",
                "Galkot Municipalities",
                "Jaimini Municipalities",
                "Dhorpatan Municipalities",
                "Bareng Rural Municipalities",
                "Kathekhola Rural Municipalities",
                "Tamankhola Rural Municipalities",
                "Tarakhola Rural Municipalities",
                "Nisikhola Rural Municipalities",
                "Badigad  Rural Municipalities"
        )

        districtToMunicipalityMap["Gorkha"] = arrayOf(
                "Gorkha Municipalities",
                "Palungtar Municipalities",
                "Sulikot Rural Municipalities",
                "Siranchok Rural Municipalities",
                "Ajirkot Rural Municipalities",
                "Aarughat Rural Municipalities",
                "Gandaki Rural Municipalities",
                "Chumnubri Rural Municipalities",
                "Dharche Rural Municipalities",
                "Vimsen Rural Municipalities",
                "Shahid Lakhan Rural Municipalities"
        )

        districtToMunicipalityMap["Kaski"] = arrayOf(
                "Pokhara Lekhanath Metropolitan City",
                "Annapurna Rural Municipality",
                "Machhapuchchhre Rural Municipality",
                "Madi Rural Municipality",
                "Rupa Rural Municipality"
        )

        districtToMunicipalityMap["Lamjung"] = arrayOf(
                "BeshiShahar Municipalities",
                "Madhyanepal Municipalities",
                "Rainas Municipalities",
                "Sundarbazar Municipalities",
                "Kbholasothar Rural Municipalities",
                "Dudhpokhari Rural Municipalities",
                "Dordi Rural Municipalities",
                "Marsyandi Rural Municipalities"
        )

        districtToMunicipalityMap["Manang"] = arrayOf(
                "Chame Rural Municipalities",
                "Narfu Rural Municipalities",
                "Nasong Rural Municipalities",
                "Nesyang Rural Municipalities"
        )

        districtToMunicipalityMap["Mustang"] = arrayOf(
                "Gharapjhong Rural Municipalities",
                "Thasang Rural Municipalities",
                "Dalome Rural Municipalities",
                "Lomanthang Rural Municipalities ",
                "Bahragaun Municipalitiesuktikshetra Rural Municipa"
        )

        districtToMunicipalityMap["Myagdi"] = arrayOf(
                "Beni Municipalities",
                "Annapurna Rural Municipalities",
                "Dhablagiri Rural Municipalities",
                "Mangala Rural Municipalities",
                "Malika  Rural Municipalities",
                "Raghuganga Rural Municipalities",
                "lokdeep secondary boarding school"
        )

        districtToMunicipalityMap["Nawalparasi East"] = arrayOf(
                "Kawasoti Municipalities",
                "Gaidakot Municipalities",
                "Debachuli Municipalities",
                "Bardaghat Municipalities",
                "Madhyabindu Municipalities",
                "Ramgram Municipalities",
                "Sunawal Municipalities",
                "Tribenisusta Rural Municipalities",
                "Palheenandan Rural Municipalities",
                "Pratappur Rural Municipalities",
                "Bungdikali Rural Municipalities",
                "Bulingtar Rural Municipalities",
                "Binayee Rural Municipalities",
                "Sarawal Rural Municipalities",
                "Hupsekot Rural Municipalities"
        )
        districtToMunicipalityMap["Parbat"] = arrayOf(
                "Kushma Municipalities",
                "Falebas Municipalities",
                "Jaljala Rural Municipalities",
                "Paiyun Rural Municipalities",
                "Mahashila Rural Municipalities",
                "Modi Rural Municipalities",
                "Bihadi Rural Municipalities"
        )

        districtToMunicipalityMap["Tanahu"] = arrayOf(
                "Galyang Municipalities",
                "Chapakot Municipalities",
                "Putalibazar Municipalities",
                "Veerkot Municipalities",
                "Waling Municipalities",
                "Arjunchaupari Rural Municipalities",
                "Aandhikhola Rural Municipalities",
                "Kaligandaki Rural Municipalities",
                "Phedikhola Rural Municipalities",
                "Biruwa Rural Municipalities",
                "Harinas Rural Municipalities"
        )

        districtToMunicipalityMap["Syangja"] = arrayOf(
                "Veri Municipalities",
                "Chhedagaad Municipalities",
                "Tribeni Municipalities",
                "Kuse Rural Municipalities",
                "Junichande Rural Municipalities",
                "Baarekot Rural Municipalities",
                "Shibalaya Rural Municipalities"
        )

        districtToMunicipalityMap["Dailekh"] = arrayOf(
                "Narayan Municipalities",
                "Dullu Municipalities",
                "Chamunda Bindrasaini  Municipalities",
                "Aathbis Municipalities",
                "Bhagwatimai Rural Municipalities",
                "Gurans Rural Municipalities",
                "Dhungedhar Rural Municipalities",
                "Naumule Rural Municipalities",
                "Mahabu Rural Municipalities",
                "Bhairawi Rural Municipalities",
                "Thatikandh Rural Municipalities"
        )

        districtToMunicipalityMap["Dolpa"] = arrayOf(
                "Tulsipur SMM",
                "Ghorahi SMM",
                "Lamahi Municipalities",
                "Banglachuli Rural Municipalities",
                "Dangisharan Rural Municipalities",
                "Gadhawa Rural Municipalities",
                "Rajpur Rural Municipalities",
                "Rapti Rural Municipalities",
                "Shantinagar Rural Municipalities",
                "Babai Rural Municipalities",
                "Chandannath Municipalities",
                "Kanakasundari Rural Municipalities",
                "Sinja Rural Municipalities",
                "Hima Rural Municipalities",
                "Tila Rural Municipalities",
                "Guthichaur Rural Municipalities",
                "Tatopani Rural Municipalities",
                "Patarasi Rural Municipalities"
        )

        districtToMunicipalityMap["Humla"] = arrayOf(
                "Thuliveri Municipalities",
                "Tripurasundari Municipalities",
                "Dolpobuddha Rural Municipalities",
                "Se-Foksundo Rural Municipalities",
                "Jagadulla Rural Municipalities",
                "Mudkechula Rural Municipalities",
                "Kaike Rural Municipalities",
                "Chharka Tangsong Rural Municipalities"
        )

        districtToMunicipalityMap["Jajarkot"] = arrayOf(
                "Simakot Rural Municipalities",
                "Namkha Rural Municipalities",
                "Kharpunath Rural Municipalities",
                "Sarkegad Rural Municipalities",
                "Chankheli Rural Municipalities",
                "Adanchuli Rural Municipalities ",
                "Tanjakot Rural Municipalities "
        )

        districtToMunicipalityMap["Jumla"] = arrayOf(
                "Hima Gaupalika ,Jumla",
                "kanakasundari gaupalika",
                "Ghuthichaur gaupalika",
                "Chandannath Municipality",
                "Sinja Rural Municipality",
                "Tila Rural Municipality",
                "Tatopani Rural Municipality",
                "Patarasi Rural Municipality"
        )

        districtToMunicipalityMap["Kalikot"] = arrayOf(
                "Khandachakra Municipalities",
                "Raskot Municipalities",
                "Tilagufa Municipalities",
                "Pachaljharana Rural Municipalities",
                "Sannitribeni Rural Municipalities",
                "Naraharinath Rural Municipalities",
                "Kalika Rural Municipalities",
                "Mahawai Rural Municipalities",
                "Palata Rural Municipalities "
        )

        districtToMunicipalityMap["Mugu"] = arrayOf(
                "Chhayanath Rara Municipalities",
                "Mugu Municipalitiesakarma Rong Rural Municipalitie",
                "Soru Rural Municipalities",
                "Khatyad Rural Municipalities"
        )

        districtToMunicipalityMap["Salyan"] = arrayOf(
                "Sharada Municipalities",
                "Bagchaur Municipalities",
                "Bangada Kupende Municipalities",
                "Kalimati Rural Municipalities",
                "Tribeni Rural Municipalities",
                "Kapurkot Rural Municipalities",
                "Chhatreshwori Rural Municipalities",
                "Dhorchaur Rural Municipalities",
                "Kumakhamalika Rural Municipalities",
                "Darma Rural Municipalities"
        )

        districtToMunicipalityMap["Surkhet"] = arrayOf(
                "Birendranagar Municipalities",
                "Bheriganga Municipalities",
                "Gurvakot Municipalities",
                "Panchapuri Municipalities",
                "Lekbeshi Municipalities",
                "Chaukune Rural Municipalities",
                "Baraahatal Rural Municipalities",
                "Chingada Rural Municipalities",
                "Simta Rural Municipalities"
        )

        districtToMunicipalityMap["Achham"] = arrayOf(
                "Mangalsen Municipalities",
                "Kamalbazar Municipalities",
                "Saanphebagar Municipalities",
                "Panchadebal Binayak  Municipalities",
                "Chaurpati Rural Municipalities",
                "Mallekh Rural Municipalities",
                "Bannigadhi Jayagadh Rural Municipalities",
                "Ramaroshan Rural Municipalities",
                "Dhakari Rural Municipalities",
                "Turmakhaanda Rural Municipalities"
        )

        districtToMunicipalityMap["Baitadi"] = arrayOf(
                "Dasharathchand Municipalities",
                "Patan Municipalities",
                "Melauli Municipalities",
                "Puchaudi Municipalities",
                "Surnaya Rural Municipalities",
                "Sigas Rural Municipalities",
                "Shibanath Rural Municipalities",
                "Panchedhar Rural Municipalities",
                "Dogadaakedar Rural Municipalities",
                "Dilasaini Rural Municipalities"
        )

        districtToMunicipalityMap["Bajhang"] = arrayOf(
                "Jayaprithibi Municipalities",
                "Bungal Municipalities",
                "Talkot Rural Municipalities",
                "Mashtaa Rural Municipalities",
                "Khaptadchhanna Rural Municipalities",
                "Thalaraa  Rural Municipalities",
                "Witthadchir Rural Municipalities",
                "Surma Rural Municipalities",
                "chhabisapaathiveraa Rural Municipalities",
                "Durgathali Rural Municipalities",
                "Kedaarsyu Rural Municipalities",
                "Kandaa Rural Municipalities"
        )

        districtToMunicipalityMap["Bajura"] = arrayOf(
                "Badimalika Municipalities",
                "Tribeni Municipalities",
                "Budhiganga Municipalities",
                "Budhinanda Municipalities",
                "Gaumul Rural Municipalities",
                "Pandab Gupha Rural Municipalities",
                "Swamikartik Rural Municipalities",
                "Chhededaha Rural Municipalities",
                "Himali Rural Municipalities"
        )

        districtToMunicipalityMap["Dadeldhura"] = arrayOf(
                "Amargadhi Municipalities",
                "Parshuram Municipalities",
                "Aalital Rural Municipalities",
                "Bhagedhar Rural Municipalities",
                "Nawadurga Rural Municipalities",
                "Ajayameru Rural Municipalities",
                "Ganyapdhuraa Rural Municipalities"
        )

        districtToMunicipalityMap["Darchula"] = arrayOf(
                "Mahakali Municipalities",
                "Shailyashikhar Municipalities",
                "Malikarjun Rural Municipalities",
                "Apihimal Rural Municipalities",
                "Dunhu Rural Municipalities",
                "Naugad Rural Municipalities",
                "Marma Rural Municipalities",
                "Lekam Rural Municipalities",
                "Byans Rural Municipalities",
                "Dhap S H K"
        )

        districtToMunicipalityMap["Doti"] = arrayOf(
                "Dipayal Silgadhi Municipalities",
                "Shikhar Municipalities",
                "Purbichauki Rural Municipalities",
                "Badikedar Rural Municipalities",
                "Jorayal Rural Municipalities",
                "Sayal Rural Municipalities",
                "Aadarsha Rural Municipalities",
                "ke.Aai.Singh Rural Municipalities",
                "Bogataan Rural Municipalities"
        )

        districtToMunicipalityMap["Kailali"] = arrayOf(
                "Dhangadhi SMM",
                "Tikapur Municipalities",
                "Ghodaaghodi Municipalities",
                "Lamkichuhaa Municipalities",
                "Bhajani Municipalities",
                "Godawari Municipalities",
                "Gauriganga Municipalities",
                "Janaki Rural Municipalities",
                "Bardagoriyaa Rural Municipalities",
                "Mohanyaal Rural Municipalities",
                "Kailari Rural Municipalities",
                "Joshipur Rural Municipalities",
                "Chure Rural Municipalities"
        )

        districtToMunicipalityMap["Kanchanpur"] = arrayOf(
                "Bheemdattnagar Municipality",
                "Punarwas Municipality",
                "Bedkot Municipality",
                "Mahakali Municipalities",
                "Shuklaphata Municipalities",
                "Belauri Municipalities ",
                "Krishnapur Municipalities",
                "Beldaandi Rural Municipalities",
                "Laljhaadi Rural Municipalities"
        )

        view.rvMunicipality.layoutManager =
                LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        val list = ArrayList<String>()
        for (m in districtToMunicipalityMap[district]!!) {
            list.add(m)
        }
        list.sort()
        view.rvMunicipality.adapter = StringAdapter(list)
                .apply {
                    onItemClickListener = object :
                            OnItemClickListener {
                        override fun onItemClicked(position: Int) {
                            municipalityBottomSheet.isCollapsed = true
                            val selectedPosition = list[position]
                            if (selectedPosition != view.etMunicipality.text.toString()) {
                                view.etMunicipality.setText(selectedPosition)
                            }
                        }
                    }
                }
    }

    private fun allFieldValid(): Boolean {
        if (etProvince.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select Province", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (etDistrict.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select District", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (etMunicipality.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select Municipality", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (etWardNo.text.toString().isEmpty()) {
            tilWardNo.isErrorEnabled = true
            tilWardNo.error = "Please enter ward no."
            etWardNo.requestFocus()
            return false
        } else {
            tilWardNo.isErrorEnabled = false
            tilWardNo.error = null
            etWardNo.clearFocus()
        }

        if (etRespondentName.text.toString().isEmpty()) {
            tilRespondentName.isErrorEnabled = true
            tilRespondentName.error = "Please enter respondent name."
            etRespondentName.requestFocus()
            return false
        } else {
            tilRespondentName.isErrorEnabled = false
            tilRespondentName.error = null
            etRespondentName.clearFocus()
        }

        if (etEthnic.text.toString().isEmpty()) {
            tilEthnic.isErrorEnabled = true
            tilEthnic.error = "Please enter Ethnic/Caste."
            etEthnic.requestFocus()
            return false
        } else {
            tilEthnic.isErrorEnabled = false
            tilEthnic.error = null
            etEthnic.clearFocus()
        }

        if (etAge.text.toString().isEmpty()) {
            tilAge.isErrorEnabled = true
            tilAge.error = "Please enter valid age."
            etAge.requestFocus()
            return false
        } else {
            tilAge.isErrorEnabled = false
            tilAge.error = null
            etAge.clearFocus()
        }

        if (etEducation.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select Education", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (etReligion.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select Religion", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (etOccupation.text.toString().isEmpty()) {
            Snackbar.make(clFragmentRoot, "Please select Occupation", Snackbar.LENGTH_SHORT)
                    .show()
            return false
        }

        if (imageFileName.isNullOrEmpty()) {

        }

        return true
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnPreviousPage -> {
                onRespondentDetailListener?.onPreviousClicked()
            }
            R.id.btnGoToQuestionnaire -> {
                if (allFieldValid()) {
                    val province = etProvince.text.toString()
                    val district = etDistrict.text.toString()
                    val municipality = etMunicipality.text.toString()
                    val wardNo = etWardNo.text.toString()
                    val respondentName = etRespondentName.text.toString()
                    val caste = etEthnic.text.toString()
                    val contactNo = etContactNo.text.toString()
                    val age = etAge.text.toString()
                    val gender = when {
                        rbMale.isChecked -> "Male"
                        rbFemale.isChecked -> "Female"
                        else -> "Others"
                    }

                    val education = etEducation.text.toString()
                    val religion = etReligion.text.toString()
                    val occupation = etOccupation.text.toString()
                    onRespondentDetailListener?.onNextPageClicked(
                            province,
                            district,
                            municipality,
                            wardNo,
                            respondentName,
                            caste,
                            contactNo,
                            age,
                            gender,
                            education,
                            religion,
                            occupation,
                            imageFileName
                    )
                }
            }
            R.id.etOccupation -> {
                occupationBottomSheet.isExpanded = true
            }
            R.id.etReligion -> {
                religionBottomSheet.isExpanded = true
            }
            R.id.etEducation -> {
                educationBottomSheet.isExpanded = true
            }
            R.id.etProvince -> {
                provinceBottomSheet.isExpanded = true
            }
            R.id.etDistrict -> {
                if (etProvince.text.toString().isEmpty()) {
                    Snackbar.make(
                            clFragmentRoot,
                            "Please select Province first",
                            Snackbar.LENGTH_SHORT
                    )
                            .show()
                } else {
                    districtBottomSheet.isExpanded = true
                }
            }
            R.id.etMunicipality -> {
                if (etDistrict.text.toString().isEmpty()) {
                    Snackbar.make(
                            clFragmentRoot,
                            "Please select District first",
                            Snackbar.LENGTH_SHORT
                    )
                            .show()
                } else {
                    municipalityBottomSheet.isExpanded = true
                }
            }

            R.id.ivProfile -> {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                imageUri = getFileUri(
                        context!!,
                        getImageFile(context!!, "image_")
                )
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                cameraIntent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                if (cameraIntent.resolveActivity(context!!.packageManager) != null) {
                    startActivityForResult(Intent.createChooser(cameraIntent, "Take picture"), rcCamera)
                }
            }
        }
    }
    interface OnRespondentDetailListener {
        fun onPreviousClicked()
        fun onNextPageClicked(
                province: String,
                district: String,
                municipality: String,
                wardNo: String,
                respondentName: String,
                caste: String,
                contactNo: String,
                age: String,
                sex: String,
                education: String,
                religion: String,
                occupation: String,
                imageFileName: String?
        )
    }
    private fun getImageFile(context: Context, imageTag: String): File? {
        val baseDirectory = getBaseDirectory(context)
        imageFileName = "${imageTag}_${System.currentTimeMillis()}.png"
        return File(baseDirectory, imageFileName)
    }

    private fun getFileUri(context: Context, file: File?): Uri? {
        return if (file != null) {
            FileProvider.getUriForFile(context, "${context.packageName}.provider", file)
        } else {
            null
        }
    }

    private fun getBaseDirectory(context: Context): File? {
        return context.filesDir
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            rcCamera -> {
                /*CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(activity!!)*/
                ivProfile.setImageBitmap(MediaStore.Images.Media.getBitmap(context!!.contentResolver, imageUri))
            }
            /*CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val cropUri = result.uri
                    if (cropUri != null) {
                        val f = File(cropUri.path)
                        val bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, cropUri)
                        if (f.exists()) {
                            f.delete()
                        }
                        try {
                            val out = FileOutputStream(f)
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                            out.flush()
                            out.close()
                        } catch (e: Exception) {
                            this@RespondentDetailFragment.javaClass.log("Error while saving image: ${e.localizedMessage}")
                        }
                    }
                } else {

                }
            }*/
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
/*

    private fun internalStorage() {
        val internalBasePath = getFilesDir()
        val internalFileTextPath = File(internalBasePath, "Internal Content")
        if (!internalFileTextPath.exists()) {
            internalFileTextPath.mkdir()
        }
        val internalFile = File(internalFileTextPath, "internal_text_file.txt")
        try {
            internalFile.createNewFile()
            val fos = FileOutputStream(internalFile, true)
            fos.write("Internal Content of the text file".toByteArray())
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
*/


}
