package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_question_five_point_one_ii_v2.view.*

class QuestionFivePointOneIiV2 : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()
    private var respondentId = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(QuestionOneFragmentV2.KEY_QUESTION)
        respondentId = arguments?.getInt(KEY_RESPONDENT_ID) ?: 0
        answerDtoList.addAll(AnswerDao(context!!).getAllAnswerInfoByRespondent(respondentId))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_five_point_one_ii_v2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
                String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }

        view.btnNext.setOnClickListener {
            onViewPagerActionListener?.onNextClicked(answerDtoList)
        }
        view.btnPrevious.setOnClickListener {
            onViewPagerActionListener?.onPreviousClick()

        }

        view.cbM5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 284) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 284
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 284) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 285) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 285
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 285) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 286) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 286
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 286) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 287) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 287
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 287) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 288) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 288
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 288) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 289) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 289
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 289) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 290) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 125
                            this.answerOptionId = 290
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 125 && answer.answerOptionId == 290) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbA12.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 291) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 291
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 291) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA1.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 292) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 292
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 292) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA2.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 293) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 293
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 293) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA3.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 294) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 294
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 294) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA4.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 295) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 295
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 295) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 296) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 126
                            this.answerOptionId = 296
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 126 && answer.answerOptionId == 296) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbE6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 297) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 297
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 297) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 298) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 298
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 298) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 299) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 299
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 299) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 300) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 300
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 300) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 301) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 301
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 301) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 302) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIiV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 127
                            this.answerOptionId = 302
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIiV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 127 && answer.answerOptionId == 302) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
    }
}