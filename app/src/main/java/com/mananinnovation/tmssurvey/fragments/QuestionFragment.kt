package com.mananinnovation.tmssurvey.fragments

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.Questions
import kotlinx.android.synthetic.main.fragment_question.*
import kotlinx.android.synthetic.main.fragment_question.view.*

class QuestionFragment : Fragment() {
    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Questions? = null
    private var padding = 0
    private var childCount = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
        padding = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionHeader ?: "No question header"
        view.tvQuestionText.text = question?.questionText ?: "No question text"
        if (question?.children?.isEmpty()!!) {
            llSubQuestionContainer.addView(getAnswerOptionView(question!!))
        } else {
            llSubQuestionContainer.addView(getSubQuestionList(question!!))
        }
    }

    private fun getSubQuestionList(questions: Questions): View {
        val llQuestion = LinearLayout(context!!)
        llQuestion.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llQuestion.orientation = LinearLayout.VERTICAL
        for (subQuestion in questions.children) {
            val tvSubQuestionText = TextView(context!!)
            tvSubQuestionText.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            tvSubQuestionText.setTextColor(Color.parseColor("#212121"))
            tvSubQuestionText.text = subQuestion.questionText
            tvSubQuestionText.setPadding(
                padding * childCount,
                padding / 2,
                padding / 2,
                padding / 2
            )
            llQuestion.addView(tvSubQuestionText)
            if (subQuestion.children.isEmpty()) {
                llQuestion.addView(getAnswerOptionView(subQuestion))
            } else {
                tvSubQuestionText.setTypeface(null, Typeface.BOLD)
                llQuestion.addView(getSubQuestionList(subQuestion))
            }
        }
        childCount = 1
        return llQuestion
    }

    private fun getAnswerOptionView(questions: Questions): View {
        if (questions.type == "SingleSelect") {
            val rgContainer = RadioGroup(context!!)
            rgContainer.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            rgContainer.orientation = RadioGroup.VERTICAL
            rgContainer.setPadding(
                padding + padding + padding,
                padding / 2,
                padding / 2,
                padding / 2
            )

            for (option in questions.answerOptionList) {
                val rbOption = RadioButton(context)
                rbOption.layoutParams =
                    RadioGroup.LayoutParams(
                        RadioGroup.LayoutParams.WRAP_CONTENT,
                        RadioGroup.LayoutParams.WRAP_CONTENT
                    )
                rbOption.text = option.option
                rgContainer.addView(rbOption)
            }
            return rgContainer
        } else {
            return LinearLayout(context)
        }
    }

}
