package com.mananinnovation.tmssurvey.fragments

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.Questions
import kotlinx.android.synthetic.main.fragment_question_one.*
import kotlinx.android.synthetic.main.fragment_quetion_three.view.*

class QuestionThreeFragment : Fragment() {
    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Questions? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_quetion_three, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionHeader
        view.tvQuestionText.text = question?.questionText
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        val headerView = getHeaderView()
//        view.llContainer.addView(headerView)

        for (i in 0 until question?.children?.size!!) {
            val subQues = question?.children!![i]
            val llQuestion = LinearLayout(context)
            llQuestion.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            llQuestion.orientation = LinearLayout.HORIZONTAL
            llQuestion.weightSum = 100f
            llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

            val tvSno = TextView(context)
            val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
            lp.topMargin = dimen1dp
            lp.marginStart = dimen2dp
            lp.marginEnd = dimen1dp
            if (i == question?.children?.size!! - 1) {
                lp.bottomMargin = dimen2dp
            } else {
                lp.bottomMargin = dimen1dp
            }
            tvSno.layoutParams = lp
            tvSno.text = String.format("%d", (i + 1))
            tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
            tvSno.setBackgroundColor(getBackgroundColor())
            tvSno.gravity = Gravity.CENTER
            tvSno.setTextColor(Color.parseColor("#212121"))
            llQuestion.addView(tvSno)

            if (subQues.type == "CustomQuestion") {
                val tvQues = EditText(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.hint = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                tvQues.setLines(10)
                tvQues.gravity = Gravity.TOP
                llQuestion.addView(tvQues)
            } else {
                val tvQues = TextView(context)
                val lpQues =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
                lpQues.topMargin = dimen1dp
                lpQues.marginStart = dimen1dp
                lpQues.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpQues.bottomMargin = dimen2dp
                } else {
                    lpQues.bottomMargin = dimen1dp
                }
                tvQues.layoutParams = lpQues
                tvQues.text = String.format("%s", subQues.questionText)
                tvQues.setBackgroundColor(getBackgroundColor())
                tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                tvQues.setTextColor(Color.parseColor("#212121"))
                llQuestion.addView(tvQues)
            }

            if (subQues.children.isEmpty()) {
                if (subQues.type == "SingleSelect") {
                    val cbOne = RadioButton(context)
                    val cbTwo = RadioButton(context)
                    val cbThree = RadioButton(context)
                    val cbFour = RadioButton(context)
                    val cbFive = RadioButton(context)

                    val lpOne =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpOne.topMargin = dimen1dp
                    lpOne.marginStart = dimen1dp
                    lpOne.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOne.bottomMargin = dimen2dp
                    } else {
                        lpOne.bottomMargin = dimen1dp
                    }
                    cbOne.layoutParams = lpOne
//                tvYes.text = String.format("Yes")
                    cbOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbOne.gravity = Gravity.CENTER
                    cbOne.setBackgroundColor(getBackgroundColor())
                    cbOne.setTextColor(Color.parseColor("#212121"))
                    cbOne.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbOne)

                    val lpTwo =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpTwo.topMargin = dimen1dp
                    lpTwo.marginStart = dimen1dp
                    lpTwo.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpTwo.bottomMargin = dimen2dp
                    } else {
                        lpTwo.bottomMargin = dimen1dp
                    }
                    cbTwo.layoutParams = lpTwo
//                tvNo.text = String.format("No")
                    cbTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbTwo.gravity = Gravity.CENTER
                    cbTwo.setBackgroundColor(getBackgroundColor())
                    cbTwo.setTextColor(Color.parseColor("#212121"))
                    cbTwo.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbTwo)

                    val lpThree =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpThree.topMargin = dimen1dp
                    lpThree.marginStart = dimen1dp
                    lpThree.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpThree.bottomMargin = dimen2dp
                    } else {
                        lpThree.bottomMargin = dimen1dp
                    }
                    cbThree.layoutParams = lpThree
//                tvNo.text = String.format("No")
                    cbThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbThree.gravity = Gravity.CENTER
                    cbThree.setBackgroundColor(getBackgroundColor())
                    cbThree.setTextColor(Color.parseColor("#212121"))
                    cbThree.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbFour.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbThree)

                    val lpFour =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFour.topMargin = dimen1dp
                    lpFour.marginStart = dimen1dp
                    lpFour.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpFour.bottomMargin = dimen2dp
                    } else {
                        lpFour.bottomMargin = dimen1dp
                    }
                    cbFour.layoutParams = lpFour
//                tvNo.text = String.format("No")
                    cbFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbFour.gravity = Gravity.CENTER
                    cbFour.setBackgroundColor(getBackgroundColor())
                    cbFour.setTextColor(Color.parseColor("#212121"))
                    cbFour.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFive.isChecked = false
                        }
                    }
                    llQuestion.addView(cbFour)

                    val lpFive =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFive.topMargin = dimen1dp
                    lpFive.marginStart = dimen1dp
                    lpFive.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpFive.bottomMargin = dimen2dp
                    } else {
                        lpFive.bottomMargin = dimen1dp
                    }
                    cbFive.layoutParams = lpFive
//                tvNo.text = String.format("No")
                    cbFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    cbFive.gravity = Gravity.CENTER
                    cbFive.setBackgroundColor(getBackgroundColor())
                    cbFive.setTextColor(Color.parseColor("#212121"))
                    cbFive.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            cbOne.isChecked = false
                            cbTwo.isChecked = false
                            cbThree.isChecked = false
                            cbFour.isChecked = false
                        }
                    }
                    llQuestion.addView(cbFive)
                } else if (subQues.type == "CustomQuestion") {
                    val viewOne = View(context)
                    val lpOne =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpOne.topMargin = dimen1dp
                    lpOne.marginStart = dimen1dp
                    lpOne.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOne.bottomMargin = dimen2dp
                    } else {
                        lpOne.bottomMargin = dimen1dp
                    }
                    viewOne.layoutParams = lpOne
                    viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewOne.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(viewOne)

                    val viewTwo = View(context)
                    val lpTwo =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpTwo.topMargin = dimen1dp
                    lpTwo.marginStart = dimen1dp
                    lpTwo.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpTwo.bottomMargin = dimen2dp
                    } else {
                        lpTwo.bottomMargin = dimen1dp
                    }
                    viewTwo.layoutParams = lpTwo
                    viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewTwo.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(viewTwo)

                    val viewThree = View(context)
                    val lpThree =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpThree.topMargin = dimen1dp
                    lpThree.marginStart = dimen1dp
                    lpThree.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpThree.bottomMargin = dimen2dp
                    } else {
                        lpThree.bottomMargin = dimen1dp
                    }
                    viewThree.layoutParams = lpThree
                    viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewThree.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(viewThree)

                    val viewFour = View(context)
                    val lpFour =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFour.topMargin = dimen1dp
                    lpFour.marginStart = dimen1dp
                    lpFour.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpFour.bottomMargin = dimen2dp
                    } else {
                        lpFour.bottomMargin = dimen1dp
                    }
                    viewFour.layoutParams = lpFour
                    viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewFour.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(viewFour)

                    val viewFive = View(context)
                    val lpFive =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                    lpFive.topMargin = dimen1dp
                    lpFive.marginStart = dimen1dp
                    lpFive.marginEnd = dimen2dp
                    if (i == question?.children?.size!! - 1) {
                        lpFive.bottomMargin = dimen2dp
                    } else {
                        lpFive.bottomMargin = dimen1dp
                    }
                    viewFive.layoutParams = lpFive
                    viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewFive.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llQuestion.addView(viewFive)
                }
            } else {
                val viewOne = View(context)
                val lpOne =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpOne.topMargin = dimen1dp
                lpOne.marginStart = dimen1dp
                lpOne.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpOne.bottomMargin = dimen2dp
                } else {
                    lpOne.bottomMargin = dimen1dp
                }
                viewOne.layoutParams = lpOne
                viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewOne.setBackgroundColor(
                    getBackgroundColor()
                )
                llQuestion.addView(viewOne)

                val viewTwo = View(context)
                val lpTwo =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpTwo.topMargin = dimen1dp
                lpTwo.marginStart = dimen1dp
                lpTwo.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpTwo.bottomMargin = dimen2dp
                } else {
                    lpTwo.bottomMargin = dimen1dp
                }
                viewTwo.layoutParams = lpTwo
                viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewTwo.setBackgroundColor(
                    getBackgroundColor()
                )
                llQuestion.addView(viewTwo)

                val viewThree = View(context)
                val lpThree =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpThree.topMargin = dimen1dp
                lpThree.marginStart = dimen1dp
                lpThree.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpThree.bottomMargin = dimen2dp
                } else {
                    lpThree.bottomMargin = dimen1dp
                }
                viewThree.layoutParams = lpThree
                viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewThree.setBackgroundColor(
                    getBackgroundColor()
                )
                llQuestion.addView(viewThree)

                val viewFour = View(context)
                val lpFour =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpFour.topMargin = dimen1dp
                lpFour.marginStart = dimen1dp
                lpFour.marginEnd = dimen1dp
                if (i == question?.children?.size!! - 1) {
                    lpFour.bottomMargin = dimen2dp
                } else {
                    lpFour.bottomMargin = dimen1dp
                }
                viewFour.layoutParams = lpFour
                viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewFour.setBackgroundColor(
                    getBackgroundColor()
                )
                llQuestion.addView(viewFour)

                val viewFive = View(context)
                val lpFive =
                    LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                lpFive.topMargin = dimen1dp
                lpFive.marginStart = dimen1dp
                lpFive.marginEnd = dimen2dp
                if (i == question?.children?.size!! - 1) {
                    lpFive.bottomMargin = dimen2dp
                } else {
                    lpFive.bottomMargin = dimen1dp
                }
                viewFive.layoutParams = lpFive
                viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                viewFive.setBackgroundColor(
                    getBackgroundColor()
                )
                llQuestion.addView(viewFive)
            }
            view.llContainer.addView(llQuestion)

            if (subQues.children.isNotEmpty()) {
                for (j in 0 until subQues.children.size) {
                    val subSubQues = subQues.children[j]
                    val llSubSub = LinearLayout(context)
                    llSubSub.layoutParams =
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    llSubSub.orientation = LinearLayout.HORIZONTAL
                    llSubSub.weightSum = 100f
                    llSubSub.setBackgroundColor(
                        ContextCompat.getColor(context!!, android.R.color.black)
                    )

                    val viewSnoEmpty = View(context)
                    val lpSnoEmpty =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
                    lpSnoEmpty.topMargin = dimen1dp
                    lpSnoEmpty.marginStart = dimen2dp
                    lpSnoEmpty.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpSnoEmpty.bottomMargin = dimen2dp
                    } else {
                        lpSnoEmpty.bottomMargin = dimen1dp
                    }
                    viewSnoEmpty.layoutParams = lpSnoEmpty
                    viewSnoEmpty.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    viewSnoEmpty.setBackgroundColor(
                        getBackgroundColor()
                    )
                    llSubSub.addView(viewSnoEmpty)

                    val tvOptions = TextView(context)
                    val lpOptions =
                        LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
                    lpOptions.topMargin = dimen1dp
                    lpOptions.marginStart = dimen1dp
                    lpOptions.marginEnd = dimen1dp
                    if (i == question?.children?.size!! - 1) {
                        lpOptions.bottomMargin = dimen2dp
                    } else {
                        lpOptions.bottomMargin = dimen1dp
                    }
                    tvOptions.layoutParams = lpOptions
                    tvOptions.text = Html.fromHtml(
                        "\u2022 ${subSubQues.questionText}"
                    )
                    tvOptions.setBackgroundColor(
                        getBackgroundColor()
                    )
                    tvOptions.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                    tvOptions.setTextColor(Color.parseColor("#212121"))
                    llSubSub.addView(tvOptions)

                    if (subSubQues.type == "SingleSelect") {
                        val cbOne = RadioButton(context)
                        val cbTwo = RadioButton(context)
                        val cbThree = RadioButton(context)
                        val cbFour = RadioButton(context)
                        val cbFive = RadioButton(context)

                        val lpOne =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpOne.topMargin = dimen1dp
                        lpOne.marginStart = dimen1dp
                        lpOne.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOne.bottomMargin = dimen2dp
                        } else {
                            lpOne.bottomMargin = dimen1dp
                        }
                        cbOne.layoutParams = lpOne
//                tvYes.text = String.format("Yes")
                        cbOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbOne.gravity = Gravity.CENTER
                        cbOne.setBackgroundColor(getBackgroundColor())
                        cbOne.setTextColor(Color.parseColor("#212121"))
                        cbOne.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbOne)

                        val lpTwo =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpTwo.topMargin = dimen1dp
                        lpTwo.marginStart = dimen1dp
                        lpTwo.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpTwo.bottomMargin = dimen2dp
                        } else {
                            lpTwo.bottomMargin = dimen1dp
                        }
                        cbTwo.layoutParams = lpTwo
//                tvNo.text = String.format("No")
                        cbTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbTwo.gravity = Gravity.CENTER
                        cbTwo.setBackgroundColor(getBackgroundColor())
                        cbTwo.setTextColor(Color.parseColor("#212121"))
                        cbTwo.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbTwo)

                        val lpThree =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpThree.topMargin = dimen1dp
                        lpThree.marginStart = dimen1dp
                        lpThree.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpThree.bottomMargin = dimen2dp
                        } else {
                            lpThree.bottomMargin = dimen1dp
                        }
                        cbThree.layoutParams = lpThree
//                tvNo.text = String.format("No")
                        cbThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbThree.gravity = Gravity.CENTER
                        cbThree.setBackgroundColor(getBackgroundColor())
                        cbThree.setTextColor(Color.parseColor("#212121"))
                        cbThree.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbFour.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbThree)

                        val lpFour =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFour.topMargin = dimen1dp
                        lpFour.marginStart = dimen1dp
                        lpFour.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpFour.bottomMargin = dimen2dp
                        } else {
                            lpFour.bottomMargin = dimen1dp
                        }
                        cbFour.layoutParams = lpFour
//                tvNo.text = String.format("No")
                        cbFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbFour.gravity = Gravity.CENTER
                        cbFour.setBackgroundColor(getBackgroundColor())
                        cbFour.setTextColor(Color.parseColor("#212121"))
                        cbFour.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFive.isChecked = false
                            }
                        }
                        llSubSub.addView(cbFour)

                        val lpFive =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFive.topMargin = dimen1dp
                        lpFive.marginStart = dimen1dp
                        lpFive.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpFive.bottomMargin = dimen2dp
                        } else {
                            lpFive.bottomMargin = dimen1dp
                        }
                        cbFive.layoutParams = lpFive
//                tvNo.text = String.format("No")
                        cbFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        cbFive.gravity = Gravity.CENTER
                        cbFive.setBackgroundColor(getBackgroundColor())
                        cbFive.setTextColor(Color.parseColor("#212121"))
                        cbFive.setOnCheckedChangeListener { _, isChecked ->
                            if (isChecked) {
                                cbOne.isChecked = false
                                cbTwo.isChecked = false
                                cbThree.isChecked = false
                                cbFour.isChecked = false
                            }
                        }
                        llSubSub.addView(cbFive)
                    } else {
                        val viewOne = View(context)
                        val lpOne =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpOne.topMargin = dimen1dp
                        lpOne.marginStart = dimen1dp
                        lpOne.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpOne.bottomMargin = dimen2dp
                        } else {
                            lpOne.bottomMargin = dimen1dp
                        }
                        viewOne.layoutParams = lpOne
                        viewOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewOne.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewOne)

                        val viewTwo = View(context)
                        val lpTwo =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpTwo.topMargin = dimen1dp
                        lpTwo.marginStart = dimen1dp
                        lpTwo.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpTwo.bottomMargin = dimen2dp
                        } else {
                            lpTwo.bottomMargin = dimen1dp
                        }
                        viewTwo.layoutParams = lpTwo
                        viewTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewTwo.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewTwo)

                        val viewThree = View(context)
                        val lpThree =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpThree.topMargin = dimen1dp
                        lpThree.marginStart = dimen1dp
                        lpThree.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpThree.bottomMargin = dimen2dp
                        } else {
                            lpThree.bottomMargin = dimen1dp
                        }
                        viewThree.layoutParams = lpThree
                        viewThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewThree.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewThree)

                        val viewFour = View(context)
                        val lpFour =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFour.topMargin = dimen1dp
                        lpFour.marginStart = dimen1dp
                        lpFour.marginEnd = dimen1dp
                        if (i == question?.children?.size!! - 1) {
                            lpFour.bottomMargin = dimen2dp
                        } else {
                            lpFour.bottomMargin = dimen1dp
                        }
                        viewFour.layoutParams = lpFour
                        viewFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewFour.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewFour)

                        val viewFive = View(context)
                        val lpFive =
                            LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
                        lpFive.topMargin = dimen1dp
                        lpFive.marginStart = dimen1dp
                        lpFive.marginEnd = dimen2dp
                        if (i == question?.children?.size!! - 1) {
                            lpFive.bottomMargin = dimen2dp
                        } else {
                            lpFive.bottomMargin = dimen1dp
                        }
                        viewFive.layoutParams = lpFive
                        viewFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
                        viewFive.setBackgroundColor(
                            getBackgroundColor()
                        )
                        llSubSub.addView(viewFive)
                    }

                    view.llContainer.addView(llSubSub)
                }
            }
        }
    }

    private fun getHeaderView(): View {
        val dimen2dp = resources.getDimensionPixelSize(R.dimen.dimen_2dp)
        val dimen1dp = resources.getDimensionPixelSize(R.dimen.dimen_1dp)
        val dimen8dp = resources.getDimensionPixelSize(R.dimen.dimen_8dp)
        val llQuestion = LinearLayout(context)
        llQuestion.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llQuestion.orientation = LinearLayout.HORIZONTAL
        llQuestion.weightSum = 100f
        llQuestion.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.black))

        val tvSno = TextView(context)
        val lp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 12f)
        lp.topMargin = dimen2dp
        lp.marginStart = dimen2dp
        lp.marginEnd = dimen1dp
        lp.bottomMargin = dimen1dp
        tvSno.layoutParams = lp
        tvSno.text = String.format("S.No.")
        tvSno.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvSno.gravity = Gravity.CENTER
        tvSno.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvSno.setTextColor(Color.parseColor("#212121"))
        tvSno.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvSno)


        val tvQues = TextView(context)
        val lpQues = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 48f)
        lpQues.topMargin = dimen2dp
        lpQues.marginStart = dimen1dp
        lpQues.marginEnd = dimen1dp
        lpQues.bottomMargin = dimen1dp
        tvQues.layoutParams = lpQues
        tvQues.text = String.format("Types of Services")
        tvQues.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvQues.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvQues.gravity = Gravity.CENTER
        tvQues.setTextColor(Color.parseColor("#212121"))
        tvQues.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvQues)

        val tvOne = TextView(context)
        val lpOne = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
        lpOne.topMargin = dimen2dp
        lpOne.marginStart = dimen1dp
        lpOne.marginEnd = dimen1dp
        lpOne.bottomMargin = dimen1dp
        tvOne.layoutParams = lpOne
        tvOne.text = String.format("1")
        tvOne.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvOne.gravity = Gravity.CENTER
        tvOne.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvOne.setTextColor(Color.parseColor("#212121"))
        tvOne.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvOne)

        val tvTwo = TextView(context)
        val lpTwo = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
        lpTwo.topMargin = dimen2dp
        lpTwo.marginStart = dimen1dp
        lpTwo.marginEnd = dimen1dp
        lpTwo.bottomMargin = dimen1dp
        tvTwo.layoutParams = lpTwo
        tvTwo.text = String.format("2")
        tvTwo.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvTwo.gravity = Gravity.CENTER
        tvTwo.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvTwo.setTextColor(Color.parseColor("#212121"))
        tvTwo.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvTwo)

        val tvThree = TextView(context)
        val lpThree = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
        lpThree.topMargin = dimen2dp
        lpThree.marginStart = dimen1dp
        lpThree.marginEnd = dimen1dp
        lpThree.bottomMargin = dimen1dp
        tvThree.layoutParams = lpThree
        tvThree.text = String.format("3")
        tvThree.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvThree.gravity = Gravity.CENTER
        tvThree.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvThree.setTextColor(Color.parseColor("#212121"))
        tvThree.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvThree)

        val tvFour = TextView(context)
        val lpFour = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
        lpFour.topMargin = dimen2dp
        lpFour.marginStart = dimen1dp
        lpFour.marginEnd = dimen1dp
        lpFour.bottomMargin = dimen1dp
        tvFour.layoutParams = lpFour
        tvFour.text = String.format("4")
        tvFour.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvFour.gravity = Gravity.CENTER
        tvFour.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvFour.setTextColor(Color.parseColor("#212121"))
        tvFour.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvFour)

        val tvFive = TextView(context)
        val lpFive = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 8f)
        lpFive.topMargin = dimen2dp
        lpFive.marginStart = dimen1dp
        lpFive.marginEnd = dimen2dp
        lpFive.bottomMargin = dimen1dp
        tvFive.layoutParams = lpFive
        tvFive.text = String.format("5")
        tvFive.setPadding(dimen8dp, dimen8dp, dimen8dp, dimen8dp)
        tvFive.gravity = Gravity.CENTER
        tvFive.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        tvFive.setTextColor(Color.parseColor("#212121"))
        tvFive.setTypeface(null, Typeface.BOLD)
        llQuestion.addView(tvFive)

        return llQuestion
    }

    private fun getBackgroundColor(): Int {
        return if (llContainer.childCount % 2 != 0) {
            Color.parseColor("#c4e3ed")
        } else {
            Color.WHITE
        }
    }
}
