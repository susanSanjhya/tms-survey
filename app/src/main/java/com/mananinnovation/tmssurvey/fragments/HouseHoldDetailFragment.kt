package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.widgets.CustomProgressDialog
import kotlinx.android.synthetic.main.fragment_household_detail.*
import kotlinx.android.synthetic.main.fragment_household_detail.view.*
import java.util.*

class HouseHoldDetailFragment : Fragment(), LocationListener {
    private var onHouseDetailEntered: OnHouseDetailEntered? = null
    private var customProgressDialog: CustomProgressDialog? = null

    private var locationManager: LocationManager? = null
    private var latitude = 0.0
    private var longitude = 0.0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnHouseDetailEntered) {
            onHouseDetailEntered = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_household_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        showProgressDialog()
//        locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//        locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 5f, this)
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + 1
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val monthString = if (month < 10) {
            "0$month"
        } else {
            month.toString()
        }
        val dayString = if (day < 10) {
            "0$day"
        } else {
            day.toString()
        }

        etDate.setText(String.format("%d-%s-%s", year, monthString, dayString))
        view.btnNextPage.setOnClickListener {
            if (allFieldValid()) {
                val houseHoldNumber = etHouseholdNumber.text.toString()
                val date = etDate.text.toString()
                onHouseDetailEntered?.onEntered(
                    houseHoldNumber,
                    date,
                    latitude.toString(),
                    longitude.toString(),
                    0.0.toString()
                )
            }
        }
    }

    private fun allFieldValid(): Boolean {
        if (etHouseholdNumber.text.toString().isEmpty()) {
            tilHouseholdNo.isErrorEnabled = true
            tilHouseholdNo.error = "Enter house hold number."
            etHouseholdNumber.requestFocus()
            return false
        } else {
            tilHouseholdNo.isErrorEnabled = false
            tilHouseholdNo.error = null
            etHouseholdNumber.clearFocus()
        }
        return true
    }

    private fun showProgressDialog() {
        customProgressDialog =
            childFragmentManager.findFragmentByTag(CustomProgressDialog.TAG) as CustomProgressDialog?
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog()
        }
        customProgressDialog?.arguments = Bundle()
            .apply {
                putString(CustomProgressDialog.KEY_MESSAGE, "Finding latitude and longitude...")
            }
        customProgressDialog?.show(childFragmentManager, CustomProgressDialog.TAG)
    }

    override fun onLocationChanged(location: Location?) {
        customProgressDialog?.dismiss()
        latitude = location?.latitude ?: 0.0
        longitude = location?.longitude ?: 0.0
        etLatitude.setText(String.format("%.02f", location?.latitude))
        etLongitude.setText(String.format("%.02f", location?.longitude))
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

    }

    override fun onProviderEnabled(p0: String?) {

    }

    override fun onProviderDisabled(p0: String?) {

    }

    interface OnHouseDetailEntered {
        fun onEntered(
            houseHoldNumber: String?,
            date: String?,
            latitude: String?,
            longitude: String?,
            elevation: String?
        )
    }
}
