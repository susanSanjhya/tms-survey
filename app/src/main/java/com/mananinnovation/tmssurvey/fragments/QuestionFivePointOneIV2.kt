package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_question_five_point_one_i_v2.view.*

class QuestionFivePointOneIV2 : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()
    private var respondentId = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(QuestionOneFragmentV2.KEY_QUESTION)
        respondentId = arguments?.getInt(KEY_RESPONDENT_ID) ?: 0
        answerDtoList.addAll(AnswerDao(context!!).getAllAnswerInfoByRespondent(respondentId))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_five_point_one_i_v2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
                String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }

        view.btnNext.setOnClickListener {
            onViewPagerActionListener?.onNextClicked(answerDtoList)
        }
        view.btnPrevious.setOnClickListener {
            onViewPagerActionListener?.onPreviousClick()
        }

        view.cbM5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 265) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 265
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 265) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 266) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 266
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 266) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 267) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 267
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 267) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 268) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 268
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 268) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 269) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 269
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 269) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 270) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 270
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 270) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 271) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 122
                            this.answerOptionId = 271
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 122 && answer.answerOptionId == 271) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbA12.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 272) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 272
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 272) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA1.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 273) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 273
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 273) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA2.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 274) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 274
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 274) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA3.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 275) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 275
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 275) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA4.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 276) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 276
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 276) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 277) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 123
                            this.answerOptionId = 277
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 123 && answer.answerOptionId == 277) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbE6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 278) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 278
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 278) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 279) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 279
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 279) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 280) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 280
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 280) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 281) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 281
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 281) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 282) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 282
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 282) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 283) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFivePointOneIV2.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 124
                            this.answerOptionId = 283
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFivePointOneIV2.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 124 && answer.answerOptionId == 283) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
    }
}