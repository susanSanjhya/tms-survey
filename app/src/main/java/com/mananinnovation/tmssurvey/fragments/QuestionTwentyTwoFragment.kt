package com.mananinnovation.tmssurvey.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.activities.MainActivity
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import kotlinx.android.synthetic.main.fragment_question_tewenty_two_v2.*
import kotlinx.android.synthetic.main.fragment_question_tewenty_two_v2.view.*

class QuestionTwentyTwoFragment : Fragment() {
    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_tewenty_two_v2, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
                String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }

        view.btnDone.setOnClickListener {
            val answer = etAnswer.text.toString()
            if (answer.isEmpty()) {
                Snackbar.make(clRoot, "Answer is empty", Snackbar.LENGTH_SHORT)
                        .show()
            } else {
                val answerDto = AnswerDto()
                        .apply {
                            AnswerDto()
                                    .apply {
                                        this.questionnaireId = question?.questionnaireId ?: 0
                                        this.principalQuestionId = question?.id ?: 0
                                        this.questionId = 1533
                                        this.respondentAnswer = answer
                                        this.questionCategoryId = question?.questionCategoryId ?: 0
                                    }
                        }
                if (AnswerDao(context!!).doesExist(answerDto)) {
                    AnswerDao(context!!).delete(answerDto)
                }
                AnswerDao(context!!).insertIntoAnswer(answerDto)
                Snackbar.make(clRoot, "Survey Complete", Snackbar.LENGTH_SHORT)
                        .show()
                startActivity(Intent(activity!!, MainActivity::class.java)
                        .apply {
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        })
                activity?.finish()
            }
        }
        view.btnPrevious.setOnClickListener {
            onViewPagerActionListener?.onPreviousClick()
        }
    }
}