package com.mananinnovation.tmssurvey.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_test.view.*

class TestFragment : Fragment() {

    companion object {
        const val KEY_QUESTION = "QUESTION"
    }

    private var question: Question? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(KEY_QUESTION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
            String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }
    }

}
