package com.mananinnovation.tmssurvey.fragmentsTwo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.fragments.KEY_RESPONDENT_ID
import com.mananinnovation.tmssurvey.fragments.QuestionOneFragmentV2
import com.mananinnovation.tmssurvey.models.AnswerDto
import com.mananinnovation.tmssurvey.models.Question
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.fragment_set_twoquestion_four_point_one_i.view.*
import kotlinx.android.synthetic.main.fragment_set_twoquestion_four_point_one_i.view.tvQuestionHeader
import kotlinx.android.synthetic.main.fragment_set_twoquestion_four_point_one_i.view.tvQuestionText
import kotlinx.android.synthetic.main.fragment_set_twoquestion_four_point_one_i.view.tvSubHeader

class QuestionFourPointOneIFragment : Fragment() {

    companion object {
        const val KEY_QUESTION = "KEY_QUESTION"
    }

    private var question: Question? = null

    private var onViewPagerActionListener: OnViewPagerActionListener? = null
    private val answerDtoList = ArrayList<AnswerDto>()
    private var respondentId = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnViewPagerActionListener) {
            onViewPagerActionListener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        question = arguments?.getParcelable(QuestionOneFragmentV2.KEY_QUESTION)
        respondentId = arguments?.getInt(KEY_RESPONDENT_ID) ?: 0
        answerDtoList.addAll(AnswerDao(context!!).getAllAnswerInfoByRespondent(respondentId))
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_set_twoquestion_four_point_one_i, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.tvQuestionHeader.text = question?.questionCategory
        view.tvQuestionText.text =
                String.format("%s. %s", question?.questionNumber, question?.title)
        if (question?.subHeader != null && question?.subHeader != "null") {
            view.tvSubHeader.isVisible = true
            view.tvSubHeader.text = question?.subHeader
        } else {
            view.tvSubHeader.isGone = true
        }

        view.btnNext.setOnClickListener {
            onViewPagerActionListener?.onNextClicked(answerDtoList)
        }
        view.btnPrevious.setOnClickListener {
            onViewPagerActionListener?.onPreviousClick()
        }


        view.cbM5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1826) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1826
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1826) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1827) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1827
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1827) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1828) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1828
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1828) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1829) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1829
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1829) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1830) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1830
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1830) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1831) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1831
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1831) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbM11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1832) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 559
                            this.answerOptionId = 1832
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 559 && answer.answerOptionId == 1832) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbA12.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1833) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1833
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1833) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA1.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1834) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1834
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1834) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA2.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1835) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1835
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1835) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA3.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1836) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1836
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1836) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA4.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1837) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1837
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1837) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbA5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1838) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 560
                            this.answerOptionId = 1838
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 560 && answer.answerOptionId == 1838) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }

        view.cbE6.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1839) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1839
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1839) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE7.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1840) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1840
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1840) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE8.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1841) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1841
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1841) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE9.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1842) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1842
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1842) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE10.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1843) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1843
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1843) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
        view.cbE11.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1844) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
                answerDtoList.add(AnswerDto()
                        .apply {
                            this.respondentId = this@QuestionFourPointOneIFragment.respondentId
                            this.questionnaireId = question?.questionnaireId ?: 0
                            this.principalQuestionId = question?.id ?: 0
                            this.questionId = 561
                            this.answerOptionId = 1844
                            this.questionCategoryId = question?.questionCategoryId ?: 0
                        })
                this@QuestionFourPointOneIFragment.javaClass.log("answer dto list size: ${answerDtoList.size}")
            } else {
                var doesExist = false
                var selectedIndex = 0
                for (llk in 0 until answerDtoList.size) {
                    val answer = answerDtoList[llk]
                    selectedIndex = llk
                    if (respondentId == answer.respondentId && answer.questionId == 561 && answer.answerOptionId == 1844) {
                        doesExist = true
                        break
                    }
                }
                if (doesExist) {
                    val answerDto = answerDtoList.removeAt(selectedIndex)
                    AnswerDao(context!!).delete(answerDto)
                }
            }
        }
    }

}
