package com.mananinnovation.tmssurvey.utils

import android.util.Log
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mananinnovation.tmssurvey.BuildConfig
var View.isVisible: Boolean
get(){
    return this.visibility == View.VISIBLE
}
set(value){
    if(value){
        this.visibility = View.VISIBLE
    }
}
var View.isGone: Boolean
    get(){
        return this.visibility == View.GONE
    }
    set(value){
        if(value){
            this.visibility = View.GONE
        }
    }
var View.isInvisible: Boolean
    get(){
        return this.visibility == View.INVISIBLE
    }
    set(value){
        if(value){
            this.visibility = View.INVISIBLE
        }
    }
fun <T> Class<T>.log(message: String) {
    if (BuildConfig.DEBUG) {
        Log.e("TMS_SURVEY", "${this.simpleName}::$message")
    }
}

var BottomSheetBehavior<View>.isExpanded: Boolean
    get() {
        return this.state == BottomSheetBehavior.STATE_EXPANDED
    }
    set(value) {
        if (value) {
            this.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

var BottomSheetBehavior<View>.isCollapsed: Boolean
    get() {
        return this.state == BottomSheetBehavior.STATE_COLLAPSED
    }
    set(value) {
        if (value) {
            this.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }