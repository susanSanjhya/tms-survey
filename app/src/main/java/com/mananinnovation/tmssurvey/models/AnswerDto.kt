package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class AnswerDto() : Parcelable {
    var answerTblId: Int = 0
    var respondentId: Int = 0
    var questionnaireId: Int = 0
    var principalQuestionId: Int = 0
    var questionId: Int = 0
    var answerOptionId: Int = 0
    var questionCategoryId: Int = 0
    var respondentAnswer: String? = null
    var status: String? = null

    override fun toString(): String {
        return "id: $answerTblId, respondent id: $respondentId, questionnaire id: $questionnaireId, principal quesiton: $principalQuestionId, quesiton id: $questionId, answer option id: $answerOptionId, " +
                "question category id: $questionCategoryId, respondent answer: $respondentAnswer, status: $status"
    }

    constructor(parcel: Parcel) : this() {
        answerTblId = parcel.readInt()
        respondentId = parcel.readInt()
        questionnaireId = parcel.readInt()
        principalQuestionId = parcel.readInt()
        questionId = parcel.readInt()
        answerOptionId = parcel.readInt()
        questionCategoryId = parcel.readInt()
        respondentAnswer = parcel.readString()
        status = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(answerTblId)
        parcel.writeInt(respondentId)
        parcel.writeInt(questionnaireId)
        parcel.writeInt(principalQuestionId)
        parcel.writeInt(questionId)
        parcel.writeInt(answerOptionId)
        parcel.writeInt(questionCategoryId)
        parcel.writeString(respondentAnswer)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswerDto> {
        override fun createFromParcel(parcel: Parcel): AnswerDto {
            return AnswerDto(parcel)
        }

        override fun newArray(size: Int): Array<AnswerDto?> {
            return arrayOfNulls(size)
        }
    }
}