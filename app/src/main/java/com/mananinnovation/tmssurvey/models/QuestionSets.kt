package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class QuestionSets() : Parcelable {
    var id: Int = 0
    var title: String? = null
    var position: Int = 0
    val questionList = ArrayList<Question>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        position = parcel.readInt()
        parcel.readTypedList(questionList, Question.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeInt(position)
        parcel.writeTypedList(questionList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionSets> {
        override fun createFromParcel(parcel: Parcel): QuestionSets {
            return QuestionSets(parcel)
        }

        override fun newArray(size: Int): Array<QuestionSets?> {
            return arrayOfNulls(size)
        }
    }
}