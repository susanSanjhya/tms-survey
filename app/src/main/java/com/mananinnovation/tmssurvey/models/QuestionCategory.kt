package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class QuestionCategory() : Parcelable {
    var id: Int = 0
    var title: String? = null
    var position: Int = 0
    var principalQuestionList = ArrayList<PrincipalQuestions>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        position = parcel.readInt()
        parcel.readTypedList(principalQuestionList, PrincipalQuestions.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeInt(position)
        parcel.writeTypedList(principalQuestionList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionCategory> {
        override fun createFromParcel(parcel: Parcel): QuestionCategory {
            return QuestionCategory(parcel)
        }

        override fun newArray(size: Int): Array<QuestionCategory?> {
            return arrayOfNulls(size)
        }
    }
}