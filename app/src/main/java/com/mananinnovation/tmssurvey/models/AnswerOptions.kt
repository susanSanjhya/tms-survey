package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class AnswerOptions() : Parcelable {
    var questionId: Int = 0
    var answerOptionId: Int = 0
    var option: String? = null
    var customText: String? = null
    var customTextHeader: String? = null

    constructor(parcel: Parcel) : this() {
        questionId = parcel.readInt()
        answerOptionId = parcel.readInt()
        option = parcel.readString()
        customText = parcel.readString()
        customTextHeader = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(questionId)
        parcel.writeInt(answerOptionId)
        parcel.writeString(option)
        parcel.writeString(customText)
        parcel.writeString(customTextHeader)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswerOptions> {
        override fun createFromParcel(parcel: Parcel): AnswerOptions {
            return AnswerOptions(parcel)
        }

        override fun newArray(size: Int): Array<AnswerOptions?> {
            return arrayOfNulls(size)
        }
    }
}
