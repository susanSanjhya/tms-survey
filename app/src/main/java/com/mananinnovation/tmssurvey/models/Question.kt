package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class Question() : Parcelable {
    var id: Int = 0
    var questionnaireId: Int = 0
    var questionCategoryId: Int = 0
    var title: String? = null
    var subHeader: String? = null
    var questionNumber: String? = null
    var answerType: String? = null
    var position: Int = 0
    var specialQuestion: String? = null
    var questionCategory: String? = null
    var subQuestionList = ArrayList<SubQuestion>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        questionnaireId = parcel.readInt()
        questionCategoryId = parcel.readInt()
        title = parcel.readString()
        subHeader = parcel.readString()
        questionNumber = parcel.readString()
        answerType = parcel.readString()
        position = parcel.readInt()
        specialQuestion = parcel.readString()
        questionCategory = parcel.readString()
        parcel.readTypedList(subQuestionList, SubQuestion.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(questionnaireId)
        parcel.writeInt(questionCategoryId)
        parcel.writeString(title)
        parcel.writeString(subHeader)
        parcel.writeString(questionNumber)
        parcel.writeString(answerType)
        parcel.writeInt(position)
        parcel.writeString(specialQuestion)
        parcel.writeString(questionCategory)
        parcel.writeTypedList(subQuestionList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Question> {
        override fun createFromParcel(parcel: Parcel): Question {
            return Question(parcel)
        }

        override fun newArray(size: Int): Array<Question?> {
            return arrayOfNulls(size)
        }
    }
}
