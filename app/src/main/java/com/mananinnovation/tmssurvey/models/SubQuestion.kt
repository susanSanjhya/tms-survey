package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class SubQuestion() : Parcelable {
    var id: Int = 0
    var position: Int = 0
    var level: Int = 0
    var title: String? = null
    var answerable: Boolean = false
    var answerType: String? = null
    var parentId: Int = 0
    var answerOptionList = ArrayList<AnswerOptionsV2>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        position = parcel.readInt()
        level = parcel.readInt()
        title = parcel.readString()
        answerable = parcel.readByte() != 0.toByte()
        answerType = parcel.readString()
        parentId = parcel.readInt()
        parcel.readTypedList(answerOptionList, AnswerOptionsV2.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(position)
        parcel.writeInt(level)
        parcel.writeString(title)
        parcel.writeByte(if (answerable) 1 else 0)
        parcel.writeString(answerType)
        parcel.writeInt(parentId)
        parcel.writeTypedList(answerOptionList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubQuestion> {
        override fun createFromParcel(parcel: Parcel): SubQuestion {
            return SubQuestion(parcel)
        }

        override fun newArray(size: Int): Array<SubQuestion?> {
            return arrayOfNulls(size)
        }
    }
}
