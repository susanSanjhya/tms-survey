package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class RespondentDto() : Parcelable {
    var respondentTblId: Int = 0
    var householdNumber: String? = null
    var latitude: String? = null
    var longitude: String? = null
    var elevation: String? = null
    var entryDate: String? = null
    var district: String? = null
    var provience: String? = null
    var municipality: String? = null
    var wardNumber: String? = null
    var name: String? = null
    var gender: String? = null
    var ethinicCaste: String? = null
    var contactNumber: String? = null
    var age: String? = null
    var education: String? = null
    var religion: String? = null
    var occupation: String? = null
    var imageFileName: String? = null
    var status: String? = null

    constructor(parcel: Parcel) : this() {
        respondentTblId = parcel.readInt()
        householdNumber = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        elevation = parcel.readString()
        entryDate = parcel.readString()
        district = parcel.readString()
        provience = parcel.readString()
        municipality = parcel.readString()
        wardNumber = parcel.readString()
        name = parcel.readString()
        gender = parcel.readString()
        ethinicCaste = parcel.readString()
        contactNumber = parcel.readString()
        age = parcel.readString()
        education = parcel.readString()
        religion = parcel.readString()
        occupation = parcel.readString()
        imageFileName = parcel.readString()
        status = parcel.readString()
    }

    override fun toString(): String {
        return "RespondentDto(respondentTblId=$respondentTblId, " +
                "householdNumber=$householdNumber, " +
                "latitude=$latitude, " +
                "longitude=$longitude, " +
                "elevation=$elevation, " +
                "entryDate=$entryDate, " +
                "district=$district, " +
                "provience=$provience, " +
                "municipality=$municipality, " +
                "wardNumber=$wardNumber, " +
                "name=$name, " +
                "gender=$gender, " +
                "ethinicCaste=$ethinicCaste, " +
                "contactNumber=$contactNumber, " +
                "age=$age, " +
                "education=$education, " +
                "religion=$religion, " +
                "occupation=$occupation, " +
                "xphoto=$imageFileName)"
                "status=$status)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(respondentTblId)
        parcel.writeString(householdNumber)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(elevation)
        parcel.writeString(entryDate)
        parcel.writeString(district)
        parcel.writeString(provience)
        parcel.writeString(municipality)
        parcel.writeString(wardNumber)
        parcel.writeString(name)
        parcel.writeString(gender)
        parcel.writeString(ethinicCaste)
        parcel.writeString(contactNumber)
        parcel.writeString(age)
        parcel.writeString(education)
        parcel.writeString(religion)
        parcel.writeString(occupation)
        parcel.writeString(imageFileName)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RespondentDto> {
        override fun createFromParcel(parcel: Parcel): RespondentDto {
            return RespondentDto(parcel)
        }

        override fun newArray(size: Int): Array<RespondentDto?> {
            return arrayOfNulls(size)
        }
    }
}