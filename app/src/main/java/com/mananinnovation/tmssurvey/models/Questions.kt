package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class Questions() : Parcelable {
    var questionId: Int = 0
    var questionText: String? = null
    var type: String? = null
    var position: Int = 0
    var setNumber: Int = 0
    var questionHeader: String? = null
    var subHeader: String? = null
    val answerOptionList = ArrayList<AnswerOptions>()
    val children = ArrayList<Questions>()

    constructor(parcel: Parcel) : this() {
        questionId = parcel.readInt()
        questionText = parcel.readString()
        type = parcel.readString()
        position = parcel.readInt()
        setNumber = parcel.readInt()
        questionHeader = parcel.readString()
        subHeader = parcel.readString()
        parcel.readTypedList(answerOptionList, AnswerOptions.CREATOR)
        parcel.readTypedList(children, CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(questionId)
        parcel.writeString(questionText)
        parcel.writeString(type)
        parcel.writeInt(position)
        parcel.writeInt(setNumber)
        parcel.writeString(questionHeader)
        parcel.writeString(subHeader)
        parcel.writeTypedList(answerOptionList)
        parcel.writeTypedList(children)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Questions> {
        override fun createFromParcel(parcel: Parcel): Questions {
            return Questions(parcel)
        }

        override fun newArray(size: Int): Array<Questions?> {
            return arrayOfNulls(size)
        }
    }
}
