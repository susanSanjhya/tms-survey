package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class PrincipalQuestions() : Parcelable {
    var id: Int = 0
    var title: String? = null
    var subHeader: String? = null
    var questionNumber: Int = 0
    var answerType: String? = null
    var position: Int = 0
    var specialQuestion: String? = null
    var questionList = ArrayList<Question>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        subHeader = parcel.readString()
        questionNumber = parcel.readInt()
        answerType = parcel.readString()
        position = parcel.readInt()
        specialQuestion = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(subHeader)
        parcel.writeInt(questionNumber)
        parcel.writeString(answerType)
        parcel.writeInt(position)
        parcel.writeString(specialQuestion)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrincipalQuestions> {
        override fun createFromParcel(parcel: Parcel): PrincipalQuestions {
            return PrincipalQuestions(parcel)
        }

        override fun newArray(size: Int): Array<PrincipalQuestions?> {
            return arrayOfNulls(size)
        }
    }

}
