package com.mananinnovation.tmssurvey.models

import android.os.Parcel
import android.os.Parcelable

class AnswerOptionsV2() : Parcelable {
    var id: Int = 0
    var title: String? = null
    var value: String? = null
    var level: Int = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        value = parcel.readString()
        level = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(value)
        parcel.writeInt(level)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnswerOptionsV2> {
        override fun createFromParcel(parcel: Parcel): AnswerOptionsV2 {
            return AnswerOptionsV2(parcel)
        }

        override fun newArray(size: Int): Array<AnswerOptionsV2?> {
            return arrayOfNulls(size)
        }
    }

}
