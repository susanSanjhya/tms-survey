package com.mananinnovation.tmssurvey.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mananinnovation.tmssurvey.models.RespondentDto

class RespondentDao(context: Context) {

    private val dbOpenHelper: DbOpenHelper = DbOpenHelper(context)
    private lateinit var database: SQLiteDatabase

    private val allRespondentParams = arrayOf(
            DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID,
            DbOpenHelper.COLUMN_HOUSEHOLD_NUMBER,
            DbOpenHelper.COLUMN_LATITUDE,
            DbOpenHelper.COLUMN_LONGITUDE,
            DbOpenHelper.COLUMN_ELEVATION,
            DbOpenHelper.COLUMN_ENTRY_DATE,
            DbOpenHelper.COLUMN_DISTRICT,
            DbOpenHelper.COLUMN_PROVINCE,
            DbOpenHelper.COLUMN_MUNICIPALITY,
            DbOpenHelper.COLUMN_WARD_NUMBER,
            DbOpenHelper.COLUMN_NAME,
            DbOpenHelper.COLUMN_GENDER,
            DbOpenHelper.COLUMN_ETHINIC_CASTE,
            DbOpenHelper.COLUMN_CONTACT_NUMBER,
            DbOpenHelper.COLUMN_AGE,
            DbOpenHelper.COLUMN_EDUCATION,
            DbOpenHelper.COLUMN_RELIGION,
            DbOpenHelper.COLUMN_OCCUPATION,
            DbOpenHelper.COLUMN_PHOTO,
            DbOpenHelper.COLUMN_STATUS
    )

    fun insertIntoRespondent(respondentDto: RespondentDto): Long {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_HOUSEHOLD_NUMBER, respondentDto.householdNumber)
        contentValues.put(DbOpenHelper.COLUMN_LATITUDE, respondentDto.latitude)
        contentValues.put(DbOpenHelper.COLUMN_LONGITUDE, respondentDto.longitude)
        contentValues.put(DbOpenHelper.COLUMN_ELEVATION, respondentDto.elevation)
        contentValues.put(DbOpenHelper.COLUMN_ENTRY_DATE, respondentDto.entryDate)
        contentValues.put(DbOpenHelper.COLUMN_DISTRICT, respondentDto.district)
        contentValues.put(DbOpenHelper.COLUMN_PROVINCE, respondentDto.provience)
        contentValues.put(DbOpenHelper.COLUMN_MUNICIPALITY, respondentDto.municipality)
        contentValues.put(DbOpenHelper.COLUMN_WARD_NUMBER, respondentDto.wardNumber)
        contentValues.put(DbOpenHelper.COLUMN_NAME, respondentDto.name)
        contentValues.put(DbOpenHelper.COLUMN_GENDER, respondentDto.gender)
        contentValues.put(DbOpenHelper.COLUMN_ETHINIC_CASTE, respondentDto.ethinicCaste)
        contentValues.put(DbOpenHelper.COLUMN_CONTACT_NUMBER, respondentDto.contactNumber)
        contentValues.put(DbOpenHelper.COLUMN_AGE, respondentDto.age)
        contentValues.put(DbOpenHelper.COLUMN_EDUCATION, respondentDto.education)
        contentValues.put(DbOpenHelper.COLUMN_RELIGION, respondentDto.religion)
        contentValues.put(DbOpenHelper.COLUMN_OCCUPATION, respondentDto.occupation)
        contentValues.put(DbOpenHelper.COLUMN_PHOTO, respondentDto.imageFileName)
        contentValues.put(DbOpenHelper.COLUMN_STATUS, "not synced")

        val checkInsertionId: Long
        try {
            checkInsertionId = database.insert(DbOpenHelper.RESPONDENT_TABLE, null, contentValues)
        } finally {
            database.close()
            dbOpenHelper.close()
        }
        return checkInsertionId
    }

    fun getAllRespondentInfo(): ArrayList<RespondentDto> {
        database = dbOpenHelper.readableDatabase
        val respondentList = ArrayList<RespondentDto>()
        val cursor = database.query(DbOpenHelper.RESPONDENT_TABLE, allRespondentParams,
                null, null, null, null, "${DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID} DESC")
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val respondentTblId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID))
                    val householdNumber = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_HOUSEHOLD_NUMBER))
                    val latitude = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_LATITUDE))
                    val longitude = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_LONGITUDE))
                    val elevation = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_ELEVATION))
                    val entryDate = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_ENTRY_DATE))
                    val district = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_DISTRICT))
                    val province = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_PROVINCE))
                    val municipality = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_MUNICIPALITY))
                    val wardNumber = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_WARD_NUMBER))
                    val name = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_NAME))
                    val gender = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_GENDER))
                    val ethinicCaste = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_ETHINIC_CASTE))
                    val contactNumber = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_CONTACT_NUMBER))
                    val age = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_AGE))
                    val education = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_EDUCATION))
                    val religion = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_RELIGION))
                    val occupation = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_OCCUPATION))
                    val photo = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_PHOTO))
                    val status = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_STATUS))

                    val respondentDto = RespondentDto().apply {
                        this.respondentTblId = respondentTblId
                        this.householdNumber = householdNumber
                        this.latitude = latitude
                        this.longitude = longitude
                        this.elevation = elevation
                        this.entryDate = entryDate
                        this.district = district
                        this.provience = province
                        this.municipality = municipality
                        this.wardNumber = wardNumber
                        this.name = name
                        this.gender = gender
                        this.ethinicCaste = ethinicCaste
                        this.contactNumber = contactNumber
                        this.age = age
                        this.education = education
                        this.religion = religion
                        this.occupation = occupation
                        this.imageFileName = photo
                        this.status = status
                    }

                    respondentList.add(respondentDto)
                    cursor.moveToNext()
                }
            }
            cursor.close()
        }
        database.close()
        dbOpenHelper.close()
        return respondentList
    }

    fun updateHouseHoldInfo(respondentDto: RespondentDto, respondentId: Int) {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_HOUSEHOLD_NUMBER, respondentDto.householdNumber)
        contentValues.put(DbOpenHelper.COLUMN_LATITUDE, respondentDto.latitude)
        contentValues.put(DbOpenHelper.COLUMN_LONGITUDE, respondentDto.longitude)
        contentValues.put(DbOpenHelper.COLUMN_ELEVATION, respondentDto.elevation)
        contentValues.put(DbOpenHelper.COLUMN_ENTRY_DATE, respondentDto.entryDate)

        try {
            database.update(DbOpenHelper.RESPONDENT_TABLE, contentValues, "${DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID}=?", arrayOf(respondentId.toString()))
        } finally {
            database.close()
            dbOpenHelper.close()
        }
    }

    fun updateStatus(respondentDto: RespondentDto) {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_HOUSEHOLD_NUMBER, respondentDto.householdNumber)
        contentValues.put(DbOpenHelper.COLUMN_LATITUDE, respondentDto.latitude)
        contentValues.put(DbOpenHelper.COLUMN_LONGITUDE, respondentDto.longitude)
        contentValues.put(DbOpenHelper.COLUMN_ELEVATION, respondentDto.elevation)
        contentValues.put(DbOpenHelper.COLUMN_ENTRY_DATE, respondentDto.entryDate)
        contentValues.put(DbOpenHelper.COLUMN_DISTRICT, respondentDto.district)
        contentValues.put(DbOpenHelper.COLUMN_PROVINCE, respondentDto.provience)
        contentValues.put(DbOpenHelper.COLUMN_MUNICIPALITY, respondentDto.municipality)
        contentValues.put(DbOpenHelper.COLUMN_WARD_NUMBER, respondentDto.wardNumber)
        contentValues.put(DbOpenHelper.COLUMN_NAME, respondentDto.name)
        contentValues.put(DbOpenHelper.COLUMN_GENDER, respondentDto.gender)
        contentValues.put(DbOpenHelper.COLUMN_ETHINIC_CASTE, respondentDto.ethinicCaste)
        contentValues.put(DbOpenHelper.COLUMN_CONTACT_NUMBER, respondentDto.contactNumber)
        contentValues.put(DbOpenHelper.COLUMN_AGE, respondentDto.age)
        contentValues.put(DbOpenHelper.COLUMN_EDUCATION, respondentDto.education)
        contentValues.put(DbOpenHelper.COLUMN_RELIGION, respondentDto.religion)
        contentValues.put(DbOpenHelper.COLUMN_OCCUPATION, respondentDto.occupation)
        contentValues.put(DbOpenHelper.COLUMN_PHOTO, respondentDto.imageFileName)
        contentValues.put(DbOpenHelper.COLUMN_STATUS, respondentDto.status)
        try {
            database.update(DbOpenHelper.RESPONDENT_TABLE, contentValues, "${DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID}=?", arrayOf(respondentDto.respondentTblId.toString()))
        } finally {
            database.close()
            dbOpenHelper.close()
        }
    }

    fun updateRespondentInfo(respondentDto: RespondentDto, respondentId: Int) {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_DISTRICT, respondentDto.district)
        contentValues.put(DbOpenHelper.COLUMN_PROVINCE, respondentDto.provience)
        contentValues.put(DbOpenHelper.COLUMN_MUNICIPALITY, respondentDto.municipality)
        contentValues.put(DbOpenHelper.COLUMN_WARD_NUMBER, respondentDto.wardNumber)
        contentValues.put(DbOpenHelper.COLUMN_NAME, respondentDto.name)
        contentValues.put(DbOpenHelper.COLUMN_GENDER, respondentDto.gender)
        contentValues.put(DbOpenHelper.COLUMN_ETHINIC_CASTE, respondentDto.ethinicCaste)
        contentValues.put(DbOpenHelper.COLUMN_CONTACT_NUMBER, respondentDto.contactNumber)
        contentValues.put(DbOpenHelper.COLUMN_AGE, respondentDto.age)
        contentValues.put(DbOpenHelper.COLUMN_EDUCATION, respondentDto.education)
        contentValues.put(DbOpenHelper.COLUMN_RELIGION, respondentDto.religion)
        contentValues.put(DbOpenHelper.COLUMN_OCCUPATION, respondentDto.occupation)
        contentValues.put(DbOpenHelper.COLUMN_PHOTO, respondentDto.imageFileName)

        try {
            database.update(DbOpenHelper.RESPONDENT_TABLE, contentValues,
                    DbOpenHelper.COLUMN_RESPONDENT_TABLE_ID + "=?", arrayOf(respondentId.toString()))
        } finally {
            database.close()
            dbOpenHelper.close()
        }
    }
}