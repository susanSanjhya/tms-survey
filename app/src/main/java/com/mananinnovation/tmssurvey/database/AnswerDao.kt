package com.mananinnovation.tmssurvey.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mananinnovation.tmssurvey.models.AnswerDto

class AnswerDao(context: Context) {

    private val dbOpenHelper: DbOpenHelper = DbOpenHelper(context)
    private lateinit var database: SQLiteDatabase


    private val allAnswerRespondentParam = arrayOf(
            DbOpenHelper.COLUMN_ANSWER_TABLE_ID,
            DbOpenHelper.COLUMN_RESPONDENT_ID,
            DbOpenHelper.COLUMN_QUESTIONNAIRE_ID,
            DbOpenHelper.COLUMN_PRINCIPAL_QUESION_ID,
            DbOpenHelper.COLUMN_QUESTION_ID,
            DbOpenHelper.COLUMN_ANSWER_OPTION_ID,
            DbOpenHelper.COLUMN_QUESTION_CATEGORY_ID,
            DbOpenHelper.COLUMN_RESPONDENT_ANSWER,
            DbOpenHelper.COLUMN_STATUS
    )

    fun insertIntoAnswer(answerDto: AnswerDto): Long {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_RESPONDENT_ID, answerDto.respondentId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTIONNAIRE_ID, answerDto.questionnaireId)
        contentValues.put(DbOpenHelper.COLUMN_PRINCIPAL_QUESION_ID, answerDto.principalQuestionId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTION_ID, answerDto.questionId)
        contentValues.put(DbOpenHelper.COLUMN_ANSWER_OPTION_ID, answerDto.answerOptionId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTION_CATEGORY_ID, answerDto.questionCategoryId)
        contentValues.put(DbOpenHelper.COLUMN_RESPONDENT_ANSWER, answerDto.respondentAnswer)
        contentValues.put(DbOpenHelper.COLUMN_STATUS, answerDto.status)

        val insertId: Long
        try {
            insertId = database.insert(DbOpenHelper.ANSWER_TABLE, null, contentValues)
        } finally {
            database.close()
            dbOpenHelper.close()
        }
        return insertId
    }

    fun doesExist(answerDto: AnswerDto): Boolean {
        database = dbOpenHelper.readableDatabase
        var doesExist = false
        val cursor = database.query(DbOpenHelper.ANSWER_TABLE, allAnswerRespondentParam, "${DbOpenHelper.COLUMN_RESPONDENT_ID}=? AND ${DbOpenHelper.COLUMN_QUESTION_ID}=?",
                arrayOf(answerDto.respondentId.toString(), answerDto.questionId.toString()), null, null, null)
        if (cursor != null) {
            doesExist = cursor.count > 0
            cursor.close()
        }

        database.close()
        dbOpenHelper.close()
        return doesExist
    }

    fun getAllAnswerInfoByRespondent(respondentId: Int): ArrayList<AnswerDto> {
        database = dbOpenHelper.readableDatabase
        val answerList = ArrayList<AnswerDto>()
        val cursor = database.query(DbOpenHelper.ANSWER_TABLE, allAnswerRespondentParam,
                "${DbOpenHelper.COLUMN_RESPONDENT_ID}=?", arrayOf(respondentId.toString()), null, null, null, null)
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val answerTblId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ANSWER_TABLE_ID))
                    val commonId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_RESPONDENT_ID))
                    val questionnaireId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTIONNAIRE_ID))
                    val principalQuestionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_PRINCIPAL_QUESION_ID))
                    val questionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTION_ID))
                    val answerOptionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ANSWER_OPTION_ID))
                    val questionCategoryId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTION_CATEGORY_ID))
                    val respondentAnswer = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_RESPONDENT_ANSWER))
                    val status = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_STATUS))

                    val answerDto = AnswerDto().apply {
                        this.answerTblId = answerTblId
                        this.respondentId = commonId
                        this.questionnaireId = questionnaireId
                        this.principalQuestionId = principalQuestionId
                        this.questionId = questionId
                        this.answerOptionId = answerOptionId
                        this.questionCategoryId = questionCategoryId
                        this.respondentAnswer = respondentAnswer
                        this.status = status
                    }

                    answerList.add(answerDto)
                    cursor.moveToNext()
                }
            }
            cursor.close()
        }
        database.close()
        dbOpenHelper.close()
        return answerList
    }

    fun update(answerDto: AnswerDto) {
        database = dbOpenHelper.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(DbOpenHelper.COLUMN_RESPONDENT_ID, answerDto.respondentId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTIONNAIRE_ID, answerDto.questionnaireId)
        contentValues.put(DbOpenHelper.COLUMN_PRINCIPAL_QUESION_ID, answerDto.principalQuestionId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTION_ID, answerDto.questionId)
        contentValues.put(DbOpenHelper.COLUMN_ANSWER_OPTION_ID, answerDto.answerOptionId)
        contentValues.put(DbOpenHelper.COLUMN_QUESTION_CATEGORY_ID, answerDto.questionCategoryId)
        contentValues.put(DbOpenHelper.COLUMN_RESPONDENT_ANSWER, answerDto.respondentAnswer)
        contentValues.put(DbOpenHelper.COLUMN_STATUS, answerDto.status)

        try {
            database.update(DbOpenHelper.ANSWER_TABLE, contentValues, "${DbOpenHelper.COLUMN_ANSWER_TABLE_ID}=?", arrayOf(answerDto.answerTblId.toString()))
        } finally {
            database.close()
            dbOpenHelper.close()
        }
    }

    fun delete(answerDto: AnswerDto) {
        database = dbOpenHelper.writableDatabase
        try {
            database.delete(DbOpenHelper.ANSWER_TABLE, "${DbOpenHelper.COLUMN_ANSWER_TABLE_ID}=?", arrayOf(answerDto.answerTblId.toString()))
        } finally {
            database.close()
            dbOpenHelper.close()
        }
    }

    fun getAllAnswerInfo(): ArrayList<AnswerDto>{
        database = dbOpenHelper.readableDatabase
        val answerList= ArrayList<AnswerDto>()
        val cursor = database.query(DbOpenHelper.ANSWER_TABLE, allAnswerRespondentParam, null, null, null, null,null)

        if(cursor != null){
            if(cursor.count > 0){
                cursor.moveToFirst()
                while(!cursor.isAfterLast){
                    val answerTblId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ANSWER_TABLE_ID))
                    val commonId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_RESPONDENT_ID))
                    val questionnaireId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTIONNAIRE_ID))
                    val principalQuestionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_PRINCIPAL_QUESION_ID))
                    val questionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTION_ID))
                    val answerOptionId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ANSWER_OPTION_ID))
                    val questionCategoryId = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_QUESTION_CATEGORY_ID))
                    val respondentAnswer = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_RESPONDENT_ANSWER))
                    val status = cursor.getString(cursor.getColumnIndex(DbOpenHelper.COLUMN_STATUS))

                    val answerDto = AnswerDto().apply {
                        this.answerTblId = answerTblId
                        this.respondentId = commonId
                        this.questionnaireId = questionnaireId
                        this.principalQuestionId = principalQuestionId
                        this.questionId = questionId
                        this.answerOptionId = answerOptionId
                        this.questionCategoryId = questionCategoryId
                        this.respondentAnswer = respondentAnswer
                        this.status = status
                    }

                    answerList.add(answerDto)
                    cursor.moveToNext()
                }
            }
            cursor.close()
        }
        database.close()
        dbOpenHelper.close()
        return answerList
    }


}