package com.mananinnovation.tmssurvey.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbOpenHelper(context: Context) : SQLiteOpenHelper(context, "TMP_SURVEY_DB", null, 1) {

    companion object {
        const val RESPONDENT_TABLE = "respondent_tbl"
        const val COLUMN_RESPONDENT_TABLE_ID = "id"
        const val COLUMN_HOUSEHOLD_NUMBER = "household_number"
        const val COLUMN_LATITUDE = "latitude"
        const val COLUMN_LONGITUDE = "longitude"
        const val COLUMN_ELEVATION = "elevation"
        const val COLUMN_ENTRY_DATE = "entry_date"
        const val COLUMN_DISTRICT = "district"
        const val COLUMN_PROVINCE = "province"
        const val COLUMN_MUNICIPALITY = "municipality"
        const val COLUMN_WARD_NUMBER = "ward_number"
        const val COLUMN_NAME = "name"
        const val COLUMN_GENDER = "gender"
        const val COLUMN_ETHINIC_CASTE = "ethinic_caste"
        const val COLUMN_CONTACT_NUMBER = "contact_number"
        const val COLUMN_AGE = "age"
        const val COLUMN_EDUCATION = "education"
        const val COLUMN_RELIGION = "religion"
        const val COLUMN_OCCUPATION = "occupation"
        const val COLUMN_PHOTO = "photo"

        const val ANSWER_TABLE = "answer_tbl"
        const val COLUMN_ANSWER_TABLE_ID = "id"
        const val COLUMN_RESPONDENT_ID = "common_id"
        const val COLUMN_QUESTIONNAIRE_ID = "questionnaire_id"
        const val COLUMN_ANSWER_OPTION_ID = "COLUMN_ANSWER_OPTION_ID"
        const val COLUMN_PRINCIPAL_QUESION_ID = "principal_question_id"
        const val COLUMN_QUESTION_ID = "question_id"
        const val COLUMN_QUESTION_CATEGORY_ID = "question_category_id"
        const val COLUMN_RESPONDENT_ANSWER = "respondent_answer"
        const val COLUMN_STATUS = "status"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
                "Create Table  $RESPONDENT_TABLE (" +
                        "$COLUMN_RESPONDENT_TABLE_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "$COLUMN_HOUSEHOLD_NUMBER TEXT," +
                        "$COLUMN_LATITUDE TEXT," +
                        "$COLUMN_LONGITUDE TEXT," +
                        "$COLUMN_ELEVATION TEXT," +
                        "$COLUMN_ENTRY_DATE TEXT," +
                        "$COLUMN_DISTRICT TEXT," +
                        "$COLUMN_PROVINCE TEXT," +
                        "$COLUMN_MUNICIPALITY TEXT," +
                        "$COLUMN_WARD_NUMBER TEXT," +
                        "$COLUMN_NAME TEXT," +
                        "$COLUMN_GENDER TEXT," +
                        "$COLUMN_ETHINIC_CASTE TEXT," +
                        "$COLUMN_CONTACT_NUMBER TEXT," +
                        "$COLUMN_AGE TEXT," +
                        "$COLUMN_EDUCATION TEXT," +
                        "$COLUMN_RELIGION TEXT," +
                        "$COLUMN_OCCUPATION TEXT," +
                        "$COLUMN_PHOTO TEXT," +
                        "$COLUMN_STATUS TEXT)")


        db.execSQL(
                "Create Table  $ANSWER_TABLE (" +
                        "$COLUMN_ANSWER_TABLE_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "$COLUMN_RESPONDENT_ID INTEGER," +
                        "$COLUMN_QUESTIONNAIRE_ID INTEGER," +
                        "$COLUMN_PRINCIPAL_QUESION_ID INTEGER," +
                        "$COLUMN_QUESTION_ID INTEGER," +
                        "$COLUMN_ANSWER_OPTION_ID INTEGER," +
                        "$COLUMN_QUESTION_CATEGORY_ID INTEGER," +
                        "$COLUMN_RESPONDENT_ANSWER TEXT," +
                        "$COLUMN_STATUS Text)")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("Drop TABLE IF EXISTS $RESPONDENT_TABLE")
        db.execSQL("Drop TABLE IF EXISTS $ANSWER_TABLE")
    }
}