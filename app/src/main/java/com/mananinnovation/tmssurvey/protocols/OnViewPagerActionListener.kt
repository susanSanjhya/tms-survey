package com.mananinnovation.tmssurvey.protocols

import com.mananinnovation.tmssurvey.models.AnswerDto

interface OnViewPagerActionListener {
    fun onPreviousClick()
    fun onNextClicked(answerDtoList: ArrayList<AnswerDto>)
}