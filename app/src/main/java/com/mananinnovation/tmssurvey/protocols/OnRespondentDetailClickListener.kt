package com.mananinnovation.tmssurvey.protocols

interface OnRespondentDetailClickListener {
    fun onSycnClicked(position: Int)
    fun onItemClicked(position: Int)
}