package com.mananinnovation.tmssurvey.protocols

interface OnResponseCallback {

    fun onResponseStarted()
    fun onResponseComplete(response: String)

}
