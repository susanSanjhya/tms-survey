package com.mananinnovation.tmssurvey.protocols

interface OnAnswerPostListener {
    fun onStarted()
    fun onComplete(message: String)
    fun onError(message: String)
}