package com.mananinnovation.tmssurvey.protocols

interface OnItemClickListener {
    fun onItemClicked(position: Int)
}
