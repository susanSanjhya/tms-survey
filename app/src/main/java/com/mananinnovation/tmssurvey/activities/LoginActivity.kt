package com.mananinnovation.tmssurvey.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.controllers.LoginController
import com.mananinnovation.tmssurvey.controllers.UserDetailController
import com.mananinnovation.tmssurvey.protocols.OnResponseCallback
import com.mananinnovation.tmssurvey.storage.PrefManager
import com.mananinnovation.tmssurvey.utils.log
import com.mananinnovation.tmssurvey.widgets.CustomProgressDialog
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var customProgressDialog: CustomProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupViews()
    }

    private fun setupViews() {
        btnLogin.setOnClickListener(this@LoginActivity)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnLogin -> {
                if (allFieldValid()) {
                    val username = etUsername.text.toString()
                    val password = etPassword.text.toString()
                    val jsonParam = JSONObject()
                    jsonParam.put("grant_type", "password")
                    jsonParam.put("email", username)
                    jsonParam.put("password", password)
                    LoginController(onLoginCallback).execute(jsonParam.toString())
                }
            }
        }
    }

    private fun showProgressDialog() {
        customProgressDialog =
            supportFragmentManager.findFragmentByTag(CustomProgressDialog.TAG) as CustomProgressDialog?
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog()
        }
        customProgressDialog?.arguments = Bundle()
            .apply {
                putString(CustomProgressDialog.KEY_MESSAGE, "Logging in...")
            }
        customProgressDialog?.show(supportFragmentManager, CustomProgressDialog.TAG)
    }

    private fun allFieldValid(): Boolean {
        if (etUsername.text.toString().isEmpty()) {
            tilUsername.isErrorEnabled = true
            tilUsername.error = "Username must not be empty."
            etUsername.requestFocus()
            return false
        } else {
            tilUsername.isErrorEnabled = false
            tilUsername.error = null
            etUsername.clearFocus()
        }

        if (etPassword.text.toString().isEmpty()) {
            tilPassword.isErrorEnabled = true
            tilPassword.error = "Password must not be empty."
            etPassword.requestFocus()
            return false
        } else {
            tilPassword.isErrorEnabled = false
            tilPassword.error = null
            etPassword.clearFocus()
        }

        return true
    }

    private val onLoginCallback = object : OnResponseCallback {
        override fun onResponseStarted() {
            showProgressDialog()
        }

        override fun onResponseComplete(response: String) {
            this@LoginActivity.javaClass.log("response: $response")
            try {
                val jsonObjectResponse = JSONObject(response)
                val accessToken = jsonObjectResponse.getString("access_token")
                PrefManager(this@LoginActivity).accessToken = accessToken
                UserDetailController(onUserDetailCallback).execute(PrefManager(this@LoginActivity).accessToken)
            } catch (e: Exception) {
                customProgressDialog?.dismiss()
                this@LoginActivity.javaClass.log("login error: ${e.localizedMessage}")
                Snackbar.make(clRoot, "User login failed.", Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private val onUserDetailCallback = object : OnResponseCallback {
        override fun onResponseStarted() {

        }

        override fun onResponseComplete(response: String) {
            customProgressDialog?.dismiss()
            this@LoginActivity.javaClass.log("Response: $response")
            try {
                val jsonObjectResponse = JSONObject(response)
                PrefManager(this@LoginActivity).fullName = jsonObjectResponse.getString("fullname")
                PrefManager(this@LoginActivity).userId = jsonObjectResponse.getInt("id")
                PrefManager(this@LoginActivity).role = jsonObjectResponse.getString("role")
                PrefManager(this@LoginActivity).enumerators =
                    if (jsonObjectResponse.has("enumerators")) {
                        jsonObjectResponse.getJSONArray("enumerators").toString()
                    } else {
                        null
                    }
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                finish()
            } catch (e: Exception) {
                this@LoginActivity.javaClass.log("error: ${e.localizedMessage}")
                Snackbar.make(clRoot, "Something went wrong", Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

}
