package com.mananinnovation.tmssurvey.activities

//import com.mananinnovation.tmssurvey.utils.log
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.adapters.SetOneQuestionPagerAdapterV2
import com.mananinnovation.tmssurvey.adapters.SetTwoQuestionPagerAdapterV2
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.models.*
import com.mananinnovation.tmssurvey.protocols.OnViewPagerActionListener
import com.mananinnovation.tmssurvey.utils.log
import kotlinx.android.synthetic.main.activity_questionnaire.*
import org.json.JSONArray
import java.nio.charset.Charset


class QuestionnaireActivityV2 : AppCompatActivity(), View.OnClickListener, OnViewPagerActionListener {
    companion object {
        const val KEY_RESPONDENT_ID = "KEY_RESPONDENT_ID"
    }

    private val questionSetList = ArrayList<QuestionSets>()
    private val questionList = ArrayList<Question>()
    private var respondentId: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questionnaire)
        respondentId = intent?.getIntExtra(KEY_RESPONDENT_ID, 0) ?: 0
        readQuestion()
        showSetOption()
        //   setupViews()
    }
//
//    private fun setupViews() {
//        btnNext.setOnClickListener(this@QuestionnaireActivityV2)
//        btnPrevious.setOnClickListener(this@QuestionnaireActivityV2)
//    }

    private fun setupActionbar(title: String) {
        toolbar.title = title
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupPager(questionList: ArrayList<Question>) {
        this@QuestionnaireActivityV2.questionList.addAll(questionList)
        vpQuestionnaire.adapter = SetOneQuestionPagerAdapterV2(respondentId, questionList, supportFragmentManager)
    }


    private fun setupPager2(questionList: ArrayList<Question>) {
        this@QuestionnaireActivityV2.questionList.addAll(questionList)
        vpQuestionnaire.adapter = SetTwoQuestionPagerAdapterV2(respondentId, questionList, supportFragmentManager)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (vpQuestionnaire.currentItem != 0) {
                    vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem - 1, true)
                } else {
                    AlertDialog.Builder(this@QuestionnaireActivityV2)
                            .setMessage("Are you sure you want to exit survey? All saved details will be deleted.")
                            .setPositiveButton("Exit") { dialog, _ ->
                                dialog.dismiss()
                                finish()
                            }
                            .setNegativeButton("No") { dialog, _ ->
                                dialog.dismiss()
                            }
                            .create().show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showSetOption() {
        val sortedQuestionSetList = questionSetList.sortedWith(compareBy { it.position })
        val titleArray = arrayOfNulls<String>(questionSetList.size)
        for (i in 0 until questionSetList.size) {
            titleArray[i] = sortedQuestionSetList[i].title
        }

        AlertDialog.Builder(this@QuestionnaireActivityV2)
                .setTitle("Choose question set")
                .setCancelable(false)
                .setItems(titleArray) { dialog, which ->
                    vpQuestionnaire.offscreenPageLimit = sortedQuestionSetList[which].questionList.size
                    when (which) {
                        0 -> {
                            dialog.dismiss()
                            val title = sortedQuestionSetList[which].title ?: ""
                            setupActionbar(title)
                            setupPager(sortedQuestionSetList[which].questionList)
                        }

                        1 -> {
                            dialog.dismiss()
                            val title = sortedQuestionSetList[which].title ?: ""
                            setupActionbar(title)
                            setupPager2(sortedQuestionSetList[which].questionList)
                        }
                    }
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
                .create().show()
    }

    private fun readQuestion() {
        val inputStream = assets.open("question2.json")
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        val json = String(buffer, Charset.forName("UTF-8"))
        val jsonArrayResponse = JSONArray(json)
        //this@QuestionnaireActivityV2.javaClass.log("question length: ${jsonArrayResponse.length()}")
        for (i in 0 until jsonArrayResponse.length()) {
            val jsonObjectResponse = jsonArrayResponse.getJSONObject(i)
            val id = jsonObjectResponse.getInt("id")
            val title = jsonObjectResponse.getString("title")
            val position = jsonObjectResponse.getInt("position")
            val questionCategoryList =
                    getQuestionList(id, jsonObjectResponse.getJSONArray("question_categories"))
            val questionSet = QuestionSets()
                    .apply {
                        this.id = id
                        this.title = title
                        this.position = position
                        this.questionList.clear()
                        this.questionList.addAll(questionCategoryList)
                    }
            questionSetList.add(questionSet)
        }
    }

    private fun getQuestionList(questionnaireId: Int, jsonArray: JSONArray): ArrayList<Question> {
        val questionList = ArrayList<Question>()

        for (i in 0 until jsonArray.length()) {
            val jsonObjectQuestionCategory = jsonArray.getJSONObject(i)
            val questionCategoryId1 = jsonObjectQuestionCategory.getInt("id")
            this@QuestionnaireActivityV2.javaClass.log("quesiton category id: $questionCategoryId1")
            val questionHeader = jsonObjectQuestionCategory.getString("title")
            val jsonArrayQuestions = jsonObjectQuestionCategory.getJSONArray("principal_questions")
            for (j in 0 until jsonArrayQuestions.length()) {
                val jsonObjectQuestions = jsonArrayQuestions.getJSONObject(j)
                val id = jsonObjectQuestions.getInt("id")
                val title = jsonObjectQuestions.getString("title")
                val subHeader = jsonObjectQuestions.getString("sub_header")
                val questionNumber = jsonObjectQuestions.getString("question_number")
                val answerType = jsonObjectQuestions.getString("answer_type")
                val position = jsonObjectQuestions.getInt("position")
                val specialQuestion = jsonObjectQuestions.getString("special_question")
                val subQuestionList =
                        getSubQuestionList(jsonObjectQuestions.getJSONArray("questions"))
                val question = Question()
                        .apply {
                            this.id = id
                            this.questionnaireId = questionnaireId
                            this.questionCategoryId = questionCategoryId1
                            this.title = title
                            this.subHeader = subHeader
                            this.questionNumber = questionNumber
                            this.answerType = answerType
                            this.position = position
                            this.specialQuestion = specialQuestion
                            this.questionCategory = questionHeader
                            this.subQuestionList.clear()
                            this.subQuestionList.addAll(subQuestionList.sortedWith(compareBy { it.position }))
                        }
                questionList.add(question)
            }
        }
        return questionList
    }

    private fun getSubQuestionList(jsonArray: JSONArray): ArrayList<SubQuestion> {
        val subQuestionList = ArrayList<SubQuestion>()
        for (i in 0 until jsonArray.length()) {
            val jsonObjectSubQuestion = jsonArray.getJSONObject(i)
            val id = jsonObjectSubQuestion.getInt("id")
            val position = jsonObjectSubQuestion.getInt("position")
            val level = jsonObjectSubQuestion.getInt("level")
            val title = jsonObjectSubQuestion.getString("title")
            val answerable = jsonObjectSubQuestion.getBoolean("answerable") ?: false
            val answerType = jsonObjectSubQuestion.getString("answer_type")
            val parentId = if (jsonObjectSubQuestion.get("parent_id").toString() != "null") {
                jsonObjectSubQuestion.getInt("parent_id")
            } else {
                0
            }

            val answerOptionList =
                    getAnswerOptionList(jsonObjectSubQuestion.getJSONArray("answer_options"))

            val subQuestion = SubQuestion()
                    .apply {
                        this.id = id
                        this.position = position
                        this.level = level
                        this.title = title
                        this.answerable = answerable
                        this.answerType = answerType
                        this.parentId = parentId
                        this.answerOptionList.clear()
                        this.answerOptionList.addAll(answerOptionList.sortedWith(compareBy { it.level }))
                    }
            subQuestionList.add(subQuestion)
        }
        return subQuestionList
    }

    private fun getAnswerOptionList(jsonArray: JSONArray): ArrayList<AnswerOptionsV2> {
        val answerOptionsList = ArrayList<AnswerOptionsV2>()

        for (i in 0 until jsonArray.length()) {
            val jsonObjectAnswerOption = jsonArray.getJSONObject(i)
            val id = jsonObjectAnswerOption.getInt("id")
            val title = jsonObjectAnswerOption.getString("title")
            val value = jsonObjectAnswerOption.getString("value")
            val level = jsonObjectAnswerOption.getInt("level")

            val answerOptionsV2 = AnswerOptionsV2()
                    .apply {
                        this.id = id
                        this.title = title
                        this.value = value
                        this.level = level
                    }
            answerOptionsList.add(answerOptionsV2)
        }
        return answerOptionsList
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnNext -> {
                if (vpQuestionnaire.currentItem != questionList.size - 1) {
                    vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem + 1, true)
                }
            }
            R.id.btnPrevious -> {
                if (vpQuestionnaire.currentItem != 0) {
                    vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem - 1, true)
                }
            }
        }
    }

    override fun onNextClicked(answerDtoList: ArrayList<AnswerDto>) {
        val answerDao = AnswerDao(this@QuestionnaireActivityV2)
        for (answer in answerDtoList) {
            if (answerDao.doesExist(answer)) {
                answerDao.update(answer)
            } else {
                answerDao.insertIntoAnswer(answer)
            }
        }
        if (vpQuestionnaire.currentItem != questionList.size - 1) {
            vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem + 1, true)
        }
    }

    override fun onPreviousClick() {
        if (vpQuestionnaire.currentItem != 0) {
            vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem - 1, true)
        }
    }

    override fun onBackPressed() {
        if (vpQuestionnaire.currentItem != 0) {
            vpQuestionnaire.setCurrentItem(vpQuestionnaire.currentItem - 1, true)
        } else {
            AlertDialog.Builder(this@QuestionnaireActivityV2)
                    .setMessage("Are you sure you want to exit survey? All saved details will be deleted.")
                    .setPositiveButton("Exit") { dialog, _ ->
                        dialog.dismiss()
                        finish()
                    }
                    .setNegativeButton("No") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create().show()
        }
    }
}