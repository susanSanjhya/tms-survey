package com.mananinnovation.tmssurvey.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.adapters.SurveyAdapter
import com.mananinnovation.tmssurvey.controllers.AnswerPostController
import com.mananinnovation.tmssurvey.database.AnswerDao
import com.mananinnovation.tmssurvey.database.RespondentDao
import com.mananinnovation.tmssurvey.models.RespondentDto
import com.mananinnovation.tmssurvey.protocols.OnAnswerPostListener
import com.mananinnovation.tmssurvey.protocols.OnRespondentDetailClickListener
import com.mananinnovation.tmssurvey.storage.PrefManager
import com.mananinnovation.tmssurvey.utils.isGone
import com.mananinnovation.tmssurvey.utils.isVisible
import com.mananinnovation.tmssurvey.utils.log
import com.mananinnovation.tmssurvey.widgets.CustomProgressDialog
import com.mananinnovation.tmssurvey.widgets.LinearItemDecorator
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var customProgressDialog: CustomProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setupViews()
    }

    private fun setupViews() {
        btnStartNewSurvey.setOnClickListener(this@MainActivity)
        rvSurveys.layoutManager = LinearLayoutManager(this@MainActivity)
        rvSurveys.addItemDecoration(LinearItemDecorator(resources.getDimensionPixelSize(R.dimen.dimen_8dp)))
        this@MainActivity.javaClass.log("answer list: ${AnswerDao(this@MainActivity).getAllAnswerInfo()}")
    }

    override fun onResume() {
        super.onResume()
        updateList()
    }

    var syncedAnswer: RespondentDto? = null

    private fun updateList() {
        if (RespondentDao(this@MainActivity).getAllRespondentInfo().isNotEmpty()) {
            tvNoSurveyMessage.isGone = true
            rvSurveys.isVisible = true
            rvSurveys.adapter = SurveyAdapter(RespondentDao(this@MainActivity).getAllRespondentInfo())
                    .apply {
                        onRespondentDetailClickListener = object : OnRespondentDetailClickListener {
                            override fun onSycnClicked(position: Int) {
                                this@MainActivity.javaClass.log("Synced")
                                val respondentInfo = (rvSurveys.adapter as SurveyAdapter).getItem(position)
                                val jsonObjectResponse = JSONObject()
                                val jsonObjectRespondent = JSONObject()
                                jsonObjectRespondent.put("household_number", respondentInfo.householdNumber)
                                jsonObjectRespondent.put("latitude", respondentInfo.latitude)
                                jsonObjectRespondent.put("longitude", respondentInfo.longitude)
                                jsonObjectRespondent.put("elevation", respondentInfo.elevation)
                                jsonObjectRespondent.put("entry_date", respondentInfo.entryDate)
                                jsonObjectRespondent.put("district", respondentInfo.district)
                                jsonObjectRespondent.put("province", respondentInfo.provience)
                                jsonObjectRespondent.put("municipality", respondentInfo.municipality)
                                jsonObjectRespondent.put("ward_number", respondentInfo.wardNumber)
                                jsonObjectRespondent.put("name", respondentInfo.name)
                                jsonObjectRespondent.put("gender", respondentInfo.gender)
                                jsonObjectRespondent.put("ethinic_caste", respondentInfo.ethinicCaste)
                                jsonObjectRespondent.put("contact_number", respondentInfo.contactNumber)
                                jsonObjectRespondent.put("age", respondentInfo.age)
                                jsonObjectRespondent.put("education", respondentInfo.education)
                                jsonObjectRespondent.put("religion", respondentInfo.religion)
                                jsonObjectRespondent.put("occupation", respondentInfo.occupation)
                                val answerDtoList = AnswerDao(this@MainActivity).getAllAnswerInfoByRespondent(respondentInfo.respondentTblId)
//                                jsonObjectResponse.put("questionnaire_id", answerDtoList[0].questionnaireId)
                                try {
                                    if (respondentInfo.imageFileName != null) {
                                        val bm = BitmapFactory.decodeFile(File(filesDir, respondentInfo.imageFileName).absolutePath)
                                        val baos = ByteArrayOutputStream()
                                        bm.compress(Bitmap.CompressFormat.PNG, 100, baos) //bm is the bitmap object
                                        val byteArrayImage = baos.toByteArray()

                                        val encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
                                        val photoJsonObject = JSONObject()
                                        photoJsonObject.put("data", encodedImage)
                                        photoJsonObject.put("filename", respondentInfo.imageFileName)
                                        photoJsonObject.put("mime_type", "png")
                                        jsonObjectRespondent.put("photo", photoJsonObject)
                                    }
                                } catch (e: Exception) {
                                    this@MainActivity.javaClass.log("error: ${e.localizedMessage}")
                                }
                                jsonObjectResponse.put("respondent", jsonObjectRespondent)
                                val jsonArrayAnswer = JSONArray()


                                for (answer in answerDtoList) {
                                    val jsonObjectAnswer = JSONObject()
                                    jsonObjectAnswer.put("questionnaire_id", answer.questionnaireId)
                                    jsonObjectAnswer.put("principal_question_id", answer.principalQuestionId)
                                    jsonObjectAnswer.put("question_id", answer.questionId)
                                    jsonObjectAnswer.put("answer_option_id", answer.answerOptionId)
                                    jsonObjectAnswer.put("question_category_id", answer.questionCategoryId)
                                    jsonObjectAnswer.put("respondent_answer", answer.respondentAnswer)
                                    jsonArrayAnswer.put(jsonObjectAnswer)
                                }

                                jsonObjectResponse.put("answers", jsonArrayAnswer)
                                this@MainActivity.javaClass.log("json response: $jsonObjectResponse")
                                syncedAnswer = respondentInfo
                                AnswerPostController(this@MainActivity, PrefManager(this@MainActivity).accessToken
                                        ?: "", respondentInfo.imageFileName, onAnswerPostController).execute(jsonObjectResponse.toString())
                            }

                            override fun onItemClicked(position: Int) {

                            }
                        }
                    }
        } else {
            tvNoSurveyMessage.isVisible = true
            rvSurveys.isGone = true
        }
    }

    private fun showDialog() {
        customProgressDialog = supportFragmentManager.findFragmentByTag(CustomProgressDialog.TAG) as CustomProgressDialog?
        if (customProgressDialog == null) {
            customProgressDialog = CustomProgressDialog()
        }
        customProgressDialog?.arguments = Bundle()
                .apply {
                    putString(CustomProgressDialog.KEY_MESSAGE, "Syncing")
                }
        customProgressDialog?.show(supportFragmentManager, CustomProgressDialog.TAG)
    }

    private var onAnswerPostController = object : OnAnswerPostListener {
        override fun onStarted() {
            showDialog()
        }

        override fun onComplete(message: String) {
            customProgressDialog?.dismiss()
            this@MainActivity.javaClass.log("sync message: $message")
            if (syncedAnswer != null) {
                syncedAnswer?.status = "synced"
                RespondentDao(this@MainActivity).updateStatus(syncedAnswer!!)
            }
            this@MainActivity.runOnUiThread { updateList() }


        }

        override fun onError(message: String) {
            customProgressDialog?.dismiss()
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnStartNewSurvey -> {
                startActivity(Intent(this@MainActivity, RespondentDetailActivity::class.java))
            }
        }
    }

    /*private fun getImageFile(context: Context, imageTag: String): File? {
        val baseDirectory = getBaseDirectory(context)
        imageFileName = "${imageTag}_${System.currentTimeMillis()}.png"
        return File(baseDirectory, imageFileName)
    }

    private fun getFileUri(context: Context, file: File?): Uri? {
        return if (file != null) {
            FileProvider.getUriForFile(context, "${context.packageName}.provider", file)
        } else {
            null
        }
    }*/

    private fun getBaseDirectory(context: Context): File? {
        return context.filesDir
    }
}
