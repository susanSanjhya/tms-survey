package com.mananinnovation.tmssurvey.activities

//import com.mananinnovation.tmssurvey.utils.log

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mananinnovation.tmssurvey.R
import com.mananinnovation.tmssurvey.adapters.RespondentDetailPagerAdapter
import com.mananinnovation.tmssurvey.database.RespondentDao
import com.mananinnovation.tmssurvey.fragments.HouseHoldDetailFragment
import com.mananinnovation.tmssurvey.fragments.RespondentDetailFragment
import com.mananinnovation.tmssurvey.models.RespondentDto
import kotlinx.android.synthetic.main.activity_respondent_detail.*

class RespondentDetailActivity : AppCompatActivity(), View.OnClickListener,
        HouseHoldDetailFragment.OnHouseDetailEntered, RespondentDetailFragment.OnRespondentDetailListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_respondent_detail)
        setupActionbar()
        setupViews()
    }

    private fun setupViews() {
        vpRespondentDetail.adapter = RespondentDetailPagerAdapter(supportFragmentManager)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnPrevious -> {
                if (vpRespondentDetail.currentItem != 0) {
                    vpRespondentDetail.setCurrentItem(vpRespondentDetail.currentItem - 1, true)
                }
            }
            R.id.btnNext -> {
                if (vpRespondentDetail.currentItem != 1) {
                    vpRespondentDetail.setCurrentItem(vpRespondentDetail.currentItem + 1, true)
                } else {
                    startActivity(
                            Intent(
                                    this@RespondentDetailActivity,
                                    QuestionnaireActivityV2::class.java
                            )
                    )
                }
            }
        }
    }

    private fun setupActionbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                AlertDialog.Builder(this@RespondentDetailActivity)
                        .setMessage("Are you sure you want to exit survey? All saved details will be deleted.")
                        .setPositiveButton("Exit") { dialog, _ ->
                            dialog.dismiss()
                            finish()
                        }
                        .setNegativeButton("No") { dialog, _ ->
                            dialog.dismiss()
                        }
                        .create().show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (vpRespondentDetail.currentItem == 0) {
            AlertDialog.Builder(this@RespondentDetailActivity)
                    .setMessage("Are you sure you want to exit survey? All saved details will be deleted.")
                    .setPositiveButton("Exit") { dialog, _ ->
                        dialog.dismiss()
                        finish()
                    }
                    .setNegativeButton("No") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create().show()
        } else {
            vpRespondentDetail.setCurrentItem(vpRespondentDetail.currentItem - 1, true)
        }
    }

    var respondentId = -1
    private var respondentDto = RespondentDto()
    override fun onEntered(
            houseHoldNumber: String?,
            date: String?,
            latitude: String?,
            longitude: String?,
            elevation: String?
    ) {
        val responseDto = RespondentDto().apply {
            this.householdNumber = houseHoldNumber
            this.entryDate = date
            this.latitude = latitude
            this.longitude = longitude
            this.elevation = elevation
        }
        val respondentDao = RespondentDao(this@RespondentDetailActivity)
        if (respondentId == -1) {
            respondentId = respondentDao.insertIntoRespondent(responseDto).toInt()
        } else {
            respondentDao.updateHouseHoldInfo(responseDto, respondentId)
        }
        vpRespondentDetail.setCurrentItem(vpRespondentDetail.currentItem + 1, true)
    }

    override fun onNextPageClicked(province: String, district: String, municipality: String,
                                   wardNo: String, respondentName: String,
                                   caste: String, contactNo: String,
                                   age: String, sex: String,
                                   education: String, religion: String, occupation: String, imageFileName: String?) {
        val respondentDto = RespondentDto().apply {
            this.provience = province
            this.district = district
            this.municipality = municipality
            this.wardNumber = wardNo
            this.name = respondentName
            this.ethinicCaste = caste
            this.contactNumber = contactNo
            this.age = age
            this.gender = sex
            this.education = education
            this.religion = religion
            this.occupation = occupation
            this.imageFileName = imageFileName
        }
        val respondentDao = RespondentDao(this@RespondentDetailActivity)
        if (respondentId != -1) {
            respondentDao.updateRespondentInfo(respondentDto, respondentId)
        } else {
            respondentId = respondentDao.insertIntoRespondent(respondentDto).toInt()
        }

        startActivity(Intent(this@RespondentDetailActivity, QuestionnaireActivityV2::class.java)
                .apply {
                    putExtra(QuestionnaireActivityV2.KEY_RESPONDENT_ID, respondentId)
                })
    }

    override fun onPreviousClicked() {
        vpRespondentDetail.setCurrentItem(vpRespondentDetail.currentItem - 1, true)
    }


}
